import { TestBed } from '@angular/core/testing';

import { OnauthService } from './onauth.service';

describe('OnauthService', () => {
  let service: OnauthService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OnauthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
