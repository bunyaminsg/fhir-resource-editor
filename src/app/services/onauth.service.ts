import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import parseURLParameters from "../util/parse-url-query";

export interface OnAuthUserInfo {
  sub: string
  gender?: string
  roles?: string[]
  preferred_username?: string
  given_name?: string
  family_name?: string
  email?: string
}

@Injectable({
  providedIn: 'root'
})
export class OnauthService {

  private LOCAL_STORAGE_KEY = 'fhir-editor-onauth-session';

  private _idToken: string | undefined;
  get idToken(): string | undefined { return this._idToken; }
  set idToken(value: string | undefined) {
    this._idToken = value;
    if (value) {
      this.userInfo = JSON.parse(atob(value.split('.')[1]))
    } else {
      this.userInfo = undefined
    }
  }

  userInfo: OnAuthUserInfo | undefined;
  accessToken: string | undefined;
  expiresOn: Date | undefined;

  constructor() { }

  public static authorize(redirectUri: string): void {
    const responseTypes = encodeURIComponent(environment.onauth.client_config.response_types)
    const clientId = encodeURIComponent(environment.onauth.client_config.client_id)
    const scope = encodeURIComponent(environment.onauth.client_config.scope)
    const aud = encodeURIComponent(environment.onauth.client_config.audience)
    const state = Math.round(Math.random() * 100000000).toString()
    redirectUri = encodeURIComponent(redirectUri)

    window.location.href = `${environment.onauth.api}/authorize?response_type=${responseTypes}&client_id=${clientId}&` +
      `scope=${scope}&prompt=login&redirect_uri=${redirectUri}&aud=${aud}&state=${state}`
  }

  public static isLoginCallback(fragment: string | null) {
    return /(^|&)access_token=/.test(fragment || '')
  }

  hasSavedSession() {
    return !!localStorage[this.LOCAL_STORAGE_KEY]
  }

  handleRouteFragment(fragment: string) {
    const { access_token, id_token, expires_in } = parseURLParameters(fragment)
    this.accessToken = access_token
    this.idToken = id_token
    this.expiresOn = new Date(Date.now() + (expires_in * 1000))
    this.storeSession()
  }

  loadSession(): void {
    const { accessToken, idToken, expiresOn } = JSON.parse(localStorage[this.LOCAL_STORAGE_KEY])
    this.accessToken = accessToken
    this.idToken = idToken
    this.expiresOn = new Date(expiresOn)
  }

  storeSession(): void {
    localStorage[this.LOCAL_STORAGE_KEY] = JSON.stringify({
      accessToken: this.accessToken,
      idToken: this._idToken,
      expiresOn: this.expiresOn?.getTime()
    })
  }

  loggedIn(): boolean {
    return !!this.accessToken;
  }

  clearSession() {
    delete localStorage[this.LOCAL_STORAGE_KEY]
    this.accessToken = undefined
    this.idToken = undefined
    this.expiresOn = undefined
  }

}
