import {Injectable} from '@angular/core';
import {FHIRClient} from "./fhir-client";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class FhirService {

  constructor(private fhirClient: FHIRClient) {
  }

  getResource<T>(rtype: string, id: string): Observable<T> {
    return this.fhirClient.Get<T>(`${rtype}/${id}`);
  }

  updateResource<T>(gid: string | undefined, resource: object): Observable<T> {
    return this.fhirClient.Put<T>(`Goal/${gid}`, resource);
  }

  getPatients(): Observable<fhir.Bundle> {
    return this.fhirClient.Get('Patient');
  }

  getAllPatients(page: number): Observable<fhir.Bundle> {
    return this.fhirClient.Get(`Patient?_page=${page}`);
  }

  getObservation(id: string | undefined, page: number | undefined, filter: Object | any): Observable<fhir.Bundle> {

    let finalURL = "Observation?subject=Patient/" + id + "&_page=" + page;

    if (filter.statusFilter) {
      finalURL = finalURL + "&status=" + filter.statusFilter;
    }

    if (filter.date1Filter && filter.date2Filter) {
      finalURL = finalURL + "&date=ge" + filter.date1Filter + "&date=le" + filter.date2Filter;

    }

    if (filter.codeFilter) {
      finalURL = finalURL + "&code=" + filter.codeFilter;
    }

    return this.fhirClient.Get(finalURL);
  }

  getBundleForCodeFilter(): Observable<fhir.Bundle> {
    return this.fhirClient.Get(`Bundle?identifier=Kroniq-Observation-VS`);
  }

  getBundleForConditionFilterFMH(): Observable<fhir.Bundle> {
    return this.fhirClient.Get(`Bundle?identifier=Kroniq-Condition-VS`);
  }

  addObservation(body: Object): Observable<any> {
    return this.fhirClient.Post('Observation', body);
  }

  deleteObservation(id: string): Observable<any> {
    console.log("DELETE OBSERVATION", id);
    return this.fhirClient.Delete(`Observation/${id}`)
  }


  updateObservation(id: string | undefined, body: Object): Observable<any> {
    console.log("UPDATE OBSERVATION", id);
    return this.fhirClient.Put(`Observation/${id}`, body);
  }

  getFMH(id: string | undefined, page: number | undefined, filter: Object | any): Observable<fhir.Bundle> {

    let finalURL = "FamilyMemberHistory?patient=Patient/" + id + "&_page=" + page;

    if (filter.statusFilter) {
      finalURL = finalURL + "&status=" + filter.statusFilter;
    }

    if (filter.dateStartFilter && filter.dateEndFilter) {
      finalURL = finalURL + "&date=ge" + filter.dateStartFilter + "&date=le" + filter.dateEndFilter;
    }

    if (filter.codeFilter) {
      finalURL = finalURL + "&code=" + filter.codeFilter;
    }

    return this.fhirClient.Get(finalURL);
  }


  addFMH(body: Object): Observable<any> {
    return this.fhirClient.Post('FamilyMemberHistory', body);
  }

  deleteFMH(id: string): Observable<any> {
    console.log("DELETE FMH", id);
    return this.fhirClient.Delete(`FamilyMemberHistory/${id}`)
  }

  updateFHM(id: string | undefined, body: Object): Observable<any> {
    return this.fhirClient.Put(`FamilyMemberHistory/${id}`, body);
  }


  getPatientGoals(
    pid: string,
    page: number,
    sortOption: string = "",
    filterDate1: string = "",
    filterDate2: string = "",
    targetCode: string = ""
  ): Observable<fhir.Bundle> {
    let queryParams = `subject=Patient/${pid}`;

    if (sortOption) queryParams += `&_sort=${sortOption}`;
    if (filterDate1 && filterDate2) queryParams += `&target-date=ge${filterDate1}&target-date=le${filterDate2}`;
    else if (filterDate1) queryParams += `&target-date=ge${filterDate1}`;
    else if (filterDate2) queryParams += `&target-date=le${filterDate2}`;
    if (targetCode) queryParams += `&target-code=${targetCode}`;

    queryParams += `&_page=${page}`;

    return this.fhirClient.Get(`Goal?${queryParams}`);
  }

  getGoal<T>(gid: string): Observable<T> {
    return this.fhirClient.Get<T>(`Goal/${gid}`);
  }

  deleteGoal<T>(gid: string | undefined): Observable<T> {
    return this.fhirClient.Delete<T>(`Goal/${gid}`);
  }

  addGoal<T>(resource: object): Observable<T> {
    return this.fhirClient.Post<T>(`Goal`, resource);
  }

  getTargetCodes<T>(lang: string): Observable<T> {
    if (lang) return this.fhirClient.Post<T>(`ValueSet/$expand?url=http://kroniq.srdc.com.tr/fhir/ValueSet/goal-target-code&displayLanguage=${lang}`, {});
    return this.fhirClient.Get<T>(`ValueSet?url=http://kroniq.srdc.com.tr/fhir/ValueSet/goal-target-code`);
  }


  /*deleteResource<T>(pid:string, resource: object): Observable<T> {
    return this.fhirClient.Delete<T>(`Goal?subject=Patient/${pid}`, resource);
  }*/

  // MEDICATION REQUESTS
  getMedicationRequests(pid: string | undefined, page: number, filters: any): Observable<fhir.Bundle> {
    let url = "MedicationRequest?_sort=-authoredon&subject=Patient/" + pid + "&_page=" + page;
    if (filters.filterStatus !== "") {
      url += "&status=" + filters.filterStatus;
    }
    if (filters.filterIntent !== "") {
      url += "&intent=" + filters.filterIntent;
    }
    if (filters.filterMedication !== "") {
      url += "&code=" + filters.filterMedication;
    }

    return this.fhirClient.Get(url);
  }

  getMedicationBundle(language: string) {
    return this.fhirClient.Post(
      "ValueSet/$expand?url=http://kroniq.srdc.com.tr/fhir/ValueSet/carepath-medication-code&displayLanguage="
    + language, {})
  }

  getOneMedicationRequest(id: string) {
    return this.fhirClient.Get("MedicationRequest/" + id);
  }

  addMedicationRequest(medicationRequest: fhir.MedicationRequest) {
    return this.fhirClient.Post("MedicationRequest", medicationRequest);
  }

  updateMedicationRequest(id: string, medicationRequest: fhir.MedicationRequest) {
    return this.fhirClient.Put("MedicationRequest/" + id, medicationRequest);
  }

  deleteMedicationRequest(id: any) {
    return this.fhirClient.Delete("MedicationRequest/" + id);
  }

  // CONDITIONS
  getConditions(pid: string | undefined, page: number, filters: any): Observable<fhir.Bundle> {
    let url = "Condition?subject=Patient/" + pid + "&_sort=-recorded-date" + "&_page=" + page;
    if (filters.clinicalStatus !== "") {
      url += "&clinical-status=" + filters.clinicalStatus;
    }
    if (filters.verificationStatus !== "") {
      url += "&verification-status=" + filters.verificationStatus;
    }
    if (filters.conditionCode !== "") {
      url += "&code=" + filters.conditionCode;
    }

    return this.fhirClient.Get(url);
  }

  getOneCondition(id: string) {
    return this.fhirClient.Get("Condition/" + id)
  }

  deleteCondition(id: string) {
    return this.fhirClient.Delete("Condition/" + id)
  }

  getConditionBundle() {
    return this.fhirClient.Get("Bundle?identifier=Kroniq-Condition-VS")
  }

  addCondition(condition: any) {
    return this.fhirClient.Post("Condition", condition);
  }

  updateCondition(condition: fhir.Condition, id: string) {
    return this.fhirClient.Put("Condition/" + id, condition);
  }

  getPatientAppointments(aid: string,
                         page: number,
                         sortOption: string = "",
                         filterDate1: string = "",
                         filterDate2: string = "",
                         targetCode: string = ""):
    Observable<fhir.Bundle> {
    let queryParams = `patient=${aid}`;
    if (sortOption) queryParams += `&_sort=${sortOption}`;
    if (filterDate1 && filterDate2) queryParams += `&date=ge${filterDate1}&date=le${filterDate2}`;
    else if (filterDate1) queryParams += `&date=ge${filterDate1}`;
    else if (filterDate2) queryParams += `&date=le${filterDate2}`;
    if (targetCode) queryParams += `&target-code=${targetCode}`;
    queryParams += `&_page=${page}`;

    return this.fhirClient.Get(`Appointment?${queryParams}`);
  }

  deleteAppointment<T>(aid: string | undefined): Observable<T> {
    return this.fhirClient.Delete<T>(`Appointment/${aid}`);
  }

  addAppointment<T>(resource: object): Observable<T> {
    return this.fhirClient.Post<T>(`Appointment`, resource);
  }
}

