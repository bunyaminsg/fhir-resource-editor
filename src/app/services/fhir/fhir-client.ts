import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http'
import {Injectable} from '@angular/core'
import {Observable} from 'rxjs/internal/Observable'
import {environment} from '../../../environments/environment'
import {OnauthService} from "../onauth.service";

export interface RequestOptions {
  headers?: HttpHeaders;
  observe?: 'body';
  params?: HttpParams;
  reportProgress?: boolean;
  responseType?: 'json';
  withCredentials?: boolean;
  body?: any;
}

@Injectable({
  providedIn: 'root',
  deps: [HttpClient, OnauthService]
})
export class FHIRClient {

  private api: string = environment.onfhir.api;

  public constructor(private http: HttpClient, private onauthService: OnauthService) {
  }

  /**
   * GET request
   * @param {string} endPoint it doesn't need / in front of the end point
   * @param {RequestOptions} options options of the request like headers, body, etc.
   * @returns {Observable<T>}
   */
  public Get<T>(endPoint: string, options?: RequestOptions): Observable<T> {
    return this.http.get<T>(`${this.api}/${endPoint}`, this.setRequestOptions(options));
  }

  /**
   * POST request
   * @param {string} endPoint end point of the api
   * @param {Object} body body of the request.
   * @param {RequestOptions} options options of the request like headers, body, etc.
   * @returns {Observable<T>}
   */
  public Post<T>(endPoint: string, body: Object, options?: RequestOptions): Observable<T> {
    return this.http.post<T>(`${this.api}/${endPoint}`, body, this.setRequestOptions(options));
  }

  /**
   * PUT request
   * @param {string} endPoint end point of the api
   * @param {Object} body body of the request.
   * @param {RequestOptions} options options of the request like headers, body, etc.
   * @returns {Observable<T>}
   */
  public Put<T>(endPoint: string, body: Object, options?: RequestOptions): Observable<T> {
    return this.http.put<T>(`${this.api}/${endPoint}`, body, this.setRequestOptions(options));
  }

  /**
   * DELETE request
   * @param {string} endPoint end point of the api
   * @param {RequestOptions} options options of the request like headers, body, etc.
   * @returns {Observable<T>}
   */
  public Delete<T>(endPoint: string, options?: RequestOptions): Observable<T> {
    return this.http.delete<T>(`${this.api}/${endPoint}`, this.setRequestOptions(options));
  }

  /**
   * Appends Authorization, Accept and Content-type headers to the each request
   * @param {RequestOptions} requestOptions
   * @returns {RequestOptions}
   */
  private setRequestOptions(requestOptions?: RequestOptions): RequestOptions {
    if (!requestOptions) { requestOptions = {} }
    const headers: HttpHeaders = requestOptions?.headers || new HttpHeaders();
    requestOptions.headers = headers
      .append('Accept', 'application/fhir+json;charset=UTF-8')
      .append('Content-Type', 'application/fhir+json;charset=UTF-8')
    if (this.onauthService.accessToken) {
      requestOptions.headers = requestOptions.headers
        .append('Authorization', 'Bearer ' + this.onauthService.accessToken)
    }
    return requestOptions;
  }
}
