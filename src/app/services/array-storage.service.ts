import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ArrayStorageService {
  private stringCodePairs: { [key: string]: string[] } = {};

  constructor() {}

  getStringCodePairs() {
    return this.stringCodePairs;
  }

  addStringCodePairs(data: { [key: string]: string[] }) {
    this.stringCodePairs = { ...this.stringCodePairs, ...data };
  }
}
