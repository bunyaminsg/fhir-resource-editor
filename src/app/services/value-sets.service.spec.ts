import { TestBed } from '@angular/core/testing';

import { ValueSetsService } from './value-sets.service';

describe('ValueSetsService', () => {
  let service: ValueSetsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ValueSetsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
