import { TestBed } from '@angular/core/testing';

import { ArrayStorageService } from './array-storage.service';

describe('ArrayStorageService', () => {
  let service: ArrayStorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ArrayStorageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
