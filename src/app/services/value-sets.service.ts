import { Injectable } from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
@Injectable({
  providedIn: 'root'
})
export class ValueSetsService {
  constructor(
    private translate: TranslateService) {
      this.updateSets();
  }

  // MEDICATION REQUESTS
  whenSet! : any
  statusSet! : any
  intentSet!: any

  // CONDITIONS
  clinicalStatusSet! : any
  verificationStatusSet!: any

  getStatusKeys(): string[] {
    return Object.keys(this.statusSet);
  }

  getIntentKeys(): string[] {
    return Object.keys(this.intentSet);
  }

  getWhenKeys(): string[] {
    return Object.keys(this.whenSet);
  }
  getClinicalStatusKeys(): string[] {
    return Object.keys(this.clinicalStatusSet);
  }
  getVerificationStatusKeys(): string[] {
    return Object.keys(this.verificationStatusSet);
  }

  // UPDATE SETS AFTER LANGUAGE CHANGE
  updateSets() {
    // Medication Request Value Sets
    this.whenSet = {
      HS: this.translate.instant("HS"),
      WAKE: this.translate.instant("WAKE"),
      C: this.translate.instant("C"),
      CM: this.translate.instant("CM"),
      CD: this.translate.instant("CD"),
      CV: this.translate.instant("CV"),
      AC: this.translate.instant("AC"),
      ACM: this.translate.instant("ACM"),
      ACD: this.translate.instant("ACD"),
      ACV: this.translate.instant("ACV"),
      PC: this.translate.instant("PC"),
      PCM: this.translate.instant("PCM"),
      PCD: this.translate.instant("PCD"),
      PCV: this.translate.instant("PCV"),
    };

    this.statusSet = {
      active: this.translate.instant("active"),
      "on-hold": this.translate.instant("on-hold"),
      stopped: this.translate.instant("stopped"),
      completed: this.translate.instant("completed"),
      cancelled: this.translate.instant("cancelled"),
      "entered-in-error": this.translate.instant("entered-in-error"),
      draft: this.translate.instant("draft"),
      unknown: this.translate.instant("unknown"),
    };

    this.intentSet = {
      proposal: this.translate.instant("proposal"),
      plan: this.translate.instant("plan"),
      order: this.translate.instant("order"),
      "original-order": this.translate.instant("original-order"),
      "reflex-order": this.translate.instant("reflex-order"),
      "filler-order": this.translate.instant("filler-order"),
      "instance-order": this.translate.instant("instance-order"),
      option: this.translate.instant("option"),
    }

    // Condition Value Sets
    this.clinicalStatusSet = {
      active: this.translate.instant("active"),
      recurrence: this.translate.instant("recurrence"),
      relapse: this.translate.instant("relapse"),
      inactive: this.translate.instant("inactive"),
      remission: this.translate.instant("remission"),
      resolved: this.translate.instant("resolved")
    }
    this.verificationStatusSet = {
      unconfirmed: this.translate.instant("unconfirmed"),
      provisional: this.translate.instant("provisional"),
      differential: this.translate.instant("differential"),
      confirmed: this.translate.instant("confirmed"),
      refuted: this.translate.instant("refuted"),
      "entered-in-error": this.translate.instant("enteredInError")
    }
  }
}
