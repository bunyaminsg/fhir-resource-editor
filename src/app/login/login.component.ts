import { Component, OnInit } from '@angular/core';
import {OnauthService} from "../services/onauth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  login() {
    const redirectUrl = window.location.href

    OnauthService.authorize(redirectUrl)
  }

}
