import {ActivatedRouteSnapshot, CanActivateFn, Router, RouterStateSnapshot} from "@angular/router";
import {inject} from "@angular/core";
import {OnauthService} from "../services/onauth.service";

export const loginCanActivate: CanActivateFn = async (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
  const onAuthService = inject(OnauthService)
  const router = inject(Router)

  if (onAuthService.loggedIn()) {
    await router.navigate(['/patients'])
    return false
  } else if (OnauthService.isLoginCallback(route.fragment)) {
    onAuthService.handleRouteFragment(<string>route.fragment)
    await router.navigate(['/patients'])
    return false
  } else if (onAuthService.hasSavedSession()) {
    onAuthService.loadSession()
    if (onAuthService.expiresOn && onAuthService.expiresOn.getTime() > Date.now()) {
      await router.navigate(['/patients'])
      return false
    } else {
      onAuthService.clearSession()
      return true
    }
  } else {
    return true
  }
}
