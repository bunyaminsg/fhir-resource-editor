import {inject, NgModule} from '@angular/core';
import {Routes, RouterModule, CanActivateFn, Router} from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {loginCanActivate} from "./login/login-can-activate";
import {PatientsComponent} from "./patients/patients.component";
import {PatientObservationListComponent} from"./patient-management/patient-observation-list/patient-observation-list.component";
import {OnauthService} from "./services/onauth.service"; // CLI imports router

const authenticatedRoute: CanActivateFn = async route => {
  const onAuthService = inject(OnauthService)

  if (onAuthService.loggedIn()) {
    return true;
  } else {
    const router = inject(Router)
    await router.navigate(['/login'])
    return false
  }
}

const routes: Routes = [
  {
    path: 'patients',
    component: PatientsComponent,
    canActivate: [ authenticatedRoute ]
  },
  {
    path: 'patient',
    canActivate: [ authenticatedRoute ],
    loadChildren: () => import('./patient-management/patient-management.module').then(m => m.PatientManagementModule),
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [ loginCanActivate ]
  },
  {
    path: '',
    redirectTo: 'patients',
    pathMatch: 'full'
  }
]; // sets up routes constant where you define your routes

// configures NgModule imports and exports
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
