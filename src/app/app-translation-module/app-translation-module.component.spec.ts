import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppTranslationModuleComponent } from './app-translation-module.component';

describe('AppTranslationModuleComponent', () => {
  let component: AppTranslationModuleComponent;
  let fixture: ComponentFixture<AppTranslationModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppTranslationModuleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AppTranslationModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
