export default function parseURLParameters(parameters: string): any {
  const result: any = {};
  parameters.split('&').forEach((part) => {
    const item = part.split('=');
    result[item[0]] = decodeURIComponent(item[1]);
  });
  return result;
}
