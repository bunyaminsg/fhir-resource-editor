import parseURLParameters from "./parse-url-query";

describe('Utils', () => {

  it('should parse url query correctly', () => {
    expect(parseURLParameters("test=true&myParam=myValue").myParam === 'myValue').toBeTruthy();
  });
});
