import { Component } from '@angular/core';
import {OnauthService} from "./services/onauth.service";
import {Router} from "@angular/router";
import {TranslateService} from "@ngx-translate/core";




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'fhir-resource-editor';

  constructor(public onauthService: OnauthService, private router: Router, public translate: TranslateService,) {
    this.translate.setDefaultLang('en');
  }

  ngOnInit(): void {
  }

  logout() {
    this.onauthService.clearSession()
    this.router.navigate(['/login']).catch(console.error)
  }

  changeLanguage(lang: string) {
    this.translate.use(lang);
  }
}
