import { Component, OnInit } from '@angular/core';
import {FhirService} from "../services/fhir/fhir.service";

@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.scss']
})
export class PatientsComponent implements OnInit {
  patients: fhir.Patient[] = [];

  constructor(private fhirService: FhirService) {

    this.getAllPatients();
  }

  currentPage: number =  1;
  totalPageNumber: number | undefined = 99;


  ngOnInit(): void {

  }

  getAllPatients(){
    this.fhirService.getAllPatients(this.currentPage).subscribe(patients => {
      this.patients = patients.entry?.map(entry => <fhir.Patient>entry.resource) || []

      let number = patients.total;
      if (number) {
        this.totalPageNumber = Math.ceil(number / 20);
      }
    })
  }
  changePage(para: number){
    this.currentPage = para;
    this.getAllPatients();
  }
}



/*
      this.fhirService.getObservation(this.patientID, this.currentPage).subscribe((observations) => {
        this.observations = observations.entry?.map((entry) => <fhir.Observation>entry.resource) || [];
  
        let number = observations.total;
        if (number) {
          this.totalPageNumber = Math.ceil(number / 20);
        }
      });
*/