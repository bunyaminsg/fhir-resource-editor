import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import {FhirService} from "../../../services/fhir/fhir.service";
import {ActivatedRoute, Router} from "@angular/router";
import { OnauthService } from '../../../services/onauth.service';
import { ArrayStorageService } from 'src/app/services/array-storage.service';

@Component({
  selector: 'app-addgoal',
  templateUrl: './addgoal.component.html',
  styleUrls: ['./addgoal.component.scss']
})
export class AddgoalComponent implements OnInit {
  addgoalForm!: FormGroup;
  pid!: string;
  gid!: string;
  disabledCategory: boolean = true;
  targetAdded: boolean = false;
  updatemode: boolean = false;
  showSuccessMessage: boolean = false;
  targetString: string = '';
  targetCode: string = '';
  targetSystem: string = '';
  completions: string[] = [];
  stringCodePairs: { [key: string]: string[] } = {};
  categoryButtonText = 'addCategory';
  addTargetString = 'addTarget';
  updateModeString = 'addGoal';

  constructor(private formBuilder: FormBuilder, private fhirService: FhirService, private route: ActivatedRoute, private onauthService: OnauthService, private router: Router, private arrayStorageService: ArrayStorageService) {
    this.route.params.subscribe(params => {
    if(params['pid']){
      this.pid = params['pid'];
    }
    if(params['gid']){
      this.updatemode = true;
      this.gid = params['gid'];
      this.updateModeString = 'updateGoal';
      this.updateGoal(params['gid']);
    }
  })
  this.stringCodePairs = this.arrayStorageService.getStringCodePairs();
  this.mainForm();
  }

  ngOnInit() {

  }

  enableCategory() {
    this.disabledCategory = !this.disabledCategory;
    this.categoryButtonText = this.disabledCategory ? 'addCategory' : 'removeCategory';
    if(this.disabledCategory){
      this.addgoalForm.controls['categorySystem'].setValue('');
      this.addgoalForm.controls['categoryCode'].setValue('');
    }
  }

  mainForm(){
    this.addgoalForm = this.formBuilder.group({
      categorySystem: [''],
      categoryCode: [''],
      descriptionSystem: ['', Validators.required],
      descriptionCode: ['', Validators.required],
      descriptionDisplay: ['', Validators.required],
      descriptionText: ['', Validators.required],
      lifeCycleStatus: ['', Validators.required],
      targetValue: [''],
      targetValueLow: [''],
      targetUnit: [''],
      targetSystem: [''],
      targetCode: [''],
      targetDuedate: [''],
      measureSystem: [''],
      measureCode: [''],
      measureDisplay: [''],
      target2Value: [''],
      target2ValueLow: [''],
      target2Unit: [''],
      target2System: [''],
      target2Code: [''],
      target2Duedate: [''],
      measure2System: [''],
      measure2Code: [''],
      measure2Display: [''],
    });
  }


  onSubmit() {
    if (this.addgoalForm.valid) {
      const category: fhir.CodeableConcept[] = [];
      if(this.addgoalForm.value.categorySystem && this.addgoalForm.value.categoryCode){
        const categoryCodeableConcept: fhir.CodeableConcept = {
          coding: [
            {
              system: this.addgoalForm.value.categorySystem,
              code: this.addgoalForm.value.categoryCode,
            },
          ],
        };
        category.push(categoryCodeableConcept);
      }
      const measureCodeableConcept: fhir.CodeableConcept = {
        coding: [
          {
            system: this.addgoalForm.value.measureSystem,
            code: this.addgoalForm.value.measureCode,
            display: this.addgoalForm.value.measureDisplay,
          },
        ],
      };

      const description: fhir.CodeableConcept = {
        coding: [
          {
            system: this.addgoalForm.value.descriptionSystem,
            code: this.addgoalForm.value.descriptionCode,
            display: this.addgoalForm.value.descriptionDisplay,
          },

        ],
        text: this.addgoalForm.value.descriptionText,};

      const target: fhir.GoalTarget[] = [];


      const detailRange: fhir.Range = {};

      if (this.addgoalForm.value.targetValue) {
        detailRange.high = {
          value: this.addgoalForm.value.targetValue,
          unit: this.addgoalForm.value.targetUnit,
          system: this.addgoalForm.value.targetSystem,
          code: this.addgoalForm.value.targetCode,
        };
      }

      if (this.addgoalForm.value.targetValueLow) {
        detailRange.low = {
          value: this.addgoalForm.value.targetValueLow,
          unit: this.addgoalForm.value.targetUnit,
          system: this.addgoalForm.value.targetSystem,
          code: this.addgoalForm.value.targetCode,
        };
      }
      if(detailRange.high || detailRange.low){
        const target1: fhir.GoalTarget = {
          detailRange: detailRange,
          dueDate: this.addgoalForm.value.targetDuedate,
          measure: measureCodeableConcept,
        };
        target.push(target1);
      }
      if(this.targetAdded){
        const detailRange2: fhir.Range = {};

      if (this.addgoalForm.value.target2Value) {
        detailRange2.high = {
          value: this.addgoalForm.value.target2Value,
          unit: this.addgoalForm.value.target2Unit,
          system: this.addgoalForm.value.target2System,
          code: this.addgoalForm.value.target2Code,
        };
      }

      if (this.addgoalForm.value.target2ValueLow) {
        detailRange2.low = {
          value: this.addgoalForm.value.target2ValueLow,
          unit: this.addgoalForm.value.target2Unit,
          system: this.addgoalForm.value.target2System,
          code: this.addgoalForm.value.target2Code,
        };
      }

        const measureCodeableConcept2: fhir.CodeableConcept = {
          coding: [
            {
              system: this.addgoalForm.value.measure2System,
              code: this.addgoalForm.value.measure2Code,
              display: this.addgoalForm.value.measure2Display,
            },
          ],
        };

        const target2: fhir.GoalTarget = {
          detailRange: detailRange2,
          dueDate: this.addgoalForm.value.target2Duedate,
          measure: measureCodeableConcept2,
        };
        target.push(target2);
      }
      const date = new Date();

      const Goal: fhir.Goal = {
        resourceType: 'Goal',
        lifecycleStatus: this.addgoalForm.value.lifeCycleStatus,
        category: category? category : undefined,
        description: description,
        subject: {
          reference: 'Patient/' + this.pid,
        },
        expressedBy: {
          display: this.onauthService.userInfo?.given_name + ' ' + this.onauthService.userInfo?.family_name,
          reference: 'Practitioner/' + this.onauthService.userInfo?.sub,
        },
        target: target,
        startDate: date.getFullYear() + '-' + (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1))+ '-' + (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()),
      };
      if(this.pid && this.updatemode){
        Goal.id = this.gid;
        this.fhirService.updateResource<fhir.Goal>(this.gid, Goal).subscribe(
          (res) => {
            this.showSuccessMessage = true;
          }
        );
      }
      else{
        this.fhirService.addGoal<fhir.Goal>(Goal).subscribe(
          (res) => {
            this.showSuccessMessage = true;
          }
        );
      }
}
  }

updateGoal(gid: string){
  this.fhirService.getResource<fhir.Goal>('Goal', gid).subscribe((res) => {
    const goal = res;
    this.addgoalForm.patchValue({
      lifeCycleStatus: goal.lifecycleStatus,
      descriptionSystem: goal.description.coding?.at(0)?.system,
      descriptionCode: goal.description.coding?.at(0)?.code,
      descriptionDisplay: goal.description.coding?.at(0)?.display,
      descriptionText: goal.description.text,
      targetValue: goal.target?.at(0)?.detailRange?.high?.value,
      targetValueLow: goal.target?.at(0)?.detailRange?.low?.value,
      targetUnit: goal.target?.at(0)?.detailRange?.high?.unit,
      targetSystem: goal.target?.at(0)?.detailRange?.high?.system,
      targetCode: goal.target?.at(0)?.detailRange?.high?.code,
      targetDuedate: goal.target?.at(0)?.dueDate,
      measureSystem: goal.target?.at(0)?.measure?.coding?.at(0)?.system,
      measureCode: goal.target?.at(0)?.measure?.coding?.at(0)?.code,
      measureDisplay: goal.target?.at(0)?.measure?.coding?.at(0)?.display,
    });
    if(goal.category?.length && goal.category?.length > 0){
      this.enableCategory();
      this.addgoalForm.patchValue({
        categorySystem: goal.category?.at(0)?.coding?.at(0)?.system,
        categoryCode: goal.category?.at(0)?.coding?.at(0)?.code});
      }
    if(goal.target?.length > 1){
      this.addTarget();
      this.addgoalForm.patchValue({
        target2Value: goal.target?.at(1)?.detailRange?.high?.value,
        target2ValueLow: goal.target?.at(1)?.detailRange?.low?.value,
        target2Unit: goal.target?.at(1)?.detailRange?.high?.unit,
        target2System: goal.target?.at(1)?.detailRange?.high?.system,
        target2Code: goal.target?.at(1)?.detailRange?.high?.code,
        target2Duedate: goal.target?.at(1)?.dueDate,
        measure2System: goal.target?.at(1)?.measure?.coding?.at(0)?.system,
        measure2Code: goal.target?.at(1)?.measure?.coding?.at(0)?.code,
        measure2Display: goal.target?.at(1)?.measure?.coding?.at(0)?.display,
      });
    }
  });



}

addTarget() {
  this.targetAdded = !this.targetAdded;
  this.addTargetString = this.targetAdded ? 'removeTarget' : 'addTarget';
  if(this.targetAdded){
    this.addgoalForm.controls['target2Value'].setValue('');
    this.addgoalForm.controls['target2Unit'].setValue('');
    this.addgoalForm.controls['target2System'].setValue('');
    this.addgoalForm.controls['target2Code'].setValue('');
    this.addgoalForm.controls['target2Duedate'].setValue('');
    this.addgoalForm.controls['measure2System'].setValue('');
    this.addgoalForm.controls['measure2Code'].setValue('');
    this.addgoalForm.controls['measure2Display'].setValue('');
  }
}

onInputChange(): void {
  if(this.stringCodePairs[this.targetString]){
    this.targetCode = this.stringCodePairs[this.targetString][0];
    this.targetSystem = this.stringCodePairs[this.targetString][1];
  }else{
    this.targetCode = '';
    this.targetSystem = '';
  }
  if(this.targetString.length == 0){
    this.targetCode = '';
    this.completions = [];
  }else{
    this.completions = Object.keys(this.stringCodePairs).filter(key =>
      key.toLowerCase().includes(this.targetString.toLowerCase())
    );
  }
}

closeTypeahead(): void {
  setTimeout(() => {
    this.completions = []; // Close the dropdown after a short delay
  }, 150);
}

selectCompletion(completion: string): void {
  this.targetString = completion;
  this.targetSystem = this.stringCodePairs[completion][1];
  this.targetCode = this.stringCodePairs[completion][0];
  this.completions = [];
}

}
