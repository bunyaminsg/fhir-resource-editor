import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FhirService} from "../../services/fhir/fhir.service";
import { TranslateService } from '@ngx-translate/core';
import { ArrayStorageService } from 'src/app/services/array-storage.service';

@Component({
  selector: 'app-goals',
  templateUrl: './goals.component.html',
  styleUrls: ['./goals.component.scss']
})
export class GoalsComponent implements OnInit {
  patient!: fhir.Patient;
  goals!: fhir.Goal[];
  currentPage: number = 1;
  totalPages!: number;
  pid!: string;
  selectedSortOption!: string;
  searchStartDate!: string;
  searchEndDate!: string;
  stringCodePairs: { [key: string]: string[] } = {};
  targetCode!: string;
  targetString!: string;
  completions: string[] = [];
  language: string = 'en';

  constructor(private route: ActivatedRoute, private fhirService: FhirService, private router: Router, private arrayStorageService: ArrayStorageService, private translate: TranslateService ) {
    this.selectedSortOption = '';
    this.route.params.subscribe(params => {
      if (params['pid']) {
        this.pid = params['pid'];
        this.fhirService.getPatientGoals(this.pid, this.currentPage, this.selectedSortOption, this.searchStartDate, this.searchEndDate, this.targetCode).subscribe({
          next: goals => {
            if(goals.total && goals.entry?.length){
              this.totalPages = Math.ceil(goals.total / goals.entry?.length);
            }
            this.goals = goals.entry!.map(entry => entry.resource as fhir.Goal);
          }
        })
        this.fhirService.getTargetCodes<fhir.Bundle>(this.language).subscribe({
          next: code => {
            if(code){
              code.entry?.forEach(entry => {
                const res = entry.resource as fhir.ValueSet;
                res.compose?.include?.forEach(include => {
                  include.concept?.forEach(concept => {
                    this.stringCodePairs[concept.display as string] = [concept.code as string, res.compose?.include[0].system as string];
                  })
                })
              })
              arrayStorageService.addStringCodePairs(this.stringCodePairs);
            }
          }
            })
        }
        })
        }

  ngOnInit(): void {
  }

  getGoals(){
    if(this.pid){
      this.fhirService.getPatientGoals(this.pid, this.currentPage, this.selectedSortOption, this.searchStartDate, this.searchEndDate, this.targetCode).subscribe({
        next: goals => {
          if(goals.total && goals.entry!.length){
            this.totalPages = Math.ceil(goals.total / 20) || 1;
          }else{
            this.totalPages = 1;
          }
          this.goals = goals.entry!.map(entry => entry.resource as fhir.Goal);
        }
      })
    }
  }

  updateGoal(goal: fhir.Goal) {
    this.router.navigate(['patient/' + this.pid + '/goals/edit/' + goal.id]);
  }

  addGoal() {
    this.router.navigate(['patient/' + this.pid + '/goals/edit']);
  }

  deleteGoal(gid: string) {
    const deleteConfirmation = this.translate.instant('deleteConfirmation');
    if (window.confirm(deleteConfirmation)) {
      if(this.goals){
        const index = this.goals.findIndex(goal => goal.id === gid);
        if (index !== -1) {
          this.goals.splice(index, 1);
        }
        this.fhirService.deleteGoal<fhir.Goal>(gid).subscribe();
      }
  }
}

getPageNumbers() {
  const pages: number[] = [];
  if(this.totalPages){
  for (let i = 1; i <= this.totalPages; i++) {
    pages.push(i);
  }}
  return pages;
}

prevPage() {
  if(this.currentPage > 1){
    this.currentPage--;
    this.getGoals();
  }}

nextPage() {
    if(this.totalPages && this.currentPage < this.totalPages){
      this.currentPage++;
      this.getGoals();
    }
}

gotoPage(page: number) {
  this.currentPage = page;
  this.getGoals();
}

sortGoals(selectedSortOption: string = this.selectedSortOption) {
  if(this.pid){
    this.selectedSortOption = selectedSortOption;
    this.getGoals();
  }
}

onInputChange(): void {
  if(this.targetString){
    if(this.stringCodePairs[this.targetString]){
      this.targetCode = this.stringCodePairs[this.targetString][0];
    }else{
      this.targetCode = '';
    }
    if(this.targetString.length == 0){
      this.targetCode = '';
      this.completions = [];
    }else{
      this.completions = Object.keys(this.stringCodePairs).filter(key =>
        key.toLowerCase().includes(this.targetString.toLowerCase())
      );
    }
  }
}

selectCompletion(completion: string): void {
  this.targetString = completion;
  this.targetCode = this.stringCodePairs[completion][0];
  this.completions = [];
}

closeTypeahead(): void {
  setTimeout(() => {
    this.completions = []; // Close the dropdown after a short delay
  }, 200);
}

clearFilters(){
  this.searchStartDate = '';
  this.searchEndDate = '';
  this.targetCode = '';
  this.targetString = '';
  this.getGoals();
}

changeLanguage(lang: string) {
  this.language = lang;
  this.translate.use(lang);
  this.stringCodePairs = {};
  this.fhirService.getTargetCodes<fhir.ValueSet>(lang).subscribe({
    next: code => {
      if(code){
        code.expansion?.contains?.forEach(entry => {
              this.stringCodePairs[entry.display as string] = [entry.code as string, entry.system as string];
            })
      }
    }
      })
}

}
