import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PatientManagementComponent} from "./patient-management.component";
import {PatientObservationListComponent} from "./patient-observation-list/patient-observation-list.component";
import {PatientObservationCreateComponent} from "./patient-observation-create/patient-observation-create.component";

import {
  PatientFamilyMemberHistoryListComponent
} from "./patient-family-member-history-list/patient-family-member-history-list.component";
import {
  PatientFamilyMemberHistoryCreateComponent
} from "./patient-family-member-history-create/patient-family-member-history-create.component";
import {MedicationRequestsComponent} from "./medication-requests/medication-requests.component";
import {
  MedicationAddOrEditComponent
} from "./medication-requests/medication-add-or-edit/medication-add-or-edit.component";
import {ConditionsComponent} from "./conditions/conditions.component";
import {GoalsComponent} from "./goals/goals.component";
import {AppointmentsComponent} from "./appointments/appointments.component";
import {AddgoalComponent} from "./goals/addgoal/addgoal.component";
import {ConditionAddOrEditComponent} from "./conditions/condition-add-or-edit/condition-add-or-edit.component";
import {AddAppointmentComponent} from "./add-appointment/add-appointment.component";


const routes: Routes = [
  {
    path: ':pid',
    component: PatientManagementComponent,
    children: []
  },
  {
    path: '',
    redirectTo: '/',
    pathMatch: 'full'
  },
  {
    path: ":pid/medication-requests",
    component: MedicationRequestsComponent
  },
  {
    path: ":pid/medication-requests/edit",
    component: MedicationAddOrEditComponent
  },
  {
    path: ":pid/medication-requests/edit/:id",
    component: MedicationAddOrEditComponent
  },
  {
    path: ":pid/conditions",
    component: ConditionsComponent
  },
  {
    path: ":pid/conditions/edit/:id",
    component: ConditionAddOrEditComponent
  },
  {
    path: ":pid/conditions/edit",
    component: ConditionAddOrEditComponent
  },
  {
    path: ':pid/goals',
    component: GoalsComponent,
  },
  {
    path: ':pid/goals/edit',
    component: AddgoalComponent,
  },
  {
    path: ':pid/goals/edit/:gid',
    component: AddgoalComponent,
  },
  {
    path: ':pid/appointments',
    component: AppointmentsComponent,
  },

  {
    path: ':pid/appointments/edit',
    component: AddAppointmentComponent,
  },

  {
    path: ':pid/appointments/edit/:aid',
    component: AddAppointmentComponent,
  },
  {
    path: ":pid/observation",
    component: PatientObservationListComponent,
  },

  {
    path: ":pid/observation/edit",
    component: PatientObservationCreateComponent,
  },

  {
    path: ":pid/observation/edit/:id",
    component: PatientObservationCreateComponent,
  },

  {
    path: ":pid/familyMemberHistory",
    component: PatientFamilyMemberHistoryListComponent,
  },

  {
    path: ":pid/familyMemberHistory/edit",
    component: PatientFamilyMemberHistoryCreateComponent,
  },

  {
    path: ":pid/familyMemberHistory/edit/:id",
    component: PatientFamilyMemberHistoryCreateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientManagementRoutingModule {
}
