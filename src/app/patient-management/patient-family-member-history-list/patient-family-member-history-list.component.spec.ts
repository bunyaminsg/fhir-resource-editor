import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientFamilyMemberHistoryListComponent } from './patient-family-member-history-list.component';

describe('PatientFamilyMemberHistoryListComponent', () => {
  let component: PatientFamilyMemberHistoryListComponent;
  let fixture: ComponentFixture<PatientFamilyMemberHistoryListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientFamilyMemberHistoryListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PatientFamilyMemberHistoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
