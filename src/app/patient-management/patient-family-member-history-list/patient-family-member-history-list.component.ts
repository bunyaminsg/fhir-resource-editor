import { Component, OnInit } from '@angular/core';
import {FhirService} from "../../services/fhir/fhir.service";
import {ActivatedRoute} from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
@Component({

  selector: 'app-patient-family-member-history-list',
  templateUrl: './patient-family-member-history-list.component.html',
  styleUrls: ['./patient-family-member-history-list.component.scss']

})
export class PatientFamilyMemberHistoryListComponent implements OnInit {

  //fmhistory array
  fmhistory: fhir.FamilyMemberHistory[] = [];

  // Patient Info
  patient: fhir.Patient | undefined
  patientName: string | undefined
  patientID: string | undefined

  //Page infos
  currentPage: number =  1;
  totalPageNumber: number | undefined = 99;

  //date
  isDateClicked: boolean = false;
  isClicked: boolean = false;


  // Filter object
  finalFilter = {
    statusFilter: "",
    dateStartFilter: "",
    dateEndFilter: "",
    codeFilter:""
  }
    
  // Status Filter
  filterStatus!: string;

  //Date Filters
  dateStart! : string ;
  dateEnd! : string ;

  // Variables for code filter
  searchCode!: string;
  searchTerm: string = '';
  filterList: any = [];
  savefilterList: any = [];
  allFMH: any[] = [];
  showResults: boolean = false;
  counter: number = 0;

  // DEFAULT LANGUAGE
  selectedLanguage!: string; 
    
  constructor(private route: ActivatedRoute, private fhirService: FhirService,public translate: TranslateService,private r : Router) {
    if(this.translate.currentLang){
      this.translate.setDefaultLang(this.translate.currentLang)
    }
    else{
      this.translate.setDefaultLang("en")
    }
    this.route.params.subscribe(params => {
      if (params['pid']) {
        this.fhirService.getResource<fhir.Patient>('Patient', params['pid']).subscribe({
          next: patient => {
            this.patient = patient
            this.patientName = this.patient?.name?.map(name => [...(name.given || []), name.family].join(' ')).pop();
            this.patientID = this.patient.id;

      
            this.getFMH();
          },
          error: console.error
        })
      }
    })
    this.bundleConditionFilterInit();
  }

  getFMH(){

    if(this.filterStatus ||  this.filterStatus === ""){
      this.finalFilter.statusFilter = this.filterStatus;
    }

    if(this.dateStart && this.dateEnd){
      this.finalFilter.dateStartFilter = this.dateStart;
      this.finalFilter.dateEndFilter = this.dateEnd;
    }

    if(this.searchCode || this.searchCode === "" ){
      this.finalFilter.codeFilter = this.searchCode;
    }

    this.fhirService.getFMH(this.patientID, this.currentPage,this.finalFilter).subscribe((fmhs) => {
      this.fmhistory = fmhs.entry?.map((entry) => <fhir.FamilyMemberHistory>entry.resource) || [];

      let number = fmhs.total;
      if (number) {
        this.totalPageNumber = Math.ceil(number / 20);
      }
      else{
        this.totalPageNumber = 0;
      }
    });
  }


  changePage(para: number){
    this.currentPage = para;
    this.getFMH();
  }

  ngOnInit(): void {
    this.filterStatus ="";
    this.dateStart = "";
    this.dateEnd = "";
    this.getFMH();
  }

  // Filters
  applyStatusFilter() {
    this.currentPage = 1; // Reset the current page to the first page when applying the filter
    this.getFMH();
  }

  applyDateFilter() {
    this.currentPage = 1; // Reset the current page to the first page when applying the filter
    this.getFMH();
  }

  applyConditionFilter() {
    this.currentPage = 1; // Reset the current page to the first page when applying the filter
    this.getFMH();
  }

  clearDates() {
    this.dateStart = "";
    this.dateEnd = "";
    this.finalFilter.dateStartFilter ="";
    this.finalFilter.dateEndFilter ="";
    this.applyDateFilter(); // Call the method to filter data again after resetting dates
  }
  
  bundleConditionFilterInit(): void {
    this.fhirService.getBundleForConditionFilterFMH().subscribe({
      next: (data: any) => {
        this.allFMH = data['entry'][0].resource.entry;

        this.updateFilterList();
      }
    });
  }

  updateFilterList(): void {
    this.filterList = [];
    
    this.filterList = this.allFMH.filter((observation: any) =>
      observation.resource.compose.include.some((include: any) =>
        include.concept.some((concept: any) =>
          this.getConceptValue(concept).toLowerCase().includes(this.searchTerm.trim().toLowerCase())
        )
      )
    );
    this.savefilterList = this.filterList;
  }

  getConceptValue(concept: any): string {
    const conceptDesignations = concept.designation;
    const selectedLanguageValue = conceptDesignations.find((designation: any) => designation.language ===  this.translate.currentLang);
    
    return selectedLanguageValue ? selectedLanguageValue.value : concept.display;
  }
 

  onSearchBoxClick(): void {

    this.showResults = true; 
    if(this.counter>0){
      this.onSearchChange();
    }
    this.counter++;
  }

  onSearchChange(): void {
    if (this.searchTerm.trim() === '') {
      //this.filterList = []; // Clear the filterList when the input is empty
    } else {
      this.updateFilterList();
    }
  }

  selectValue(value: string, value2: string, event: Event): void {
    const clickedElement = event.target as HTMLElement;
    const dropdownList = document.querySelector('.dropdown-list');
    
    if (dropdownList && dropdownList.contains(clickedElement)) {

      this.searchTerm = value;
      this.searchCode = value2;
      this.showResults = false;
    }
  }

  clearSearch(): void {
    this.showResults = false; 
  }

  clearNoFilter(): void{
    this.searchTerm = '';
    this.searchCode = '';
    this.updateFilterList();
    this.getFMH();
  }


  removeFMH(histID:any,index:any) {
    if (window.confirm(this.translate.instant('confirmDelete'))) {
      this.fhirService.deleteFMH(histID).subscribe(
        () => {
          this.fmhistory.splice(index, 1);

          alert(this.translate.instant('deleteSuccess'));
        },
        () => {

          alert(this.translate.instant('deleteError'));
        }
      );
    }
  }


  toggleEffectiveDateTimeClicked() {
    this.isDateClicked = !this.isDateClicked;
  }

  changeLanguage(language: string) {
    alert("Language change");
    this.selectedLanguage = language;
    //this.localizationService.setLanguage(language);
    this.translate.use(language);
    
    const patientId = this.route.snapshot.params['pid'];
    const updatedUrl = `/patient/${patientId}/familyMemberHistory`;
    this.r.navigateByUrl(updatedUrl);

  }

  sort(sortBy: string) {
    
    this.toggleEffectiveDateTimeClicked();


    this.fmhistory.sort((a: any, b: any) => {
      const propA = a[sortBy];
      const propB = b[sortBy];
      

      if(propA>propB ){
        if(this.isDateClicked){
          return -1;
        }
        else{
          return 1;
        }

      }
      else{
        if(this.isDateClicked){
          return 1;
        }
        else{
          return -1;
        }
      }
  

    });
  }
}