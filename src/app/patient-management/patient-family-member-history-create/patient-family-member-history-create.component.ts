import { Component, OnInit } from '@angular/core';
import {FhirService} from "../../services/fhir/fhir.service";
import {ActivatedRoute} from "@angular/router";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LocalizationService } from '../../services/localization.service';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-patient-family-member-history-create',
  templateUrl: './patient-family-member-history-create.component.html',
  styleUrls: ['./patient-family-member-history-create.component.scss']
})
export class PatientFamilyMemberHistoryCreateComponent implements OnInit {

  submitted = false;

  // Patient info to display
  patient: fhir.Patient | undefined;
  patientName: string | undefined;
  patientID: string | undefined;


  fmhForm!: FormGroup;


  // variables to be used in editing
  id: string | undefined; 
  isEdit: boolean | undefined;
  fhmEdit : fhir.FamilyMemberHistory | undefined;

  // Variables for code filter
  searchCode!: string;
  searchTerm: string = '';
  searchSystem!: string;
  filterList: any = [];
  savefilterList: any = [];
  allFMH: any[] = [];
  showResults: boolean = false;
  counter: number = 0;

  //Check if selected
  selectedRelationship: boolean = false;
  selectRelationshipDisplay!: string;

  //FAMILY MEMBER
  familyList: any = [
    { code: "FAMMEMB", display: { en: "family member", fr: "membre de la famille", de: "Familienglied", es: "miembro de la familia", tr: "aile üyesi" }},
    { code: "CHILD", display: { en: "child", fr: "enfant", de: "Kind", es: "niño", tr: "çocuk" }},
    { code: "CHLD", display: { en: "child", fr: "enfant", de: "Kind", es: "niño", tr: "çocuk" }},
    { code: "CHLDADOPT", display: { en: "adopted child", fr: "enfant adopté", de: "adoptiertes Kind", es: "niño adoptado", tr: "evlatlık çocuk" }},
    { code: "DAUADOPT", display: { en: "adopted daughter", fr: "fille adoptive", de: "adoptierte Tochter", es: "hija adoptada", tr: "evlatlık kız" }},
    { code: "SONADOPT", display: { en: "adopted son", fr: "fils adoptif", de: "adoptierter Sohn", es: "hijo adoptado", tr: "evlatlık erkek" }},
    { code: "CHLDFOST", display: { en: "foster child", fr: "enfant d'accueil", de: "Pflegekind", es: "niño de crianza", tr: "evlat edinilen çocuk" }},
    { code: "DAUFOST", display: { en: "foster daughter", fr: "fille d'accueil", de: "Pflegetochter", es: "hija de crianza", tr: "evlat edinilen kız" }},
    { code: "SONFOST", display: { en: "foster son", fr: "fils d'accueil", de: "Pflegesohn", es: "hijo de crianza", tr: "evlat edinilen erkek" }},
    { code: "DAUC", display: { en: "daughter", fr: "fille", de: "Tochter", es: "hija", tr: "kız" }},
    { code: "DAU", display: { en: "natural daughter", fr: "fille biologique", de: "leibliche Tochter", es: "hija biológica", tr: "doğal kız" }},
    { code: "STPDAU", display: { en: "stepdaughter", fr: "belle-fille", de: "Stieftochter", es: "hijastra", tr: "üvey kız" }},
    { code: "NCHILD", display: { en: "natural child", fr: "enfant biologique", de: "leibliches Kind", es: "niño biológico", tr: "doğal çocuk" }},
    { code: "SON", display: { en: "natural son", fr: "fils biologique", de: "leiblicher Sohn", es: "hijo biológico", tr: "doğal erkek çocuk" }},
    { code: "SONC", display: { en: "son", fr: "fils", de: "Sohn", es: "hijo", tr: "erkek çocuk" }},
    { code: "STPSON", display: { en: "stepson", fr: "beau-fils", de: "Stiefsohn", es: "hijastro", tr: "üvey erkek çocuk" }},
    { code: "STPCHLD", display: { en: "step child", fr: "beau-enfant", de: "Stiefkind", es: "hijastro", tr: "üvey çocuk" }},
    { code: "ADOPTP", display: { en: "adoptive parent", fr: "parent adoptif", de: "Adoptiveltern", es: "padre adoptivo", tr: "evlat edinen ebeveyn" }},
    { code: "FTH", display: { en: "father", fr: "père", de: "Vater", es: "padre", tr: "baba" }},
    { code: "FTHFOST", display: { en: "foster father", fr: "père d'accueil", de: "Pflegevater", es: "padre de crianza", tr: "evlat edinen baba" }},
    { code: "NFTH", display: { en: "natural father", fr: "père biologique", de: "leiblicher Vater", es: "padre biológico", tr: "doğal baba" }},
    { code: "NFTHF", display: { en: "natural father of fetus", fr: "père biologique du fœtus", de: "leiblicher Vater des Fötus", es: "padre biológico del feto", tr: "doğal fetusun babası" }},
    { code: "STPFTH", display: { en: "stepfather", fr: "beau-père", de: "Stiefvater", es: "padrastro", tr: "üvey baba" }},
    { code: "MTH", display: { en: "mother", fr: "mère", de: "Mutter", es: "madre", tr: "anne" }},
    { code: "GESTM", display: { en: "gestational mother", fr: "mère gestationnelle", de: "Leihmutter", es: "madre gestacional", tr: "gestasyonel anne" }},
    { code: "MTHFOST", display: { en: "foster mother", fr: "mère d'accueil", de: "Pflegemutter", es: "madre de crianza", tr: "evlat edinen anne" }},
    { code: "NMTH", display: { en: "natural mother", fr: "mère biologique", de: "leibliche Mutter", es: "madre biológica", tr: "doğal anne" }},
    { code: "NMTHF", display: { en: "natural mother of fetus", fr: "mère biologique du fœtus", de: "leibliche Mutter des Fötus", es: "madre biológica del feto", tr: "doğal fetusun annesi" }},
    { code: "STPMTH", display: { en: "stepmother", fr: "belle-mère", de: "Stiefmutter", es: "madrastra", tr: "üvey anne" }},
    { code: "NPRN", display: { en: "natural parent", fr: "parent biologique", de: "leiblicher Elternteil", es: "padre biológico", tr: "doğal ebeveyn" }},
    { code: "PRNFOST", display: { en: "foster parent", fr: "parent d'accueil", de: "Pflegeelternteil", es: "padre de crianza", tr: "evlat edinen ebeveyn" }},
    { code: "STPPRN", display: { en: "step parent", fr: "beau-parent", de: "Stiefelternteil", es: "padrastro/madrastra", tr: "üvey ebeveyn" }},
    { code: "SIB", display: { en: "sibling", fr: "frère/soeur", de: "Geschwister", es: "hermano/hermana", tr: "kardeş" }},
    { code: "BRO", display: { en: "brother", fr: "frère", de: "Bruder", es: "hermano", tr: "erkek kardeş" }},
    { code: "HSIB", display: { en: "half-sibling", fr: "demi-frère/demi-soeur", de: "Halbgeschwister", es: "medio hermano/medio hermana", tr: "yarım kardeş" }},
    { code: "NSIB", display: { en: "natural sibling", fr: "frère/soeur biologique", de: "leibliche Geschwister", es: "hermano/hermana biológico/a", tr: "doğal kardeş" }},
    { code: "TWINBRO", display: { en: "twin brother", fr: "frère jumeau", de: "Zwillingsbruder", es: "hermano gemelo", tr: "ikiz erkek kardeş" }},
    { code: "FTWINBRO", display: { en: "fraternal twin brother", fr: "frère jumeau fraternel", de: "fraternaler Zwillingsbruder", es: "hermano gemelo fraterno", tr: "fraternal ikiz erkek kardeş" }},
    { code: "ITWINBRO", display: { en: "identical twin brother", fr: "frère jumeau identique", de: "identischer Zwillingsbruder", es: "hermano gemelo idéntico", tr: "aynı cins ikiz erkek kardeş" }},
    { code: "GRNDDAU", display: { en: "granddaughter", fr: "petite-fille", de: "Enkelin", es: "nieta", tr: "torun kızı" }},
    { code: "GRNDSON", display: { en: "grandson", fr: "petit-fils", de: "Enkel", es: "nieto", tr: "torun erkek" }},
    { code: "GRFTH", display: { en: "grandfather", fr: "grand-père", de: "Großvater", es: "abuelo", tr: "büyükbaba" }},
    { code: "MGRFTH", display: { en: "maternal grandfather", fr: "grand-père maternel", de: "mütterlicher Großvater", es: "abuelo materno", tr: "anne tarafından büyükbaba" }},
    { code: "PGRFTH", display: { en: "paternal grandfather", fr: "grand-père paternel", de: "väterlicher Großvater", es: "abuelo paterno", tr: "baba tarafından büyükbaba" }},
    { code: "GRMTH", display: { en: "grandmother", fr: "grand-mère", de: "Großmutter", es: "abuela", tr: "büyükanne" }},
    { code: "MGRMTH", display: { en: "maternal grandmother", fr: "grand-mère maternelle", de: "mütterliche Großmutter", es: "abuela materna", tr: "anne tarafından büyükanne" }},
    { code: "PGRMTH", display: { en: "paternal grandmother", fr: "grand-mère paternelle", de: "väterliche Großmutter", es: "abuela paterna", tr: "büyükbabaanne" }},
    { code: "GESTM", display: { en: "gestational mother", fr: "mère gestationnelle", de: "Leihmutter", es: "madre gestacional", tr: "gestasyonel anne" }},
    { code: "HSIS", display: { en: "half-sister", fr: "demi-sœur", de: "Halbschwester", es: "media hermana", tr: "yarı kız kardeş" }},
    { code: "NSIS", display: { en: "natural sister", fr: "sœur biologique", de: "leibliche Schwester", es: "hermana biológica", tr: "doğal kız kardeş" }},
    { code: "FTWINSIS", display: { en: "fraternal twin sister", fr: "sœur jumelle fraternelle", de: "fraternale Zwillingschwester", es: "hermana gemela fraterna", tr: "fraternal ikiz kız kardeş" }},
    { code: "ITWINSIS", display: { en: "identical twin sister", fr: "sœur jumelle identique", de: "identische Zwillingschwester", es: "hermana gemela idéntica", tr: "identik ikiz kız kardeş" }},
    { code: "TWIN", display: { en: "twin", fr: "jumeau/jumelle", de: "Zwilling", es: "gemelo/gemela", tr: "ikiz" }},
    { code: "FTWIN", display: { en: "fraternal twin", fr: "jumeau fraternel", de: "fraternale Zwilling", es: "gemelo fraterno", tr: "fraternal ikiz" }},
    { code: "ITWIN", display: { en: "identical twin", fr: "jumeau identique", de: "identischer Zwilling", es: "gemelo idéntico", tr: "identik ikiz" }},
    { code: "MGGRFTH", display: { en: "maternal great-grandfather", fr: "arrière-grand-père maternel", de: "mütterlicher Urgroßvater", es: "bisabuelo materno", tr: "anne tarafından büyük dede" }},
    { code: "PGGRFTH", display: { en: "paternal great-grandfather", fr: "arrière-grand-père paternel", de: "väterlicher Urgroßvater", es: "bisabuelo paterno", tr: "baba tarafından büyük dede" }},
    { code: "MGGRMTH", display: { en: "maternal great-grandmother", fr: "arrière-grand-mère maternelle", de: "mütterliche Urgroßmutter", es: "bisabuela materna", tr: "anne tarafından büyük babaanne" }},
    { code: "PGGRMTH", display: { en: "paternal great-grandmother", fr: "arrière-grand-mère paternelle", de: "väterliche Urgroßmutter", es: "bisabuela paterna", tr: "baba tarafından büyük babaanne" }},
    { code: "MGGRPRN", display: { en: "maternal great-grandparent", fr: "arrière-grand-parent maternel", de: "mütterlicher Urgroßelternteil", es: "bisabuelo/bisabuela materno/a", tr: "anne tarafından büyük büyükanne/büyükbaba" }},
    { code: "PGGRPRN", display: { en: "paternal great-grandparent", fr: "arrière-grand-parent paternel", de: "väterlicher Urgroßelternteil", es: "bisabuelo/bisabuela paterno/a", tr: "baba tarafından büyük büyükanne/büyükbaba" }},
    { code: "DAUINLAW", display: { en: "daughter in-law", fr: "belle-fille", de: "Schwiegertochter", es: "nuera", tr: "gelin" }},
    { code: "SONINLAW", display: { en: "son in-law", fr: "gendre", de: "Schwiegersohn", es: "yerno", tr: "damat" }},
    { code: "FTHINLAW", display: { en: "father-in-law", fr: "beau-père", de: "Schwiegervater", es: "suegro", tr: "kayınpeder" }},
    { code: "MTHINLAW", display: { en: "mother-in-law", fr: "belle-mère", de: "Schwiegermutter", es: "suegra", tr: "kayınvalide" }},
    { code: "BROINLAW", display: { en: "brother-in-law", fr: "beau-frère", de: "Schwager", es: "cuñado", tr: "kayınbirader" }},
    { code: "SISINLAW", display: { en: "sister-in-law", fr: "belle-sœur", de: "Schwägerin", es: "cuñada", tr: "kayınbirader" }},
    { code: "NIECE", display: { en: "niece", fr: "nièce", de: "Nichte", es: "sobrina", tr: "yeğen kız" }},
    { code: "NEPHEW", display: { en: "nephew", fr: "neveu", de: "Neffe", es: "sobrino", tr: "yeğen erkek" }}

  ];

  constructor( public fb: FormBuilder,private router: ActivatedRoute, private fhirService: FhirService,private route: ActivatedRoute,public localizationService: LocalizationService,public translate: TranslateService, private r : Router) {
    
    // if url has an observation ID edit else create
    let editnumber = this.route.snapshot.paramMap.get('id') ;
    if(editnumber){
      this.id = editnumber;
    }

    if(this.id){
      this.isEdit = true;
    }
    else{
      this.isEdit = false;
    }

    this.bundleConditionFilterInit();
  }

  ngOnInit(): void {

    this.mainForm();

    if(this.isEdit){
      this.getFMHorDefault(this.id);
    }
  }

  findMath(code:string | undefined): string{
    let bool = false;
    let disp = "";
    this.familyList.forEach((element:any) => {

      if(element.code === code){
        bool = true;
        disp = element.display[this.translate.currentLang]
      }
    });

    if(bool){
      return disp;
    }
    else{
      return "NO DISPLAY";
    }

  }


  getFMHorDefault(id : string | undefined){
    if (id) {
      this.selectedRelationship = true;

      

      this.fhirService.getResource<fhir.FamilyMemberHistory>('FamilyMemberHistory', id).subscribe({

        next: hist => {
          this.fhmEdit = hist;

          this.searchTerm = this.fhmEdit.condition?.at(0)?.code.coding?.at(0)?.display || "";

          console.log("code",this.fhmEdit.relationship.coding?.at(0)?.code)
          this.selectRelationshipDisplay = this.findMath(this.fhmEdit.relationship.coding?.at(0)?.code)
          console.log("this.selectRelationshipDisplay",this.selectRelationshipDisplay)
          
          console.log(this.selectRelationshipDisplay)

          this.updateFilterList();

          this.fmhForm = this.fb.group({

            patientReference: "Patient/"+this.patientID,
            patientDisplay: this.patientName,

            status: this.fhmEdit.status,

            relationshipCode: this.fhmEdit.relationship.coding?.at(0)?.code,         /// MUST HAVE
            relationshipSystem: this.fhmEdit.relationship.coding?.at(0)?.system,
            relationshipDisplay:  this.selectRelationshipDisplay,
      
            // Condition
            conditionCode: this.fhmEdit.condition?.at(0)?.code.coding?.at(0)?.code,
            conditionDisplay: this.fhmEdit.condition?.at(0)?.code.coding?.at(0)?.display,
            conditionSystem: this.fhmEdit.condition?.at(0)?.code.coding?.at(0)?.system,
      
            // Date
            date: this.fhmEdit.date,


          });
          this.updateFilterList();
        },
        error: console.error
      })
    }
  };


  mainForm() {

    this.route.params.subscribe(params => {
      if (params['pid']) {
        this.fhirService.getResource<fhir.Patient>('Patient', params['pid']).subscribe({
          next: patient => {
            this.patient = patient
            this.patientName = this.patient?.name?.map(name => [...(name.given || []), name.family].join(' ')).pop();
            this.patientID = this.patient.id;

            this.fmhForm.patchValue({
              patientReference: "Patient/"+this.patientID,
              patientDisplay: this.patientName,
              
              fhmID: this.id, //

              date: new Date(),

              relationshipSystem: "http://terminology.hl7.org/ValueSet/v3-FamilyMember"
            });
          },
          error: console.error
        })
      }
    })


    this.fmhForm= this.fb.group({

      // Patient
      patientReference: [{ value: '', disabled: true }], 
      patientDisplay: [{ value: '', disabled: true }],

      // Status
      status: ['entered-in-error', [Validators.required]], /// MUST HAVE
      
      // Relationship
      relationshipCode: ['', Validators.required],          /// MUST HAVE
      relationshipSystem: [{ value: '', disabled: true }],
      relationshipDisplay:[''],


      // Condition
      conditionCode: ['', Validators.required],
      conditionDisplay: ['', Validators.required],
      conditionSystem: ['', Validators.required],


      // Date
      date: [{ value: '', disabled: true }], // Control for Effective DateTime


      fhmID:[{ value: '', disabled: true }],
    });
  }

    // Getter to access form control
  get myForm() {
    return this.fmhForm.controls;
  }

  bundleConditionFilterInit(): void {
    this.fhirService.getBundleForConditionFilterFMH().subscribe({
      next: (data: any) => {
        this.allFMH = data['entry'][0].resource.entry;

        this.updateFilterList();
      }
    });
  }

  updateFilterList(): void {
    this.filterList = [];
    
    this.filterList = this.allFMH.filter((observation: any) =>
      observation.resource.compose.include.some((include: any) =>
        include.concept.some((concept: any) =>
          this.getConceptValue(concept).toLowerCase().includes(this.searchTerm.trim().toLowerCase())
        )
      )
    );
    this.savefilterList = this.filterList;
  }

  getConceptValue(concept: any): string {
    const conceptDesignations = concept.designation;
    const selectedLanguageValue = conceptDesignations.find((designation: any) => designation.language ===  this.translate.currentLang);
    
    return selectedLanguageValue ? selectedLanguageValue.value : concept.display;
  }
 

  onSearchBoxClick(): void {

    this.showResults = true; 
    if(this.counter>0){
      this.onSearchChange();
    }
    this.counter++;
  }

  onSearchChange(): void {
    if (this.searchTerm.trim() === '') {
      //this.filterList = []; // Clear the filterList when the input is empty
    } else {
      this.updateFilterList();
    }
  }

  selectValue(value: string, value2: string, value3: string, event: Event): void {
    const clickedElement = event.target as HTMLElement;
    const dropdownList = document.querySelector('.dropdown-list');

    if (dropdownList && dropdownList.contains(clickedElement)) {

      this.searchTerm = value;
      this.searchCode = value2;
      this.searchSystem = value3;

      this.fmhForm.patchValue({
        conditionCode: this.searchCode  ,
        conditionDisplay: this.searchTerm ,
        conditionSystem: this.searchSystem ,
      });

      this.showResults = false;
    }
  }
  clearSearch(): void {
    this.showResults = false; 
  }

  onRelationshipDisplaySelected(event: any) {
    const selectedValue = event.target.value;
    this.selectedRelationship = selectedValue !== null && selectedValue !== 'null';
    this.fmhForm.controls['relationshipCode'].setValue(this.getCodeFromDisplay(selectedValue));
  }
  
  getCodeFromDisplay(display: string): string | undefined {
    const selectedRelationship = this.familyList.find((relationship :any ) => relationship.display[this.translate.currentLang] === display);
    return selectedRelationship ? selectedRelationship.code : undefined;
  }

  onSubmit() {
    this.submitted = true;

    const statusA = this.fmhForm.get("status")?.value;

    const patientReferenceA = this.fmhForm.get("patientReference")?.value;
    const patientDisplayA =  this.fmhForm.get("patientDisplay")?.value;

    const relationshipCode = this.fmhForm.get("relationshipCode")?.value;
    const relationshipSystem = this.fmhForm.get("relationshipSystem")?.value;

    const conditionCode = this.fmhForm.get("conditionCode")?.value;
    const conditionSystem = this.fmhForm.get("conditionSystem")?.value;
    const conditionDisplay = this.fmhForm.get("conditionDisplay")?.value;

    const date = this.fmhForm.get("date")?.value;


    if (!this.selectedRelationship || conditionCode===null ) {
      alert("SOME PARTS ARE MISSING. FILL THEM AND TRY AGAIN");
      return;
    }

    let fmhObject: fhir.FamilyMemberHistory={
      
      resourceType: "FamilyMemberHistory",

      status: statusA,

      patient: {
        reference: patientReferenceA,
      },

      
      relationship: {
        coding: [
            {
                system: relationshipSystem,
                code: relationshipCode
            }
        ]
      },

      condition: [
        {
            code: {
                coding: [
                    {
                        system: conditionSystem,
                        code: conditionCode,
                        display: conditionDisplay 
                    }
                ]
            }
        }
      ],

      date: date,

    }

    if(this.isEdit){
      fmhObject.id =  this.id;
    }

    if(this.isEdit){
      this.fhirService.updateFHM(this.id,fmhObject)
      .subscribe({
        complete: () =>{
          alert(this.translate.instant('editConfirm'));
          this.r.navigateByUrl('/patient/' + this.patientID + '/familyMemberHistory');
        },
        error: (e) =>{
          alert("SOMETHING WENT WRONG");
          alert(this.translate.instant('editError'));
        }
      });
    }
    if(!this.isEdit){
      this.fhirService.addFMH(fmhObject)
      .subscribe({
        complete: () =>{
          alert(this.translate.instant('addConfirm'));
          this.r.navigateByUrl('/patient/' + this.patientID + '/familyMemberHistory');
        },
        error: (e) =>{
          alert("SOMETHING WENT WRONG");
          alert(this.translate.instant('addError'));
        }
      });
    }
    

    
  }

}
