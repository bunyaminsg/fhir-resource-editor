import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientFamilyMemberHistoryCreateComponent } from './patient-family-member-history-create.component';

describe('PatientFamilyMemberHistoryCreateComponent', () => {
  let component: PatientFamilyMemberHistoryCreateComponent;
  let fixture: ComponentFixture<PatientFamilyMemberHistoryCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientFamilyMemberHistoryCreateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PatientFamilyMemberHistoryCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
