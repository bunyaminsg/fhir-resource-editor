import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {FhirService} from "../../../services/fhir/fhir.service";
import {OnauthService, OnAuthUserInfo} from "../../../services/onauth.service";
import {ValueSetsService} from "../../../services/value-sets.service";
import {TranslateService} from "@ngx-translate/core";

// CONSTANTS FOR TRANSLATION PURPOSES
const ADD_SUCCESS = "addSuccess"
const UPDATE_SUCCESS = "updateSuccess"
const ADD_ERROR = "addError"
const UPDATE_ERROR = "updateError"

@Component({
  selector: 'app-medication-add-or-edit',
  templateUrl: './medication-add-or-edit.component.html',
  styleUrls: ['./medication-add-or-edit.component.scss']
})
export class MedicationAddOrEditComponent implements OnInit {
  editOrAdd : boolean = false;
  medicationForm : FormGroup;
  submitted : boolean = false;

  patientName: string | undefined
  patient: fhir.Patient | undefined
  patientID: string | undefined

  addDosageInfo: boolean = false;
  addDosageQuantity: boolean = false;
  hourOrWhen: boolean = true;

  // FOR EDITING
  medication: any;

  //------------------ BOUND VARIABLES ---------------
  medicationName: string = ""
  code: string = ""
  system: string = ""
  status: string = ""
  intent: string = ""

  //DosageInstruction
  text: string = ""
  startDate: string = ""
  endDate: string = ""
  frequency: number = 1
  period: number = 1
  periodUnit: string = ""

  //DosageQuantity
  quantityValue: number = 20
  quantityUnit: string = ""
  quantitySystem: string = ""
  quantityCode: string = ""

  // MULTIPLE ELEMENTS ARRAY
  multipleElements: string[] = []

  // FOR MEDICATION FILTER
  medicationBundle: any;
  filterList: any = [];
  medicationFilterMode: boolean = false;

  languageOptions = [
    { language: "en",  display: "English" },
    { language: "de",  display: "German" },
    { language: "tr",  display: "Turkish" },
    { language: "es",  display: "Spanish" },
  ];

  currentLanguage!: string

  constructor(private fb: FormBuilder,
              private route: ActivatedRoute,
              private fhirService: FhirService,
              private router: Router,
              private onAuthService: OnauthService,
              public valueSetsService: ValueSetsService,
              public translate: TranslateService) {
    this.route.params.subscribe(params => {
      if(this.translate.currentLang) {
        this.currentLanguage = this.translate.currentLang;
      }
      else {
        this.translate.setDefaultLang("en")
        this.translate.use("en");
        this.currentLanguage = "en";
      }

      if (params['pid']) {
        this.fhirService.getResource<fhir.Patient>('Patient', params['pid']).subscribe({
          next: patient => {
            this.patient = patient
            this.patientName = this.patient?.name?.map(name => [...(name.given || []), name.family].join(' ')).pop();
            this.patientID = this.patient.id;
          },
          error: console.error
        })
      }
    })
    this.medicationForm = this.fb.group({
      medicationName: ['', Validators.required],
      code: ['', Validators.required],
      system: ['', Validators.required],
      status: ['', Validators.required],
      intent: ['', Validators.required],
      text: [''],
      startDate: [''],
      endDate: [''],
      frequency: [''],
      period: [''],
      periodUnit: [''],
      quantityValue: [''],
      quantityUnit: [''],
      quantityCode: [''],
      quantitySystem: ['']
    })

    this.bundleLoader();
  }

  ngOnInit(): void {
    if(this.route.snapshot.url[3]) {
      this.editOrAdd = true;

      this.getMedicationRequestToEdit();
    }
    else {
      this.editOrAdd = false;
    }
  }

  onLanguageChange() {
    this.translate.use(this.currentLanguage)

    // UPDATE VALUE SETS AND BUNDLE
    this.valueSetsService.updateSets();
    this.bundleLoader();
  }

  get myForm() {
    return this.medicationForm.controls;
  }

  getMedicationRequestToEdit() {
    this.fhirService.getOneMedicationRequest(this.route.snapshot.url[3].toString())
      .subscribe({
        next: res => {
          this.medication =  <fhir.MedicationRequest> res;

          this.medicationName = this.medication?.medicationCodeableConcept?.coding?.[0]?.display
          this.code = this.medication?.medicationCodeableConcept?.coding?.[0]?.code
          this.system = this.medication?.medicationCodeableConcept?.coding?.[0]?.system
          this.status = this.medication?.status
          this.intent = this.medication?.intent

          if(!this.medication?.dosageInstruction) {
            this.addDosageInfo = false;
          }
          else {
            this.addDosageInfo = true;

            this.text = this.medication?.dosageInstruction[0].text || ""
            this.startDate = this.medication?.dosageInstruction[0].timing.repeat.boundsPeriod.start || ""
            // SLICE ":00.000Z"
            this.startDate = this.startDate.slice(0,-8);
            this.endDate = this.medication?.dosageInstruction[0].timing.repeat.boundsPeriod.end || "";
            this.endDate = this.endDate.slice(0,-8);
            this.frequency = this.medication?.dosageInstruction[0].timing.repeat.frequency || 1
            this.period = this.medication?.dosageInstruction[0].timing.repeat.period || 1

            this.periodUnit = this.medication?.dosageInstruction[0].timing.repeat.periodUnit || 1

            if(this.medication?.dosageInstruction[0].timing.repeat.timeOfDay) {
              this.hourOrWhen = true;
            }
            else if (this.medication?.dosageInstruction[0].timing.repeat.when) {
              this.hourOrWhen = false;
            }

            if(this.medication?.dosageInstruction[0].timing.repeat.timeOfDay
              || this.medication?.dosageInstruction[0].timing.repeat.when
              || this.medication?.dosageInstruction[0].timing.repeat.dayOfWeek) {
              this.multipleElements = this.medication?.dosageInstruction[0].timing.repeat.timeOfDay
                || this.medication?.dosageInstruction[0].timing.repeat.when
                || this.medication?.dosageInstruction[0].timing.repeat.dayOfWeek;
            }

            if(!this.medication?.dosageInstruction[0].doseAndRate) {
              this.addDosageQuantity = false;
            }
            else {
              this.addDosageQuantity = true;

              this.quantityValue = this.medication?.dosageInstruction[0].doseAndRate[0].doseQuantity.value;
              this.quantityUnit = this.medication?.dosageInstruction[0].doseAndRate[0].doseQuantity.unit;
              this.quantitySystem = this.medication?.dosageInstruction[0].doseAndRate[0].doseQuantity.system;
              this.quantityCode = this.medication?.dosageInstruction[0].doseAndRate[0].doseQuantity.code;
            }
          }
        },
        error: err => {
          console.error(err);
        }
      })
  }

  toggleDosageInfo() {
    this.addDosageInfo = !this.addDosageInfo;

    // RESET
    this.medicationForm.get("text")?.setValue('');
    this.medicationForm.get("startDate")?.setValue('');
    this.medicationForm.get("frequency")?.setValue('');
    this.medicationForm.get("period")?.setValue('');
    this.medicationForm.get("periodUnit")?.setValue('');

    // SET VALIDATORS IN ORDER TO UPDATE VALIDITY
    const validators = this.addDosageInfo ? [Validators.required] : null;
    this.medicationForm.get("text")?.setValidators(validators)
    this.medicationForm.get("text")?.updateValueAndValidity();

    this.medicationForm.get("startDate")?.setValidators(validators)
    this.medicationForm.get("startDate")?.updateValueAndValidity();

    this.medicationForm.get("endDate")?.setValidators(validators);
    this.medicationForm.get("endDate")?.updateValueAndValidity();

    this.medicationForm.get("frequency")?.setValidators(validators)
    this.medicationForm.get("frequency")?.updateValueAndValidity();

    this.medicationForm.get("period")?.setValidators(validators)
    this.medicationForm.get("period")?.updateValueAndValidity();

    this.medicationForm.get("periodUnit")?.setValidators(validators)
    this.medicationForm.get("periodUnit")?.updateValueAndValidity();

  }
  toggleDosageQuantity() {
    this.addDosageQuantity = !this.addDosageQuantity;

    // RESET
    this.medicationForm.get("quantityValue")?.setValue('');
    this.medicationForm.get("quantityUnit")?.setValue('');
    this.medicationForm.get("quantitySystem")?.setValue('');
    this.medicationForm.get("quantityCode")?.setValue('');

    // SET VALIDATORS IN ORDER TO UPDATE VALIDITY
    const validators = this.addDosageQuantity ? [Validators.required] : null;
    this.medicationForm.get("quantityValue")?.setValidators(validators)
    this.medicationForm.get("quantityValue")?.updateValueAndValidity()

    this.medicationForm.get("quantityUnit")?.setValidators(validators)
    this.medicationForm.get("quantityUnit")?.updateValueAndValidity()

    this.medicationForm.get("quantitySystem")?.setValidators(validators)
    this.medicationForm.get("quantitySystem")?.updateValueAndValidity()

    this.medicationForm.get("quantityCode")?.setValidators(validators)
    this.medicationForm.get("quantityCode")?.updateValueAndValidity()
  }

  changeHourOrWhen(bool:boolean) {
    this.hourOrWhen = bool;
  }

  multipleElementsArray(n: number) {
    let arr: string[] = [];
    for (let i=0; i<n; i++) {
      arr.push('');
    }
    return arr;
  }

  multipleElementsHelper() {
    if(this.frequency > 1) {
      this.multipleElements = new Array(this.frequency).fill('');
    }
  }

  bundleLoader() {
    this.fhirService.getMedicationBundle(this.translate.currentLang).subscribe({
      next: res => {
        this.medicationBundle = res;
      }
    })
  }

  initializeFilterList() {
    this.medicationFilterMode = true
    if(this.medicationName === "") {
      this.filterList = []

      for(let i=0; i < this.medicationBundle.expansion.total; i++) {
        this.filterList.push(this.medicationBundle.expansion.contains[i]);
      }
    }
    else {
      this.onSearchChange()
    }
  }

  closeMedicationFilterMode() {
    this.medicationFilterMode = false;
  }

  onSearchChange(): void {
    this.filterList = [];

    if(this.medicationName === "") {
      this.code = "";
      this.system = "";
    }

    for(let i=0; i < this.medicationBundle.expansion.total; i++) {
      const medicationName = this.medicationBundle.expansion.contains[i].display
      if(medicationName.toLowerCase().includes(this.medicationName.trim().toLowerCase())) {
        this.filterList.push(this.medicationBundle.expansion.contains[i])
      }
    }
  }

  selectValue(display: string, code: string, system: string) {
    this.medicationName = display;
    this.code = code;
    this.system = system;

    this.medicationFilterMode = false;
  }

  onSubmit() {
    this.submitted = true;
    if(!this.medicationForm.valid) {
      return;
    }
    else {
      // CHECK IF MULTIPLE ELEMENTS ARRAY IS VALID
      if(this.frequency > 1)
      {
        for (let i=0; i < this.multipleElements.length; i++) {
          if(this.multipleElements[i] === ""){
            return;
          }
        }
      }

      let medicationRequest: fhir.MedicationRequest = {
        resourceType: "MedicationRequest",
        subject: {
          reference: "Patient/" + this.patientID,
          display: this.patientName
        },
        status: this.status,
        intent: this.intent,
        medicationCodeableConcept: {
          coding: [
            {
              display: this.medicationName,
              code: this.code,
              system: this.system
            }
          ]
        },
      }

      // ADD DOSAGE INSTRUCTION
      if(this.addDosageInfo) {
        medicationRequest.dosageInstruction = [
          {
            text: this.text,
            timing: {
              repeat: {
                boundsPeriod: {
                  start: this.startDate + ":00.000Z",
                  end: this.endDate + ":00.000Z"
                },
                frequency: this.frequency,
                period: this.period,
                periodUnit: this.periodUnit
              }
            }
          }
        ]
        // ADD DOSAGE QUANTITY
        if(this.addDosageQuantity) {
          medicationRequest.dosageInstruction[0].doseAndRate = [{
            doseQuantity: {
              value: this.quantityValue,
              unit: this.quantityUnit,
              system: this.quantitySystem,
              code: this.quantityCode
            }
          }]
        }

        // TIME OF DAY / WHEN / DAY OF WEEK
        if(this.frequency > 1) {
          if(this.periodUnit === "wk") {
            // @ts-ignore
            medicationRequest.dosageInstruction[0].timing.repeat.dayOfWeek = this.multipleElements;
          }
          else {
            if(this.hourOrWhen)
            {
              // @ts-ignore
              medicationRequest.dosageInstruction[0].timing.repeat.timeOfDay = this.multipleElements.map(el => {
                if(el.length === 5) {
                  return el + ":00";
                }
                  return el;
              });
            }
            else {
              // @ts-ignore
              medicationRequest.dosageInstruction[0].timing.repeat.when = this.multipleElements;
            }
          }
        }
      }

      // ADD REQUESTER AND AUTHOREDON IF IT IS IN ADD MODE
      if(!this.editOrAdd) {
        medicationRequest.requester = {
          reference: "Practitioner/" + this.onAuthService.userInfo?.sub,
          display: this.onAuthService.userInfo?.given_name + " " + this.onAuthService.userInfo?.family_name
        }

        const localDate = new Date();
        const newHour = -(localDate.getTimezoneOffset() / 60)
        localDate.setHours(localDate.getHours() + newHour);
        medicationRequest.authoredOn = localDate.toISOString();
      }

      // KEEP REQUESTER AND AUTHOREDON IF IT IS IN EDIT MODE
      if(this.editOrAdd) {
        medicationRequest.requester = {
          reference: this.medication.requester.reference,
          display: this.medication.requester.display
        }

        medicationRequest.authoredOn = this.medication.authoredOn;
      }
      // ADD THE REQUEST
      if(!this.editOrAdd) {
        this.fhirService.addMedicationRequest(medicationRequest)
          .subscribe({
            next: res =>{
              window.alert(this.translate.instant(ADD_SUCCESS));
              this.router.navigateByUrl("/patient/" + this.patientID+ "/medication-requests");
            },
            error: err => {
              window.alert(this.translate.instant(ADD_ERROR));
            }
          })
      }
      else {
        medicationRequest.id = this.route.snapshot.url[3].toString();
        this.fhirService.updateMedicationRequest(this.route.snapshot.url[3].toString(), medicationRequest)
          .subscribe({
            next: res => {
              window.alert(this.translate.instant(UPDATE_SUCCESS));
              this.router.navigateByUrl("/patient/" + this.patientID+ "/medication-requests");
            },
            error: err => {
              window.alert(this.translate.instant(UPDATE_ERROR));
            }
          })
      }
    }
  }
}
