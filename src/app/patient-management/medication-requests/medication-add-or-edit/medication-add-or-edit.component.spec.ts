import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicationAddOrEditComponent } from './medication-add-or-edit.component';

describe('MedicationAddOrEditComponent', () => {
  let component: MedicationAddOrEditComponent;
  let fixture: ComponentFixture<MedicationAddOrEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicationAddOrEditComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MedicationAddOrEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
