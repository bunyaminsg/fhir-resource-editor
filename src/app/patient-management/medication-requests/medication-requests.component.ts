import { Component, OnInit } from '@angular/core';
import {FhirService} from "../../services/fhir/fhir.service";
import {ActivatedRoute} from "@angular/router";
import {ValueSetsService} from "../../services/value-sets.service";
import {TranslateService} from "@ngx-translate/core";

// SOME CONSTANTS
const MEDICATION_NAME = "medicationName"
const CODE = "code"
const SYSTEM = "system"
const STATUS = "status"
const INTENT = "intent"
const INSTRUCTION = "instruction"
const START_DATE = "startDate"
const END_DATE = "endDate"
const QUANTITY_VALUE = "quantityValue"
const QUANTITY_UNIT = "quantityUnit"
const FREQUENCY = "frequency"
const FREQUENCY_SPECIFIER = "frequencySpecifier"
const PERIOD = "period"
const PERIOD_UNIT = "periodUnit"
const REQUESTER = "requester"
const AUTHORED_ON = "authoredOn"

// CONSTANTS FOR TRANSLATION PURPOSES
const DELETE_CONFIRM = "deleteConfirmMedicationRequest"

@Component({
  selector: 'app-medication-requests',
  templateUrl: './medication-requests.component.html',
  styleUrls: ['./medication-requests.component.scss']
})
export class MedicationRequestsComponent implements OnInit {
  patientName!: string
  patient!: fhir.Patient
  patientID!: string
  medications: fhir.MedicationRequest[] = [];
  currentPage : number = 1;
  totalPages: number = 1;
  sortKey: string = '';
  sortDirection: number = 1;

  filters = {
    filterStatus: "",
    filterIntent: "",
    filterMedication: ""
  }

  // FOR MEDICATION FILTER
  medicationBundle: any;
  query = {
    medicationName: "",
    code: ""
  };
  filterList: any = [];
  medicationFilterMode: boolean = false;

  // ngContainer table headers. Initialized and updated by setTableHeaders()
  tableHeaders!: any

  currentLanguage!: string

  languageOptions = [
    { language: "en",  display: "English" },
    { language: "de",  display: "German" },
    { language: "tr",  display: "Turkish" },
    { language: "es",  display: "Spanish" },
  ];

  constructor(private route: ActivatedRoute,
              private fhirService: FhirService,
              public valueSetsService: ValueSetsService,
              public translate: TranslateService) {
    this.route.params.subscribe(params => {

      if(this.translate.currentLang) {
        this.currentLanguage = this.translate.currentLang;
      }
      else {
        this.translate.setDefaultLang("en")
        this.translate.use("en");
        this.currentLanguage = "en";
      }

      if (params['pid']) {
        this.fhirService.getResource<fhir.Patient>('Patient', params['pid']).subscribe({
          next: patient => {
            this.patient = patient
            const tmpName = this.patient?.name?.map(name => [...(name.given || []), name.family].join(' ')).pop();
            this.patientName =  tmpName ? tmpName : "";
            const tmpID = this.patient.id;
            this.patientID = tmpID ? tmpID : "";
            this.getMedicationRequests();
            this.setTableHeaders();
            this.bundleLoader();
          },
          error: console.error
        })
      }
    })
  }

  setTableHeaders() {
    this.tableHeaders = [
      { label: this.translate.instant(MEDICATION_NAME), sortKey: MEDICATION_NAME },
      { label: this.translate.instant(CODE), sortKey: CODE },
      { label: this.translate.instant( STATUS), sortKey: STATUS },
      { label: this.translate.instant(INTENT), sortKey: INTENT },
      { label: this.translate.instant(INSTRUCTION), sortKey: INSTRUCTION },
      { label: this.translate.instant(START_DATE), sortKey: START_DATE },
      { label: this.translate.instant(END_DATE), sortKey: END_DATE },
      { label: this.translate.instant(QUANTITY_VALUE), sortKey: QUANTITY_VALUE },
      { label: this.translate.instant(QUANTITY_UNIT), sortKey: QUANTITY_UNIT },
      { label: this.translate.instant(FREQUENCY), sortKey: FREQUENCY },
      { label: this.translate.instant(FREQUENCY_SPECIFIER), sortKey: FREQUENCY_SPECIFIER },
      { label: this.translate.instant(PERIOD), sortKey: PERIOD },
      { label: this.translate.instant(PERIOD_UNIT), sortKey: PERIOD_UNIT },
      { label: this.translate.instant(REQUESTER), sortKey: REQUESTER },
      { label: this.translate.instant(AUTHORED_ON), sortKey: AUTHORED_ON },
    ];
  }

  getMedicationRequests() {
    this.fhirService.getMedicationRequests(this.patientID, this.currentPage, this.filters).subscribe(
      medications => {
        this.medications = medications.entry?.map(entry => <fhir.MedicationRequest>entry.resource)
          || [];

        const tmp = medications.total;
        if(tmp) {
          this.totalPages = Math.ceil(tmp / 20);
        }
      }
    )
  }

  sendFilteredRequest() {
    this.currentPage = 1;
    this.getMedicationRequests();
  }

  deleteMedicationRequest(medication: any, index: number) {
    if (window.confirm(this.translate.instant(DELETE_CONFIRM))) {
      this.fhirService.deleteMedicationRequest(medication?.id).subscribe(() => {
        this.medications.splice(index, 1);
      })
    }
  }

  goToPage(pageNumber: number) {
    this.currentPage = pageNumber;
    this.sortKey = "";
    this.getMedicationRequests();
  }

  getColumn(medication:any, column:string){
    // nil and -Infinity are used for better sorting
    const nil = "ZZZZZZZZZZZZZ";

    if(column === MEDICATION_NAME) {
      return medication?.medicationCodeableConcept?.coding?.[0]?.display;
    }
    else if(column == CODE) {
      return medication?.medicationCodeableConcept?.coding?.[0]?.code;
    }
    else if(column == SYSTEM) {
      return medication?.medicationCodeableConcept?.coding?.[0]?.system;
    }
    else if(column === STATUS) {
      return medication.status;
    }
    else if(column === INTENT) {
      return medication.intent;
    }
    else if(column === INSTRUCTION) {
      return medication?.dosageInstruction?.[0]?.text || nil;
    }
    else if(column === START_DATE) {
      return medication?.dosageInstruction?.[0]?.timing?.repeat?.boundsPeriod?.start || nil;
    }
    else if(column === END_DATE) {
      return medication?.dosageInstruction?.[0]?.timing?.repeat?.boundsPeriod?.end || nil;
    }
    else if(column === QUANTITY_VALUE) {
      return medication?.dosageInstruction?.[0]?.doseAndRate?.[0]?.doseQuantity.value || -Infinity;
    }
    else if(column === QUANTITY_UNIT) {
      return medication?.dosageInstruction?.[0]?.doseAndRate?.[0]?.doseQuantity.unit || nil;
    }
    else if(column === FREQUENCY) {
      return medication?.dosageInstruction?.[0]?.timing?.repeat?.frequency || -Infinity
    }
    else if(column === FREQUENCY_SPECIFIER) {
      return medication?.dosageInstruction?.[0]?.timing?.repeat?.timeOfDay ||
        medication?.dosageInstruction?.[0]?.timing?.repeat?.when ||
        medication?.dosageInstruction?.[0]?.timing?.repeat?.dayOfWeek || []
    }
    else if(column === PERIOD) {
      return medication?.dosageInstruction?.[0]?.timing?.repeat?.period || -Infinity
    }
    else if(column === PERIOD_UNIT) {
      return medication?.dosageInstruction?.[0]?.timing?.repeat?.periodUnit || nil
    }
    else if(column === REQUESTER) {
      return medication?.requester?.display || nil;
    }
    else if(column === AUTHORED_ON) {
      return medication?.authoredOn || nil
    }
  }

  sort(column:string) {
    if (this.sortKey === column) {
      this.sortDirection *= -1;
    } else {
      this.sortKey = column;
      this.sortDirection = 1;
    }
    this.medications.sort((a:any, b:any) => {
      const valA = this.getColumn(a, column);
      const valB = this.getColumn(b, column);
      if (valA > valB) {
        return this.sortDirection;
      } else if (valA < valB) {
        return -this.sortDirection;
      }
      return 0;
    });
  }

  getWhenToUse(medication: fhir.MedicationRequest) {
    const timeOfDay = medication?.dosageInstruction?.[0]?.timing?.repeat?.timeOfDay
    const when = medication?.dosageInstruction?.[0]?.timing?.repeat?.when;
    const dayOfWeek = medication?.dosageInstruction?.[0]?.timing?.repeat?.dayOfWeek

    if(timeOfDay) {
      return timeOfDay;
    }
    if(when) {
      let newArr: string[] = [];
      for(let i=0; i<when.length; i++) {
        newArr.push(this.valueSetsService.whenSet[when[i]])
      }
      return newArr;
    }
    if(dayOfWeek) {
      return dayOfWeek;
    }
    return "--"
  }

  bundleLoader() {
    this.fhirService.getMedicationBundle(this.translate.currentLang).subscribe({
      next: res => {
        this.medicationBundle = res;
      }
    })
  }

  initializeFilterList() {
    this.medicationFilterMode = true;

    if(this.query.medicationName === "") {
      this.filterList = []

      for(let i=0; i < this.medicationBundle.expansion.total; i++) {
        this.filterList.push(this.medicationBundle.expansion.contains[i]);
      }
    }
    else {
      this.onSearchChange()
    }
  }

  closeMedicationFilterMode() {
    this.medicationFilterMode = false;
  }

  onSearchChange(): void {
    this.filterList = [];

    for(let i=0; i < this.medicationBundle.expansion.total; i++) {
      const medicationName = this.medicationBundle.expansion.contains[i].display
      if(medicationName.toLowerCase().includes(this.query.medicationName.trim().toLowerCase())) {
        this.filterList.push(this.medicationBundle.expansion.contains[i])
      }
    }
  }

  onLanguageChange() {
    this.translate.use(this.currentLanguage)

    // UPDATE TABLE HEADERS, VALUE SETS, AND BUNDLE
    this.setTableHeaders();
    this.valueSetsService.updateSets();
    this.bundleLoader();
  }

  selectValue(display: string, code: string): void {
    this.query.medicationName = display;
    this.query.code = code;

    this.medicationFilterMode = false;
  }

  applyMedicationFilter() {
    this.currentPage = 1;

    if(this.query.medicationName === "") {
      this.filters.filterMedication = ""
    }
    else {
      this.filters.filterMedication = this.query.code;
    }
    this.getMedicationRequests();
  }

  clearMedicationFilter() {
    this.medicationFilterMode = false;
    this.filterList = [];
    this.filters.filterMedication = "";
    this.query.medicationName = "";
    this.query.code = "";
    this.currentPage = 1;

    this.getMedicationRequests();
  }

  ngOnInit(): void {}
}
