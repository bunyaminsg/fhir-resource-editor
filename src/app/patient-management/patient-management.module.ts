import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PatientManagementComponent} from "./patient-management.component";
import {PatientObservationListComponent} from "./patient-observation-list/patient-observation-list.component";
import {PatientObservationCreateComponent} from "./patient-observation-create/patient-observation-create.component";
import {
  PatientFamilyMemberHistoryListComponent
} from "./patient-family-member-history-list/patient-family-member-history-list.component";
import {
  PatientFamilyMemberHistoryCreateComponent
} from "./patient-family-member-history-create/patient-family-member-history-create.component";
import {MedicationRequestsComponent} from "./medication-requests/medication-requests.component";
import {
  MedicationAddOrEditComponent
} from "./medication-requests/medication-add-or-edit/medication-add-or-edit.component";
import {ConditionsComponent} from "./conditions/conditions.component";
import {PatientManagementRoutingModule} from "./patient-management-routing.module";
import {NgxPaginationModule} from "ngx-pagination";
import { GoalsComponent } from './goals/goals.component';
import { AddgoalComponent } from './goals/addgoal/addgoal.component';
import { AppointmentsComponent } from './appointments/appointments.component';
import { AddAppointmentComponent } from './add-appointment/add-appointment.component';
import {TranslateModule} from "@ngx-translate/core";
import {ConditionAddOrEditComponent} from "./conditions/condition-add-or-edit/condition-add-or-edit.component";

@NgModule({
  declarations: [
    PatientManagementComponent,
    PatientObservationListComponent,
    PatientObservationCreateComponent,
    PatientFamilyMemberHistoryListComponent,
    PatientFamilyMemberHistoryCreateComponent,
    MedicationRequestsComponent,
    MedicationAddOrEditComponent,
    ConditionsComponent,
    ConditionAddOrEditComponent,
    GoalsComponent,
    AddgoalComponent,
    AppointmentsComponent,
    AddAppointmentComponent
  ],
  imports: [
    CommonModule,
    PatientManagementRoutingModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule
  ]
})
export class PatientManagementModule { }
