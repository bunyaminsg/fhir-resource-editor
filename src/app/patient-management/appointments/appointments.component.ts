import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FhirService} from "../../services/fhir/fhir.service";
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-goals',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.scss']
})
export class AppointmentsComponent implements OnInit {
  patient!: fhir.Patient;
  appointments!: fhir.Appointment[];
  currentPage: number = 1;
  totalPages!: number;
  pid!: string;
  selectedSortOption!: string;
  searchStartDate!: string;
  searchEndDate!: string;

  constructor(private route: ActivatedRoute, private fhirService: FhirService, private router: Router, private translate: TranslateService ) {
    this.translate.setDefaultLang('en');
    this.selectedSortOption = '';
    this.route.params.subscribe(params => {
      if (params['pid']) {
        this.pid = params['pid'];
        this.fhirService.getPatientAppointments(this.pid, this.currentPage, this.selectedSortOption, this.searchStartDate, this.searchEndDate).subscribe({
          next: appointments => {
            console.log(appointments);
            if(appointments.total && appointments.entry?.length){
              this.totalPages = Math.ceil(appointments.total / appointments.entry?.length);
            }
            this.appointments = appointments.entry!.map(entry => entry.resource as fhir.Appointment);
          }
        })
        }
        })
        }

  ngOnInit(): void {
  }

  getAppointments(){
    if(this.pid){
      this.fhirService.getPatientAppointments(this.pid, this.currentPage, this.selectedSortOption, this.searchStartDate, this.searchEndDate).subscribe({
        next: appointments => {
          if(appointments.total && appointments.entry!.length){
            this.totalPages = Math.ceil(appointments.total / 20) || 1;
          }else{
            this.totalPages = 1;
          }
          this.appointments = appointments.entry!.map(entry => entry.resource as fhir.Appointment);
        }
      })
    }
  }

  updateAppointment(appointment: fhir.Appointment) {
    this.router.navigate(['patient/' + this.pid + '/appointments/edit/' + appointment.id]);
  }

  addAppointment() {
    this.router.navigate(['patient/' + this.pid + '/appointments/edit']);
  }

  deleteAppointment(aid: string) {
    const deleteConfirmation = this.translate.instant('deleteConfirmation');
    if (window.confirm(deleteConfirmation)) {
      if(this.appointments){
        const index = this.appointments.findIndex(appointment => appointment.id === aid);
        if (index !== -1) {
          this.appointments.splice(index, 1);
        }
        this.fhirService.deleteAppointment<fhir.Appointment>(aid).subscribe();
      }
  }
}

getPageNumbers() {
  const pages: number[] = [];
  if(this.totalPages){
  for (let i = 1; i <= this.totalPages; i++) {
    pages.push(i);
  }}
  return pages;
}

prevPage() {
  if(this.currentPage > 1){
    this.currentPage--;
    this.getAppointments();
  }}

nextPage() {
    if(this.totalPages && this.currentPage < this.totalPages){
      this.currentPage++;
      this.getAppointments();
    }
}

gotoPage(page: number) {
  this.currentPage = page;
  this.getAppointments();
}

sortAppointments(selectedSortOption: string = this.selectedSortOption) {
  if(this.pid){
    this.selectedSortOption = selectedSortOption;
    this.getAppointments();
  }
}

clearFilters(){
  this.searchStartDate = '';
  this.searchEndDate = '';
  this.getAppointments();
}

changeLanguage(lang: string) {
  this.translate.use(lang);
}
}
