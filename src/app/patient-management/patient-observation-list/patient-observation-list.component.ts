import { Component, OnInit, HostListener } from '@angular/core';
import {FhirService} from "../../services/fhir/fhir.service";
import {ActivatedRoute} from "@angular/router";
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-patient-observation-list',
  templateUrl: './patient-observation-list.component.html',
  styleUrls: ['./patient-observation-list.component.scss']
})
export class PatientObservationListComponent implements OnInit {

  // DEFAULT LANGUAGE
  selectedLanguage!: string; 

  // Arrays 
  observations: fhir.Observation[] = [];
  
  // Patient info
  patient!: fhir.Patient
  patientName!: string
  patientID!: string 
  
  isEffectiveDateTimeClicked: boolean = false;
  isClicked: boolean = false;

  currentPage: number =  1;
  totalPageNumber: number  = 99;


  filterStatus!: string;

  // Variables for code filter
  searchCode!: string;
  date1! : string ;
  date2! : string ;
  searchTerm: string = '';
  filterList: any = [];
  savefilterList: any = [];
  allObservations: any[] = [];
  showResults: boolean = false;
  counter: number = 0;

  // Filter object
  finalFilter = {
    statusFilter: "",
    date1Filter: "",
    date2Filter: "",
    codeFilter:""
  }

  constructor(private route: ActivatedRoute, private fhirService: FhirService, private r : Router, public translate: TranslateService) {
    this.translate.setDefaultLang("en");
    this.route.params.subscribe(params => {
      if (params['pid']) {
        this.fhirService.getResource<fhir.Patient>('Patient', params['pid']).subscribe({
          next: patient => {
            this.patient = patient

            const name = this.patient?.name?.map(name => [...(name.given || []), name.family].join(' ')).pop();
            this.patientName = name ? name : "";

            const id = this.patient.id
            this.patientID = id ? id : "";

            this.getObservations();
          },
          error: console.error
        })
      }
    })

    this.bundleFilterInit();
  }


  getObservations(){
    
    if(this.date1 && this.date2){
      this.finalFilter.date1Filter = this.date1;
      this.finalFilter.date2Filter = this.date2;

    }

    if(this.filterStatus ||  this.filterStatus === ""){
      this.finalFilter.statusFilter = this.filterStatus;
    }
    
    if(this.searchCode || this.searchCode === "" ){
      this.finalFilter.codeFilter = this.searchCode;

    }

    this.fhirService.getObservation(this.patientID, this.currentPage,this.finalFilter).subscribe((observations) => {
      this.observations = observations.entry?.map((entry) => <fhir.Observation>entry.resource) || [];

      let number = observations.total;
      if (number || number === 0) {
        this.totalPageNumber = Math.ceil(number / 20);
      }
    });
  }

  applyStatusFilter() {
    this.currentPage = 1; // Reset the current page to the first page when applying the filter
    this.getObservations();
  }

  applyDateFilter() {
    this.currentPage = 1; // Reset the current page to the first page when applying the filter
    this.getObservations();
  }

  applyCodeFilter() {
    this.currentPage = 1; // Reset the current page to the first page when applying the filter
    this.getObservations();
  }

  changePage(para: number){
    this.currentPage = para;
    this.getObservations();
  }

  clearDates() {
    this.date1 = "";
    this.date2 = "";
    this.finalFilter.date1Filter ="";
    this.finalFilter.date2Filter ="";
    this.applyDateFilter(); // Call the method to filter data again after resetting dates
  }

  removeObservation(obsID: any, index: any) {
    if (window.confirm(this.translate.instant('confirmDelete'))) {
      this.fhirService.deleteObservation(obsID).subscribe(
        () => {
          this.observations.splice(index, 1);

          alert(this.translate.instant('deleteSuccess'));
        },
        () => {

          alert(this.translate.instant('deleteError'));
        }
      );
    }
  }  

  bundleFilterInit(): void {
    this.fhirService.getBundleForCodeFilter().subscribe({
      next: (data: any) => {
        this.allObservations = data['entry'][0].resource.entry;
        this.updateFilterList();
      }
    });
  }

  
  updateFilterList(): void {
    this.filterList = [];
    
    this.filterList = this.allObservations.filter((observation: any) =>
      observation.resource.compose.include.some((include: any) =>
        include.concept.some((concept: any) =>
          this.getConceptValue(concept).toLowerCase().includes(this.searchTerm.trim().toLowerCase())
        )
      )
    );
    this.savefilterList = this.filterList;
  }
  /*
    updateFilterList(): void {
    this.filterList = [];
    this.filterList = this.allObservations.filter((observation: any) =>
      observation.resource.compose.include.some((include: any) =>
        include.concept.some((concept: any) =>
          concept.display.toLowerCase().includes(this.searchTerm.trim().toLowerCase())
        )
      )
    );
    this.savefilterList = this.filterList;
  }
 
  */

  getConceptValue(concept: any): string {
    const conceptDesignations = concept.designation;
    const selectedLanguageValue = conceptDesignations.find((designation: any) => designation.language ===  this.translate.currentLang);
    
    return selectedLanguageValue ? selectedLanguageValue.value : concept.display;
  }


  onSearchBoxClick(): void {

    this.showResults = true; 
    if(this.counter>0){
      this.onSearchChange();
    }
    this.counter++;
  }

  onSearchChange(): void {
    if (this.searchTerm.trim() === '') {
      //this.filterList = []; // Clear the filterList when the input is empty
    } else {
      this.updateFilterList();
    }
  }

  selectValue(value: string, value2: string, event: Event): void {
    const clickedElement = event.target as HTMLElement;
    const dropdownList = document.querySelector('.dropdown-list');
    
    if (dropdownList && dropdownList.contains(clickedElement)) {

      this.searchTerm = value;
      this.searchCode = value2;
      this.showResults = false;
    }
  }

  clearSearch(): void {
    this.showResults = false; 
  }

  clearNoFilter(): void{
    this.searchTerm = '';
    this.searchCode = '';
    this.updateFilterList();
    this.getObservations();
  }

  ngOnInit(){
    this.filterStatus = "";
    this.date1 = "";
    this.date2 = "";
    this.searchCode = "";
    this.searchTerm = '';
    this.getObservations();
  }

  toggleEffectiveDateTimeClicked() {
    this.isEffectiveDateTimeClicked = !this.isEffectiveDateTimeClicked;
  }

  changeLanguage(language: string) {

    alert("Language change");
    this.selectedLanguage = language;

    //this.localizationService.setLanguage(language);
    this.translate.use(language);

    
    const patientId = this.route.snapshot.params['pid'];
    const updatedUrl = `/patient/${patientId}/observation`;
    this.r.navigateByUrl(updatedUrl);

  }


  sort(sortBy: string) {
    
    this.toggleEffectiveDateTimeClicked();

    this.observations.sort((a: any, b: any) => {
      const propA = a[sortBy];
      const propB = b[sortBy];
      
      if(propA>propB ){
        if(this.isEffectiveDateTimeClicked){
          return -1;
        }
        else{
          return 1;
        }

      }
      else{
        if(this.isEffectiveDateTimeClicked){
          return 1;
        }
        else{
          return -1;
        }
      }
  

    });
    
  }

}
