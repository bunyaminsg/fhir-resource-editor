import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientObservationListComponent } from './patient-observation-list.component';

describe('PatientObservationListComponent', () => {
  let component: PatientObservationListComponent;
  let fixture: ComponentFixture<PatientObservationListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientObservationListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PatientObservationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
