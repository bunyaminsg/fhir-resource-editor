import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {FhirService} from "../services/fhir/fhir.service";

@Component({
  selector: 'app-patient-management',
  templateUrl: './patient-management.component.html',
  styleUrls: ['./patient-management.component.scss']
})
export class PatientManagementComponent implements OnInit {
  patient: fhir.Patient | undefined
  patientName: string | undefined

  constructor(private route: ActivatedRoute, private fhirService: FhirService) {
    this.route.params.subscribe(params => {
      if (params['pid']) {
        this.fhirService.getResource<fhir.Patient>('Patient', params['pid']).subscribe({
          next: patient => {
            this.patient = patient
            this.patientName = this.patient?.name?.map(name => [...(name.given || []), name.family].join(' ')).pop();
          },
          error: console.error
        })
      }
    })
  }

  ngOnInit(): void {
  }

}
