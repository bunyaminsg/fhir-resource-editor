import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { FhirService } from 'src/app/services/fhir/fhir.service';
import { ActivatedRoute, Router } from '@angular/router';
import { OnauthService } from 'src/app/services/onauth.service';
import { ArrayStorageService } from 'src/app/services/array-storage.service';
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-add-appointment',
  templateUrl: './add-appointment.component.html',
  styleUrls: ['./add-appointment.component.scss'],
})
export class AddAppointmentComponent implements OnInit {
  timezones: string[] = [];
  addappointmentForm!: FormGroup;
  pid!: string;
  aid!: string;
  updatemode: boolean = false;
  showSuccessMessage: boolean = false;
  updateModeString = 'addAppointment';

  /*Uncomment the commented areas to enable timezone offset functionality*/

  constructor(
    private formBuilder: FormBuilder,
    private fhirService: FhirService,
    private route: ActivatedRoute,
    private onauthService: OnauthService,
    private router: Router,
    private arrayStorageService: ArrayStorageService
  ) {
    this.route.params.subscribe((params) => {
      if (params['pid']) {
        this.pid = params['pid'];
      }
      if (params['aid']) {
        this.updatemode = true;
        this.aid = params['aid'];
        this.updateModeString = 'updateAppointment';
        this.updateAppointment(params['aid']);
      }
    });
    this.mainForm();
  }

  ngOnInit() {
    this.timezones = moment.tz.names();
  }

  mainForm() {
    this.addappointmentForm = this.formBuilder.group({
      appointmentDescription: ['', Validators.required],
      appointmentStartDate: ['', Validators.required],
      appointmentStartTime: ['', Validators.required],
      appointmentEndDate: ['', Validators.required],
      appointmentEndTime: ['', Validators.required],
      patientStatus: ['', Validators.required],
      doctorStatus: ['', Validators.required],
      appointmentStatus: ['', Validators.required],
      /*timezoneHours: [0, Validators.required],
      timezoneMinutes: [0, Validators.required],*/
    });
  }

  onSubmit() {
    /*const hours = this.addappointmentForm.value.timezoneHours || 0;
    const minutes = this.addappointmentForm.value.timezoneMinutes || 0;

    const formattedHours = hours.toString().padStart(2, '0');
    const formattedMinutes = minutes.toString().padStart(2, '0');

    const timezoneOffset = formattedHours + ':' + formattedMinutes;*/

    if (this.addappointmentForm.valid) {
      const appointment: fhir.Appointment = {
        resourceType: 'Appointment',
        description: this.addappointmentForm.value.appointmentDescription,
        /*Remove the 'Z' from the end of the start and end dates to enable timezone offset functionality*/
        start:
          this.addappointmentForm.value.appointmentStartDate +
          'T' +
          this.addappointmentForm.value.appointmentStartTime +
          ':00' +
          'Z' /*'+' + timezoneOffset*/,
        end:
          this.addappointmentForm.value.appointmentEndDate +
          'T' +
          this.addappointmentForm.value.appointmentEndTime +
          ':00' +
          'Z' /*'+' + timezoneOffset*/,
        status: this.addappointmentForm.value.appointmentStatus,
        participant: [
          {
            actor: {
              reference: 'Patient/' + this.pid,
            },
            status: this.addappointmentForm.value.patientStatus,
          },
          {
            actor: {
              reference: 'Practitioner/' + this.onauthService.userInfo?.sub,
              display: this.onauthService.userInfo?.preferred_username,
            },
            status: this.addappointmentForm.value.doctorStatus,
          },
        ],
      };
      this.fhirService.addAppointment(appointment).subscribe((res) => {
        this.showSuccessMessage = true;
        this.addappointmentForm.reset();
      });
    }
  }

  updateAppointment(aid: string) {
    this.fhirService
      .getResource<fhir.Appointment>('Appointment', aid)
      .subscribe((res) => {
        const appointment = res;
        this.addappointmentForm.patchValue({
          appointmentDescription: appointment.description,
          appointmentStartDate: appointment.start?.substring(0, 10),
          appointmentStartTime: appointment.start?.substring(11, 16),
          appointmentEndDate: appointment.end?.substring(0, 10),
          appointmentEndTime: appointment.end?.substring(11, 16),
          patientStatus: appointment.participant?.at(0)?.status,
          doctorStatus: appointment.participant?.at(1)?.status,
          appointmentStatus: appointment.status,
        });
      });
  }
}
