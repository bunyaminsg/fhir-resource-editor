import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConditionAddOrEditComponent } from './condition-add-or-edit.component';

describe('ConditionAddOrEditComponent', () => {
  let component: ConditionAddOrEditComponent;
  let fixture: ComponentFixture<ConditionAddOrEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConditionAddOrEditComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConditionAddOrEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
