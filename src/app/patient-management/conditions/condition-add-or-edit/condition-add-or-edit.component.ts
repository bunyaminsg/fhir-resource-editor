import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {FhirService} from "../../../services/fhir/fhir.service";
import {OnauthService} from "../../../services/onauth.service";
import {ValueSetsService} from "../../../services/value-sets.service";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-condition-add-or-edit',
  templateUrl: './condition-add-or-edit.component.html',
  styleUrls: ['./condition-add-or-edit.component.scss']
})
export class ConditionAddOrEditComponent implements OnInit {
  editOrAdd : boolean = false;
  conditionForm : FormGroup;
  submitted : boolean = false;

  patientName: string | undefined
  patient: fhir.Patient | undefined
  patientID: string | undefined

  // FOR EDITING
  condition: any;

  //------------------ BOUND VARIABLES ---------------
  conditionDisplay: string = ""
  conditionCode: string = ""
  conditionSystem: string = ""
  categoryDisplay: string = ""
  categoryCode: string = ""
  categorySystem: string = "http://terminology.hl7.org/CodeSystem/condition-category" // DEFAULT
  clinicalStatus: string = ""
  verificationStatus: string = ""
  onSetDateTime: string = ""
  clinicalStatusSystem: string = "http://terminology.hl7.org/CodeSystem/condition-clinical" // DEFAULT
  verificationStatusSystem: string = "http://terminology.hl7.org/CodeSystem/condition-ver-status" // DEFAULT

  // FOR CONDITION FILTER
  conditionBundle: any
  conditionBundleHelper: any = []
  filterList: any = [];
  conditionFilterMode: boolean = false;

  currentLanguage!: string

  languageOptions = [
    { language: "en",  display: "English" },
    { language: "de",  display: "German" },
    { language: "tr",  display: "Turkish" },
    { language: "es",  display: "Spanish" },
  ];
  constructor(private fb: FormBuilder,
              private route: ActivatedRoute,
              private fhirService: FhirService,
              private router: Router,
              private onAuthService: OnauthService,
              public valueSetsService: ValueSetsService,
              public translate: TranslateService) {
    if(this.translate.currentLang) {
      this.currentLanguage = this.translate.currentLang;
    }
    else {
      this.translate.setDefaultLang("en")
      this.translate.use("en");
      this.currentLanguage = "en";
    }

    this.route.params.subscribe(params => {
      if (params['pid']) {
        this.fhirService.getResource<fhir.Patient>('Patient', params['pid']).subscribe({
          next: patient => {
            this.patient = patient
            this.patientName = this.patient?.name?.map(name => [...(name.given || []), name.family].join(' ')).pop();
            this.patientID = this.patient.id;

            this.bundleLoader();
            },
          error: console.error
        })
      }
    })

    this.conditionForm = this.fb.group({
      conditionDisplay: ['', Validators.required],
      conditionCode: ['', Validators.required],
      conditionSystem: ['', Validators.required],
      categoryDisplay: ['', Validators.required],
      categoryCode: ['', Validators.required],
      categorySystem: ['', Validators.required],
      clinicalStatus: ['', Validators.required],
      verificationStatus: ['', Validators.required],
      onSetDateTime: ['', Validators.required],
      clinicalStatusSystem: ['', Validators.required],
      verificationStatusSystem: ['', Validators.required],
    })
  }

  ngOnInit(): void {
    if(this.route.snapshot.url[3]) {
      this.editOrAdd = true;

      this.getConditionToEdit();
    }
    else {
      this.editOrAdd = false;
    }
  }

  get myForm() {
    return this.conditionForm.controls;
  }

  getConditionToEdit() {
    this.fhirService.getOneCondition(this.route.snapshot.url[3].toString())
      .subscribe({
        next: (data: any) => {
          this.condition = <fhir.Condition> data;

          this.categoryDisplay = this.condition.category[0].coding[0].display;
          this.categoryCode = this.condition.category[0].coding[0].code;
          this.categorySystem = this.condition.category[0].coding[0].system;

          this.conditionDisplay = this.condition.code.coding[0].display;
          this.conditionSystem = this.condition.code.coding[0].system;
          this.conditionCode = this.condition.code.coding[0].code;

          this.verificationStatus = this.condition.verificationStatus.coding[0].code;
          this.verificationStatusSystem = this.condition.verificationStatus.coding[0].system;

          this.clinicalStatus = this.condition.clinicalStatus.coding[0].code;
          this.clinicalStatusSystem = this.condition.clinicalStatus.coding[0].system;

          this.onSetDateTime = this.condition.onsetDateTime.slice(0,-8)

          // TO DO
        },
        error: (err: any) => {
          console.error(err);
        }
      })
  }

  bundleLoader() {
    this.fhirService.getConditionBundle().subscribe({
      next: bundle => {
        this.conditionBundle = bundle;
        this.bundleFacilitator();
      }
    })
  }

  // CALL THAT WHENEVER THE CURRENT LANGUAGE CHANGES
  // THIS FUNCTION PUTS SYSTEMS, CODES AND DISPLAYS IN A SINGLE ARRAY
  bundleFacilitator() {
    this.conditionBundleHelper = [];

    for(let i = 0; i < this.conditionBundle.entry[0].resource.entry.length; i++) {
      let system = this.conditionBundle.entry[0].resource.entry[i].resource.compose.include[0].system;

      for(let j=0;
          j < this.conditionBundle.entry[0].resource.entry[i].resource.compose.include[0].concept.length; j++) {
        let display = "";
        let code = this.conditionBundle.entry[0].resource.entry[i].resource.compose.include[0].concept[j].code;
        let isParticularLanguageFound = false;

        for (let k=0;
             k < this.conditionBundle.entry[0].resource.entry[i].resource.compose.include[0].concept[j].designation.length; k++) {

          if(this.translate.currentLang ===
            this.conditionBundle.entry[0].resource.entry[i].resource.compose.include[0].concept[j].designation[k].language) {
            display = this.conditionBundle.entry[0].resource.entry[i].resource.compose.include[0].concept[j].designation[k].value
            isParticularLanguageFound = true;
            break;
          }
        }

        if(!isParticularLanguageFound) {
          display = this.conditionBundle.entry[0].resource.entry[i].resource.compose.include[0].concept[j].display;
        }

        let element = {
          system: system,
          code: code,
          display: display
        }

        this.conditionBundleHelper.push(element);
      }
    }
  }

  initializeFilterList() {
    this.conditionFilterMode = true;

    if(this.conditionDisplay === "") {
      this.filterList = this.conditionBundleHelper;
    }
    else {
      this.onSearchChange()
    }
  }

  closeConditionFilterMode() {
    this.conditionFilterMode = false;
  }

  onSearchChange(): void {
    this.filterList = [];

    if(this.conditionDisplay === "") {
      this.conditionCode = "";
      this.conditionSystem = "";
    }

    for(let i=0; i<this.conditionBundleHelper.length; i++) {
      if(this.conditionBundleHelper[i].display.toLowerCase().includes(this.conditionDisplay.toLowerCase())) {
        this.filterList.push(this.conditionBundleHelper[i])
      }
    }
  }

  selectValue(display: string, code: string, system: string): void {
    this.conditionDisplay = display;
    this.conditionCode = code;
    this.conditionSystem = system;

    this.conditionFilterMode = false;
  }

  getToday() {
    const currentDate = new Date();
    const year = currentDate.getFullYear();
    const month = (currentDate.getMonth() + 1).toString().padStart(2, '0');
    const day = currentDate.getDate().toString().padStart(2, '0');

    return `${year}-${month}-${day}`;
  }

  onLanguageChange() {
    this.translate.use(this.currentLanguage)
    this.valueSetsService.updateSets();
    this.bundleFacilitator();
  }

  onSubmit() {
    this.submitted = true;

    if(!this.conditionForm.valid) {
      return;
    }
    else {
      let condition: fhir.Condition = {
        resourceType: "Condition",
        clinicalStatus: {
          coding: [
            {
              system: this.clinicalStatusSystem,
              code: this.clinicalStatus
            }
          ]
        },
        verificationStatus: {
          coding: [
            {
              system: this.verificationStatusSystem,
              code: this.verificationStatus
            }
          ]
        },
        category: [
          {
            coding: [
              {
                system: this.categorySystem,
                code: this.categoryCode,
                display: this.categoryDisplay
              }
            ]
          }
        ],
        code: {
          coding: [
            {
              system: this.conditionSystem,
              code: this.conditionCode,
              display: this.conditionDisplay
            }
          ]
        },
        subject: {
          reference: "Patient/" + this.patientID,
        },

        onsetDateTime: this.onSetDateTime + ":00.000Z"
      }

      // KEEP RECORDED DATE AND ASSERTER IF IT IS IN EDIT MODE
      if(this.editOrAdd) {
        condition.recordedDate = this.condition.recordedDate;
        condition.asserter = this.condition.asserter;
      }

      // ADD RECORDED DATE AND ASSERTER IF IT IS IN ADD MODE
      else {
        condition.recordedDate = this.getToday();
        condition.asserter = {
          reference: "Practitioner/" + this.onAuthService.userInfo?.sub,
          display: this.onAuthService.userInfo?.given_name + " " + this.onAuthService.userInfo?.family_name
        }
      }

      // ADD THE CONDITION
      if(!this.editOrAdd) {
        this.fhirService.addCondition(condition)
          .subscribe({
            next: () => {
              window.alert(this.translate.instant("conditionAddSuccess"))
              this.router.navigateByUrl("/patient/" + this.patientID+ "/conditions");
            },
            error: () => {
              window.alert(this.translate.instant("conditionAddFail"))
            }
          })
      }

      // UPDATE THE CONDITION
      else {
        condition.id = this.route.snapshot.url[3].toString();

        this.fhirService.updateCondition(condition, this.route.snapshot.url[3].toString())
          .subscribe({
            next: () => {
              window.alert(this.translate.instant("conditionUpdateSuccess"))
              this.router.navigateByUrl("/patient/" + this.patientID+ "/conditions");
            },
            error: () => {
              window.alert(this.translate.instant("conditionUpdateFail"))
            }
          })
      }
    }
  }
}
