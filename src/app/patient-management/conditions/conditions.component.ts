import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {FhirService} from "../../services/fhir/fhir.service";
import {ValueSetsService} from "../../services/value-sets.service";
import {TranslateService} from "@ngx-translate/core";

// SOME CONSTANTS
const CATEGORY = "category"
const CONDITION_CODE_DISPLAY = 'conditionCodeDisplay'
const CONDITION_CODE_CODE = 'conditionCodeCode'
const CLINICAL_STATUS = 'clinicalStatus'
const VERIFICATION_STATUS = 'verificationStatus'
const ON_SET_DATE_TIME = 'onSetDateTime'
const RECORDED_DATE = 'recordedDate'

@Component({
  selector: 'app-conditions',
  templateUrl: './conditions.component.html',
  styleUrls: ['./conditions.component.scss']
})
export class ConditionsComponent implements OnInit {
  patientName: string | undefined
  patient: fhir.Patient | undefined
  patientID: string | undefined

  conditions : fhir.Condition[] = []

  currentPage : number = 1;
  totalPages: number = 1;
  sortKey: string = '';
  sortDirection: number = 1;

  filters = {
    clinicalStatus: "",
    verificationStatus: "",
    conditionCode: "",
  }

  // FOR CONDITION FILTER
  conditionBundle: any
  conditionBundleHelper: any = []
  query = {
    conditionDisplay: "",
    conditionCode: ""
  };
  filterList: any = [];
  conditionFilterMode: boolean = false;

  // ngContainer headers
  tableHeaders! : any

  currentLanguage!: string

  languageOptions = [
    { language: "en",  display: "English" },
    { language: "de",  display: "German" },
    { language: "tr",  display: "Turkish" },
    { language: "es",  display: "Spanish" },
  ];

  constructor(private route: ActivatedRoute,
              private fhirService: FhirService,
              public valueSetsService: ValueSetsService,
              public translate: TranslateService) {
    if(this.translate.currentLang) {
      this.currentLanguage = this.translate.currentLang;
    }
    else {
      this.translate.setDefaultLang("en")
      this.translate.use("en");
      this.currentLanguage = "en";
    }

    this.route.params.subscribe(params => {
    if (params['pid']) {
      this.fhirService.getResource<fhir.Patient>('Patient', params['pid']).subscribe({
        next: patient => {
          this.patient = patient
          this.patientName = this.patient?.name?.map(name => [...(name.given || []), name.family].join(' ')).pop();
          this.patientID = this.patient.id;
          this.getConditions();
          this.bundleLoader();
          this.setTableHeaders();
        },
        error: console.error
      })
    }
  })}

  getConditions() {
    this.fhirService.getConditions(this.patientID, this.currentPage, this.filters)
      .subscribe({
        next: conditions => {
          this.conditions = conditions.entry?.map(entry => <fhir.Condition>entry.resource)
            || [];

          const tmp = conditions.total;
          if(tmp) {
            this.totalPages = Math.ceil(tmp / 20);
          }
        }
      })
  }

  sendFilteredRequest() {
    this.currentPage = 1;
    this.getConditions();
  }

  deleteCondition(condition: any, index: number) {
    if(window.confirm(this.translate.instant("deleteConfirmCondition"))) {
      this.fhirService.deleteCondition(condition?.id).subscribe(() => {
        this.conditions.splice(index, 1);
      })
    }
  }

  setTableHeaders() {
    this.tableHeaders = [
      { key: CATEGORY, label: this.translate.instant("category") },
      { key: CONDITION_CODE_DISPLAY, label: this.translate.instant("conditionDisplay") },
      { key: CONDITION_CODE_CODE, label: this.translate.instant("conditionCode") },
      { key: CLINICAL_STATUS, label: this.translate.instant("clinicalStatus") },
      { key: VERIFICATION_STATUS, label: this.translate.instant("verificationStatus") },
      { key: ON_SET_DATE_TIME, label: this.translate.instant("onsetDate") },
      { key: RECORDED_DATE, label: this.translate.instant("recordedDate") },
    ];
  }

  onLanguageChange() {
    this.translate.use(this.currentLanguage)
    this.setTableHeaders();
    this.valueSetsService.updateSets();
    this.bundleFacilitator();
  }


  goToPage(pageNumber: number) {
    this.currentPage = pageNumber;
    this.sortKey = "";
    this.getConditions();
  }

  getColumn(condition:any, column:string) {
    // nil is used for better sorting
    const nil = "ZZZZZZZZZZZZZ";

    if(column === CATEGORY) {
      return condition?.category?.[0]?.coding?.[0]?.display || nil
    }
    else if(column === CONDITION_CODE_DISPLAY) {
      return condition?.code?.coding?.[0]?.display || nil
    }
    else if(column === CONDITION_CODE_CODE) {
      return condition?.code?.coding?.[0]?.code || nil
    }
    else if(column === CLINICAL_STATUS) {
      return condition?.clinicalStatus?.coding?.[0]?.code || nil
    }
    else if(column === VERIFICATION_STATUS) {
      return condition?.verificationStatus?.coding?.[0]?.code || nil
    }
    else if(column === ON_SET_DATE_TIME) {
      return condition?.onsetDateTime || nil
    }
    else if(column === RECORDED_DATE) {
      return condition?.recordedDate || nil
    }
  }

  sort(column: string) {
    if (this.sortKey === column) {
      this.sortDirection *= -1;
    } else {
      this.sortKey = column;
      this.sortDirection = 1;
    }

    this.conditions.sort((a:any, b:any) => {
      const valA = this.getColumn(a, column);
      const valB = this.getColumn(b, column);
      if (valA > valB) {
        return this.sortDirection;
      } else if (valA < valB) {
        return -this.sortDirection;
      }
      return 0;
    });
  }
  bundleLoader() {
    this.fhirService.getConditionBundle().subscribe({
      next: bundle => {
        this.conditionBundle = bundle;
        this.bundleFacilitator();
      }
    })
  }

  // CALL THAT WHENEVER THE CURRENT LANGUAGE CHANGES
  // THIS FUNCTION PUTS SYSTEMS, CODES AND DISPLAYS IN A SINGLE ARRAY
  bundleFacilitator() {
    this.conditionBundleHelper = [];

    for(let i = 0; i < this.conditionBundle.entry[0].resource.entry.length; i++) {
      let system = this.conditionBundle.entry[0].resource.entry[i].resource.compose.include[0].system;

      for(let j=0;
          j < this.conditionBundle.entry[0].resource.entry[i].resource.compose.include[0].concept.length; j++) {
        let display = "";
        let code = this.conditionBundle.entry[0].resource.entry[i].resource.compose.include[0].concept[j].code;
        let isParticularLanguageFound = false;

        for (let k=0;
             k < this.conditionBundle.entry[0].resource.entry[i].resource.compose.include[0].concept[j].designation.length; k++) {

          if(this.translate.currentLang ===
            this.conditionBundle.entry[0].resource.entry[i].resource.compose.include[0].concept[j].designation[k].language) {
            display = this.conditionBundle.entry[0].resource.entry[i].resource.compose.include[0].concept[j].designation[k].value
            isParticularLanguageFound = true;
            break;
          }
        }

        if(!isParticularLanguageFound) {
          display = this.conditionBundle.entry[0].resource.entry[i].resource.compose.include[0].concept[j].display;
        }

        let element = {
          system: system,
          code: code,
          display: display
        }

        this.conditionBundleHelper.push(element);
      }
    }
  }

  initializeFilterList() {
    this.conditionFilterMode = true;

    if(this.query.conditionDisplay === "") {
      this.filterList = this.conditionBundleHelper;
    }
    else {
      this.onSearchChange()
    }
  }

  closeConditionFilterMode() {
    this.conditionFilterMode = false;
  }

  onSearchChange(): void {
    this.filterList = [];

    for(let i=0; i<this.conditionBundleHelper.length; i++) {
     if(this.conditionBundleHelper[i].display.toLowerCase().includes(this.query.conditionDisplay.toLowerCase())) {
       this.filterList.push(this.conditionBundleHelper[i])
     }
    }
  }

  selectValue(display: string, code: string): void {
    this.query.conditionDisplay = display;
    this.query.conditionCode = code;

    this.conditionFilterMode = false;
  }

  applyConditionFilter() {
    this.currentPage = 1;

    if(this.query.conditionDisplay === "") {
      this.filters.conditionCode = ""
    }
    else {
      this.filters.conditionCode = this.query.conditionCode;
    }
    this.getConditions();
  }

  clearConditionFilter() {
    this.conditionFilterMode = false;
    this.filterList = [];
    this.filters.conditionCode = "";
    this.query.conditionDisplay = "";
    this.query.conditionCode = "";
    this.currentPage = 1;

    this.getConditions();
  }

  ngOnInit(): void {
  }

}
