import { Component, OnInit } from '@angular/core';
import {FhirService} from "../../services/fhir/fhir.service";
import {ActivatedRoute} from "@angular/router";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-patient-observation-create',
  templateUrl: './patient-observation-create.component.html',
  styleUrls: ['./patient-observation-create.component.scss']
})
export class PatientObservationCreateComponent implements OnInit {

  submitted = false;

  // Patient info to display
  patient: fhir.Patient | undefined
  patientName: string | undefined
  patientID: string | undefined

  //Observation create/edit form
  obsForm!: FormGroup;


  // boolean to display category options
  showInputs: boolean = false;

  // boolean to display code options
  showInputsCode: boolean = true;


  // variables for effective type
  showEffectiveInputs: boolean = false;
  effectiveType: 'dateTime' | 'period' = 'dateTime';


  // variables for value type
  valueType: string = 'boolean';
  showValueInputs: boolean = false;
  selectedValueType: 'boolean' | 'string' | 'quantity' | 'CodeableConcept' | '' = '';


  // boolean to display interpretation options
  showInterpretationInputs: boolean = false;


  // boolean to display reference range options
  showReferenceRangeInputs: boolean = false;



  // variables to be used in editing
  id: string | undefined;
  isEdit: boolean;
  obsEdit : fhir.Observation | undefined;


  //Code Search

  searchCode!: string;
  searchSystem!: string;
  date1! : string ;
  date2! : string ;
  searchTerm: string = '';
  filterList: any = [];
  savefilterList: any = [];
  allObservations: any[] = [];
  showResults: boolean = false;
  counter: number = 0;


  constructor(
    public fb: FormBuilder,
    private router: ActivatedRoute,
    private fhirService: FhirService,
    private route: ActivatedRoute,
    private r : Router,
    public translate: TranslateService)
    {

      // if url has an observation ID edit else create
      let editnumber = this.route.snapshot.paramMap.get('id') ;
      if(editnumber){
        this.id = editnumber;
      }

      if(this.id){
        this.isEdit = true;
      }
      else{
        this.isEdit = false;
      }

      this.bundleFilterInit();
    }



  ngOnInit(): void {
    this.mainForm();

    if(this.isEdit){
      this.getObservationForDefault(this.id);
    }
  }


  mainForm() {

    this.route.params.subscribe(params => {
      if (params['pid']) {
        this.fhirService.getResource<fhir.Patient>('Patient', params['pid']).subscribe({
          next: patient => {
            this.patient = patient
            this.patientName = this.patient?.name?.map(name => [...(name.given || []), name.family].join(' ')).pop();
            this.patientID = this.patient.id;

            this.obsForm.patchValue({
              subjectReference: "Patient/"+this.patientID,
              subjectDisplay: this.patientName,


              observationID: this.id //
            });
          },
          error: console.error
        })
      }
    })


    this.obsForm = this.fb.group({

      // Subject
      subjectReference: [{ value: '', disabled: true }],
      subjectDisplay: [{ value: '', disabled: true }],

      // Status
      status: ['registered', [Validators.required]], /// MUST HAVE


      // Code
      codeCode: ['', Validators.required],          /// MUST HAVE
      codeDisplay: ['', Validators.required],
      codeSystem: ['', Validators.required],


      // Category
      categoryCode: [''],
      categoryDisplay: [''],
      categorySystem: [''],


      // Effective[x]
      effectiveDateTime: [''], // Control for Effective DateTime
      effectivePeriodStart: [''], // Control for Effective Period Start
      effectivePeriodEnd: [''], // Control for Effective Period End


      //value[X]
      booleanValue: [''], // Default boolean value is 'true'
      stringValue: [''], // Default string value is empty
      numericValue: [''], // Default numeric value is empty
      unit: [''], // Default unit is empty
      system: [''], // Default system is empty
      code:[''],// Default code is empty
      codeableSystem:[''],
      codeableCode:[''],
      codeableDisplay:[''],


      //Interpretation
      interpretationCode: [''],
      interpretationDisplay: [''],
      interpretationSystem: [''],

      //Referance Range
      valueLow: [''],
      valueHigh: [''],
      referenceRangeSystem: [''],
      referenceRangeCode: [''],

      // ADDITIONS FOR EDIT

      observationID:[{ value: '', disabled: true }],


    });


  }

  // get default values for editing
  getObservationForDefault(id: string | undefined){
    if (id) {
      this.fhirService.getResource<fhir.Observation>('Observation', id).subscribe({
        next: obs => {
          this.obsEdit = obs;

          if(this.obsEdit.effectiveDateTime || this.obsEdit.effectivePeriod?.start || this.obsEdit.effectivePeriod?.end ){
            this.showEffectiveInputs = true;
          }

          let ymdDateTime = this.obsEdit.effectiveDateTime?.slice(0,16);

          let ymdPeriodS = this.obsEdit.effectivePeriod?.start?.slice(0,16);

          let ymdPeriodE = this.obsEdit.effectivePeriod?.end?.slice(0,16);



          this.showInputsCode = true; // REQUIRED

          let categoryCheck = false;
          if(this.obsEdit.category){
            categoryCheck = true
            this.showInputs = true;
          }

          let booleanValueCheck = false;
          if(this.obsEdit.valueBoolean){
            this.selectedValueType = "boolean";
            booleanValueCheck = true;
            this.showValueInputs= true;
          }

          let stringValueCheck = '';
          if(this.obsEdit.valueString){
            this.selectedValueType = "string";
            stringValueCheck = this.obsEdit.valueString;
            this.showValueInputs= true;
          }

          let numberic ;
          let unit;
          let system;
          let code;
          if(this.obsEdit.valueQuantity){
            this.selectedValueType = "quantity";
            numberic = this.obsEdit.valueQuantity.value
            unit = this.obsEdit.valueQuantity.unit
            system = this.obsEdit.valueQuantity.system
            code = this.obsEdit.valueQuantity.code
            this.showValueInputs= true;
          }

          let Cdisplay;
          let Csystem;
          let Ccode;
          if(this.obsEdit.valueCodeableConcept){
            this.selectedValueType = "CodeableConcept";
            Cdisplay = this.obsEdit.valueCodeableConcept?.coding?.at(0)?.display;
            Csystem= this.obsEdit.valueCodeableConcept?.coding?.at(0)?.system;
            Ccode = this.obsEdit.valueCodeableConcept?.coding?.at(0)?.code;
            this.showValueInputs= true;
          }



          let intCode;
          let intDisplay ;
          let intSystem ;
          if(this.obsEdit.interpretation){
            intCode =  this.obsEdit.interpretation?.at(0)?.coding?.at(0)?.code;
            intDisplay = this.obsEdit.interpretation?.at(0)?.coding?.at(0)?.display;
            intSystem = this.obsEdit.interpretation?.at(0)?.coding?.at(0)?.system;
            this.showInterpretationInputs = true;
          }


          let refLow;
          let refHigh;
          let refSystem ;
          let refCode ;
          if(this.obsEdit.referenceRange){
            refLow = this.obsEdit.referenceRange?.at(0)?.low?.value;
            refHigh = this.obsEdit.referenceRange?.at(0)?.high?.value;
            refSystem = this.obsEdit.referenceRange?.at(0)?.low?.system;
            refCode= this.obsEdit.referenceRange?.at(0)?.low?.code;
            this.showReferenceRangeInputs = true;
          }



          this.searchTerm = this.obsEdit.code.coding?.at(0)?.display || "";

          this.updateFilterList();

          this.obsForm = this.fb.group({

            subjectReference: "Patient/"+this.patientID,
            subjectDisplay: this.patientName,

            status: this.obsEdit.status,

            codeCode: this.obsEdit.code.coding?.at(0)?.code ,
            codeDisplay: this.obsEdit.code.coding?.at(0)?.display ,
            codeSystem: this.obsEdit.code.coding?.at(0)?.system ,

            categoryCode: this.obsEdit.category?.at(0)?.coding?.at(0)?.code || "",
            categoryDisplay: this.obsEdit.category?.at(0)?.coding?.at(0)?.display || "",
            categorySystem: this.obsEdit.category?.at(0)?.coding?.at(0)?.system || "",


            // Effective[x]

            effectiveDateTime: ymdDateTime || "",
            effectivePeriodStart: ymdPeriodS || "",
            effectivePeriodEnd: ymdPeriodE || "",


            //value[X]
            booleanValue: booleanValueCheck || "",
            stringValue: stringValueCheck || "",
            numericValue: numberic || "",
            unit: unit || "",
            system: system || "",
            code: code || "",
            codeableSystem: Csystem || "",
            codeableCode: Ccode || "",
            codeableDisplay: Cdisplay || "",


            //Interpretation
            interpretationCode: intCode || "",
            interpretationDisplay: intDisplay || "",
            interpretationSystem: intSystem || "",



            //Referance Range
            valueLow: refLow || "",
            valueHigh: refHigh || "",
            referenceRangeSystem: refSystem || "",
            referenceRangeCode: refCode || "",



          });
          this.updateFilterList();
        },
        error: console.error
      })
    }
  }


  toggleCategoryInputs() {
    this.showInputs = !this.showInputs; // Toggle the value of showInputs
    if (!this.showInputs) {
      // Reset the form controls when hiding the inputs
      this.obsForm.patchValue({
        categoryCode: '',
        categoryDisplay: '',
        categorySystem: '',
      });
    }
  }


  showEffective() {
    this.showEffectiveInputs = !this.showEffectiveInputs; // Toggle the value of showEffective
    if (!this.showEffectiveInputs) {
      // Reset the form controls when hiding the inputs
      this.obsForm.patchValue({
        effectiveDateTime: '',
        effectivePeriodStart: '',
        effectivePeriodEnd: '',
      });
    }
  }

  // Function to handle the selected effective type
  onEffectiveTypeChange(type: 'dateTime' | 'period') {
    this.effectiveType = type;

    if (type === 'dateTime') {
      this.obsForm.get('effectivePeriodStart')?.setValue('');
      this.obsForm.get('effectivePeriodEnd')?.setValue('');
    } else if (type === 'period') {
      this.obsForm.get('effectiveDateTime')?.setValue('');
    }
  }


  showValue() {
    this.showValueInputs = !this.showValueInputs; // Toggle the value of showValueInputs
    if (!this.showValueInputs) {
      // Reset the form controls when hiding the inputs
      this.obsForm.patchValue({
        stringValue: '',
        numericValue: '',
        unit: '',
        system: '',
        code: '',
        booleanValue: '',
        codeableSystem:'',
        codeableCode:'',
        codeableDisplay:'',
      });
    }
  }

  onValueTypeChange(type: 'boolean' | 'string' | 'quantity' | 'CodeableConcept') {
    // Set the selected value type
    this.selectedValueType = type;

    if(type === 'boolean' || type === 'string' || type === 'quantity' ){

      this.obsForm.get('codeableSystem')?.setValue('');
      this.obsForm.get('codeableCode')?.setValue('');
      this.obsForm.get('codeableDisplay')?.setValue('');

      if(type === 'boolean' || type === 'string' ){
        this.obsForm.get('numericValue')?.setValue('');
        this.obsForm.get('unit')?.setValue('');
        this.obsForm.get('system')?.setValue('');
        this.obsForm.get('code')?.setValue('');

        if(type === 'boolean'){
          this.obsForm.get('stringValue')?.setValue('');
        }
        else{
          this.obsForm.get('booleanValue')?.setValue('');
        }
      }
      else{
        this.obsForm.get('booleanValue')?.setValue('');
        this.obsForm.get('stringValue')?.setValue('');
      }
    }
    else{
      this.obsForm.get('stringValue')?.setValue('');
      this.obsForm.get('numericValue')?.setValue('');
      this.obsForm.get('unit')?.setValue('');
      this.obsForm.get('system')?.setValue('');
      this.obsForm.get('code')?.setValue('');
      this.obsForm.get('booleanValue')?.setValue('');
    }


  }


  toggleInterpretationInputs() {
    this.showInterpretationInputs = !this.showInterpretationInputs;
    // Clear the content of the interpretation input fields if they are hidden
    if (!this.showInterpretationInputs) {
      this.obsForm.get('interpretationSystem')?.setValue('');
      this.obsForm.get('interpretationDisplay')?.setValue('');
      this.obsForm.get('interpretationCode')?.setValue('');
    }
  }

  toggleReferenceRangeInputs() {
    this.showReferenceRangeInputs = !this.showReferenceRangeInputs;
    // Clear the content of the reference range input fields if they are hidden
    if (!this.showReferenceRangeInputs) {
      this.obsForm.get('valueLow')?.setValue('');
      this.obsForm.get('valueHigh')?.setValue('');
      this.obsForm.get('referenceRangeSystem')?.setValue('');
      this.obsForm.get('referenceRangeCode')?.setValue('');
    }
  }

  showCodeInputs() {
    this.showInputsCode = !this.showInputsCode; // Toggle the value of showInputs
    if (!this.showInputsCode) {
      // Reset the form controls when hiding the inputs
      this.obsForm.patchValue({
        codeCode: '',
        codeDisplay: '',
        codeSystem:'',
      });
    }
  }


  bundleFilterInit(): void {
    this.fhirService.getBundleForCodeFilter().subscribe({
      next: (data: any) => {
        this.allObservations = data['entry'][0].resource.entry;
        this.updateFilterList();
      }
    });
  }


  updateFilterList(): void {
    this.filterList = [];

    this.filterList = this.allObservations.filter((observation: any) =>
      observation.resource.compose.include.some((include: any) =>
        include.concept.some((concept: any) =>
          this.getConceptValue(concept).toLowerCase().includes(this.searchTerm.trim().toLowerCase())
        )
      )
    );
    this.savefilterList = this.filterList;
  }

  getConceptValue(concept: any): string {
    const conceptDesignations = concept.designation;
    const selectedLanguageValue = conceptDesignations.find((designation: any) => designation.language ===  this.translate.currentLang);

    return selectedLanguageValue ? selectedLanguageValue.value : concept.display;
  }


  onSearchBoxClick(): void {

    this.showResults = true;
    if(this.counter>0){
      this.onSearchChange();
    }
    this.counter++;
  }

  onSearchChange(): void {
    if (this.searchTerm.trim() === '') {
      this.obsForm.patchValue({
        codeCode: '' ,
        codeDisplay: '' ,
        codeSystem: ''  ,
      });
    }
    this.updateFilterList();
  }

  selectValue(value: string, value2: string, value3: string, event: Event): void {
    const clickedElement = event.target as HTMLElement;
    const dropdownList = document.querySelector('.dropdown-list');

    if (dropdownList && dropdownList.contains(clickedElement)) {

      this.searchTerm = value;
      this.searchCode = value2;
      this.searchSystem = value3;

      this.obsForm.patchValue({
        codeCode: this.searchCode  ,
        codeDisplay: this.searchTerm ,
        codeSystem: this.searchSystem ,
      });

      this.showResults = false;
    }
  }

  clearSearch(): void {
    this.showResults = false;
  }

  // Getter to access form control
  get myForm() {
    return this.obsForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (!this.obsForm.valid) {
      alert(this.translate.instant('invalidForm'));
    } else {
      let observation: fhir.Observation={

        resourceType: "Observation",

        status: this.obsForm.get("status")?.value, // STATUS

        code: {   /// CODE
          coding: [
              {
                  system: this.obsForm.get("codeSystem")?.value,
                  code: this.obsForm.get("codeCode")?.value,
                  display: this.obsForm.get("codeDisplay")?.value
              }
          ]
        },

        subject: {  /// SUBJECT
          type: "Patient",
          reference:  this.obsForm.get("subjectReference")?.value,
          display: this.obsForm.get("subjectDisplay")?.value
        },

      }

      if(this.isEdit){
        observation.id =  this.id;
      }

      const categoryCode = this.obsForm.get("categoryCode")?.value


      console.log("categoryCode",categoryCode);

      if(categoryCode != '' ){   // CATEGORY (OPTIONAL)

        const categorySystem =  this.obsForm.get("categorySystem")?.value;

        const categoryDisplay =  this.obsForm.get("categoryDisplay")?.value;

        if(categoryDisplay != ''){
          observation.category=[  // CATEGORY WITH DISPLAY
            {
              coding: [
                {
                  system: categorySystem,
                  code: categoryCode,
                  display: categoryDisplay,
                }
              ]
            }
          ]
        }
        else{
          observation.category=[  // CATEGORY WITHOUT DISPLAY
            {
              coding: [
                {
                  system: categorySystem,
                  code: categoryCode
                }
              ]
            }
          ]
        }
      }

      const effectiveDateTime = this.obsForm.get("effectiveDateTime")?.value;
      if(effectiveDateTime != ''  ){                           // EFFECTIVE DATETIME
        observation.effectiveDateTime = effectiveDateTime + ":00+00:00";
      }

      const effectivePeriodStart =  this.obsForm.get("effectivePeriodStart")?.value;
      const effectivePeriodEnd =  this.obsForm.get("effectivePeriodEnd")?.value;
      if(effectivePeriodStart != '' && effectivePeriodEnd != '' && effectivePeriodStart != null  && effectivePeriodEnd != null ){ // EFFECTIVE PERIOD
        observation.effectivePeriod= {
          start: effectivePeriodStart +":00+00:00",
          end: effectivePeriodEnd+":00+00:00"
        }
      }

      const stringValue= this.obsForm.get("stringValue")?.value;
      if(stringValue != '' ) {                             // VALUE STRING
        observation.valueString = stringValue;
      }
      const booleanValue =  this.obsForm.get("booleanValue")?.value;
      if(booleanValue!= '' ){
        let bool = JSON.parse(booleanValue);
        observation.valueBoolean = bool;          // VALUE BOOLEAN
      }

      const numericValue =  this.obsForm.get("numericValue")?.value;
      const unit =  this.obsForm.get("unit")?.value;
      const system = this.obsForm.get("system")?.value;
      const code =  this.obsForm.get("code")?.value;
      if (numericValue!= '' && unit != '' && system != '' && code != '')  {  // VALUE QUANTITY
        observation.valueQuantity = {
          value: numericValue,
          unit: unit,
          system: system,
          code: code
        }
      }

      const codeableSystem =  this.obsForm.get("codeableSystem")?.value;
      const codeableCode=  this.obsForm.get("codeableCode")?.value;
      const codeableDisplay=  this.obsForm.get("codeableDisplay")?.value;
      if (codeableCode!= '' && codeableDisplay != '' && codeableSystem!= '' ) {  // VALUE CODEABLE
        observation.valueCodeableConcept= {
          coding: [
              {
                  system: codeableSystem,
                  code: codeableCode,
                  display: codeableDisplay
              }
          ]
        }
      }


      const interpretationCode = this.obsForm.get("interpretationCode")?.value;
      const interpretationSystem = this.obsForm.get("interpretationSystem")?.value;
      const interpretationDisplay = this.obsForm.get("interpretationDisplay")?.value;
      if (interpretationCode!= '' && interpretationDisplay != '' && interpretationSystem!= '' ) {  // VALUE CODEABLE
        observation.interpretation= [
          {
              coding: [
                  {
                      system: interpretationSystem,
                      code: interpretationCode,
                      display: interpretationDisplay
                  }
              ]
          }
        ]
      }


      const valueLow = this.obsForm.get("valueLow")?.value;
      const valueHigh = this.obsForm.get("valueHigh")?.value;
      const referenceRangeSystem = this.obsForm.get("referenceRangeSystem")?.value;
      const referenceRangeCode = this.obsForm.get("referenceRangeCode")?.value;
      if(valueLow != '' && valueHigh != '' && referenceRangeSystem != '' && referenceRangeCode != '' ){
        observation.referenceRange = [
          {
              low: {
                  value: valueLow,
                  unit: referenceRangeCode,
                  system: referenceRangeSystem,
                  code: referenceRangeCode
              },
              high: {
                  value: valueHigh,
                  unit: referenceRangeCode,
                  system: referenceRangeSystem,
                  code: referenceRangeCode
              }
          }
      ]
      }

      if(!this.isEdit){       // CREATE OBSERVATION
        this.fhirService.addObservation(observation)
        .subscribe({
          complete: () =>{
            alert(this.translate.instant('addConfirm'));
            this.r.navigateByUrl('/patient/' + this.patientID + '/observation');
          },
          error: (e) =>{
            alert(this.translate.instant('addError'));

          }
        });
      }

      if(this.isEdit){        // EDIT OBSERVATION
        this.fhirService.updateObservation(this.id, observation)
        .subscribe({
          complete: () =>{
            alert(this.translate.instant('editConfirm'));
            this.r.navigateByUrl('/patient/' + this.patientID + '/observation');
          },
          error: (e) =>{
            alert(this.translate.instant('editError'));
          }
        });

      }

    }
  }
}
