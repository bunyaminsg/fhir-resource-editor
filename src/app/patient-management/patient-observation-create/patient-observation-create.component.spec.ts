import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientObservationCreateComponent } from './patient-observation-create.component';

describe('PatientObservationCreateComponent', () => {
  let component: PatientObservationCreateComponent;
  let fixture: ComponentFixture<PatientObservationCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientObservationCreateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PatientObservationCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
