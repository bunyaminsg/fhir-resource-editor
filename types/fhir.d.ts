declare namespace fhir {
  type id = string;
  type uri = string;
  type url = string;
  type uuid = string;
  type canonical = string;
  type integer = number;
  type decimal = number;
  type base64Binary = string;
  type instant = string;
  type date = string;
  type dateTime = string;
  type xhtml = string;
  type time = string;
  type code = string;
  type oid = string;
  type unsignedInt = number;
  type positiveInt = number;
  type markdown = string;
  interface ResourceBase {
    id?: id;
    meta?: Meta;
    isRefOnly?: boolean;
    refDisplay?: string;
    implicitRules?: uri;
    language?: code;
    resourceType?: code;
  }
  interface Element {
    id?: id;
    extension?: Extension[];
  }
  interface Identifier extends Element {
    use?: code;
    type?: CodeableConcept;
    system?: uri;
    value?: string;
    period?: Period;
    assigner?: Reference;
  }
  interface Narrative extends Element {
    status: code;
    div: xhtml;
  }
  interface CodeableConcept extends Element {
    coding?: Coding[];
    text?: string;
  }
  interface Period extends Element {
    start?: dateTime;
    end?: dateTime;
  }
  interface UsageContext extends Element {
    code: Coding;
    valueCodeableConcept?: CodeableConcept;
    valueQuantity?: Quantity;
    valueRange?: Range;
    valueReference?: Reference; // R4Added
  }
  interface Reference extends Element {
    reference?: string;
    type?: uri; // R4Added
    identifier?: Identifier;
    display?: string;
  }
  interface Coding extends Element {
    system?: uri;
    version?: string;
    code?: code;
    display?: string;
    userSelected?: boolean;
  }
  interface Quantity extends Element {
    value?: decimal;
    comparator?: code;
    unit?: string;
    system?: uri;
    code?: code;
  }
  interface Meta extends Element {
    versionId?: id;
    lastUpdated?: instant;
    source?: uri; // R4Added
    // R4Deleted profile?: uri[];
    profile?: canonical[]; // R4Added
    security?: Coding[];
    tag?: Coding[];
  }
  interface Extension extends Element {
    url: uri;
    valueBase64Binary?: base64Binary;
    valueBoolean?: boolean;
    valueCanonical?: canonical; // R4Added
    valueCode?: code;
    valueDate?: date;
    valueDateTime?: dateTime;
    valueDecimal?: decimal;
    valueId?: id;
    valueInstant?: instant;
    valueInteger?: integer;
    valueMarkdown?: markdown;
    valueOid?: oid;
    valuePositiveInt?: positiveInt;
    valueString?: string;
    valueTime?: time;
    valueUnsignedInt?: unsignedInt;
    valueUri?: uri;
    valueUrl?: url; // R4Added
    valueUuid?: uuid; // R4Added
    valueAddress?: Address;
    valueAge?: Age;
    valueAnnotation?: Annotation;
    valueAttachment?: Attachment;
    valueCodeableConcept?: CodeableConcept;
    valueCoding?: Coding;
    valueContactPoint?: ContactPoint;
    valueCount?: Count;
    valueDistance?: Distance;
    valueDuration?: Duration;
    valueHumanName?: HumanName;
    valueIdentifier?: Identifier;
    valueMoney?: Money;
    valuePeriod?: Period;
    valueQuantity?: Quantity;
    valueRange?: Range;
    valueRatio?: Ratio;
    valueReference?: Reference;
    valueSampledData?: SampledData;
    valueSignature?: Signature;
    valueTiming?: Timing;
    valueContactDetail?: ContactDetail; // R4Added
    valueContributor?: Contributor; // R4Added
    valueDataRequirement?: DataRequirement; // R4Added
    valueExpression?: Expression; // R4Added
    valueParameterDefinition?: ParameterDefinition; // R4Added
    valueRelatedArtifact?: RelatedArtifact; // R4Added
    valueTriggerDefinition?: TriggerDefinition; // R4Added
    valueUsageContext?: UsageContext; // R4Added
    valueDosage?: Dosage; // R4Added
    valueMeta?: Meta;
  }
  interface DomainResource extends ResourceBase {
    text?: Narrative;
    contained?: Resource[];
    extension?: Extension[];
    modifierExtension?: Extension[];
  }
  interface Account extends DomainResource {
    identifier?: Identifier[];
    status?: code;
    type?: CodeableConcept;
    name?: string;
    subject?: Reference;
    servicePeriod?: Period; // R4Added

    // R4Deleted active?: Period;
    // R4Deleted currency?: Coding;
    // R4Deleted balance?: Money;
    // R4Deleted coverage?: Reference[];
    // R4Deleted coveragePeriod?: Period;
    coverage?: AccountCoverage[]; // R4Added
    owner?: Reference;
    description?: string;
    guarantor?: AccountGuarantor[];
    partOf?: Reference; // R4Added
  }
  interface AccountCoverage extends Element { // R4Added
    coverage: Reference;
    priority?: positiveInt;
  }
  interface AccountGuarantor extends Element {
    party: Reference;
    onHold?: boolean;
    period?: Period;
  }
  interface ActivityDefinition extends DomainResource {
    url?: uri;
    identifier?: Identifier[];
    version?: string;
    name?: string;
    title?: string;
    subtitle?: string; // R4Added
    status: code;
    experimental?: boolean;
    subjectCodeableConcept?: CodeableConcept; // R4Added
    subjectReference?: Reference; // R4Added
    date?: dateTime;
    publisher?: string;
    contact?: ContactDetail[];
    description?: markdown;
    useContext?: UsageContext[];
    jurisdiction?: CodeableConcept[];
    purpose?: markdown;
    usage?: string;
    copyright?: markdown;
    approvalDate?: date;
    lastReviewDate?: date;
    effectivePeriod?: Period;
    topic?: CodeableConcept[];
    author?: ContactDetail[]; // R4Added
    editor?: ContactDetail[]; // R4Added
    reviewer?: ContactDetail[]; // R4Added
    endorser?: ContactDetail[]; // R4Added
    // R4Deleted contributor?: Contributor[];
    relatedArtifact?: RelatedArtifact[];
    library?: canonical[]; // R4Added
    // R4Deleted category?: code;
    kind?: code; // R4Added
    profile?: canonical; // R4Added
    code?: CodeableConcept;
    intent?: code; // R4Added
    priority?: code; // R4Added
    doNotPerform?: boolean; // R4Added
    timingTiming?: Timing;
    timingDateTime?: dateTime // R4Added
    timingAge?: Age; // R4Added
    timingPeriod?: Period; // R4Added
    timingRange?: Range; // R4Added
    timingDuration?: Duration; // R4Added
    // R4Deleted timingCodeableConcept?: CodeableConcept;
    location?: Reference;
    // R4Deleted participantType?: code[];
    participant?: ActivityDefinitionParticipant[]; // R4Added
    productReference?: Reference;
    productCodeableConcept?: CodeableConcept;
    quantity?: SimpleQuantity;
    // R4Deleted dosageInstruction?: DosageInstruction[];
    dosage?: Dosage[];
    bodySite?: CodeableConcept[];
    specimenRequirement?: Reference[]; // R4Added
    observationRequirement?: Reference[]; // R4Added
    observationResultRequirement?: Reference[]; // R4Added
    transform?: Reference; // R4 Added
    dynamicValue?: ActivityDefinitionDynamicValue[];
  }
  interface ActivityDefinitionParticipant extends Element { // R4Added
    type: code;
    role?: CodeableConcept;
  }
  interface ActivityDefinitionDynamicValue extends Element {
    // R4Deleted description?: string;
    path?: string;
    // R4Deleted language?: string;
    // R4Deleted expression?: string;
    // expression?: Expression; // R4Added TODO Expression
  }
  interface AllergyIntolerance extends DomainResource {
    identifier?: Identifier[];
    // R4Deleted clinicalStatus?: code;
    clinicalStatus?: CodeableConcept; // R4Added
    // R4Deleted verificationStatus: code;
    verificationStatus: CodeableConcept; // R4Added
    type?: code;
    category?: code[];
    criticality?: code;
    code?: CodeableConcept;
    patient: Reference;
    encounter?: Reference; // R4Added
    onsetDateTime?: dateTime;
    onsetAge?: Age;
    onsetPeriod?: Period;
    onsetRange?: Range;
    onsetString?: string;
    // R4Deleted assertedDate?: dateTime;
    recordedDate?: dateTime; // R4Added
    recorder?: Reference;
    asserter?: Reference;
    lastOccurrence?: dateTime;
    note?: Annotation[];
    reaction?: AllergyIntoleranceReaction[];
  }
  interface AllergyIntoleranceReaction extends Element {
    substance?: CodeableConcept;
    // R4Deleted certainty?: code;
    manifestation: CodeableConcept[];
    description?: string;
    onset?: dateTime;
    severity?: code;
    exposureRoute?: CodeableConcept;
    note?: Annotation[];
  }
  interface Appointment extends DomainResource {
    identifier?: Identifier[];
    status: code;
    cancelationReason?: CodeableConcept; // R4Added
    serviceCategory?: CodeableConcept;
    serviceType?: CodeableConcept[];
    specialty?: CodeableConcept[];
    appointmentType?: CodeableConcept;
    // R4Deleted reason?: CodeableConcept[];
    reasonCode?: CodeableConcept[]; // R4Added
    reasonReference?: Reference[]; // R4Added
    priority?: unsignedInt;
    description?: string;
    supportingInformation?: Reference[]; // R4Added
    start?: instant;
    end?: instant;
    minutesDuration?: positiveInt;
    slot?: Reference[];
    created?: dateTime;
    comment?: string;
    patientInstruction?: string; // R4Added
    // R4Deleted incomingReferral?: Reference[];
    basedOn?: Reference[]; // R4Added
    participant: AppointmentParticipant[];
    requestedPeriod?: Period[];
  }
  interface AppointmentParticipant extends Element {
    type?: CodeableConcept[];
    actor?: Reference;
    required?: code;
    status: code;
  }
  interface AppointmentResponse extends DomainResource {
    identifier?: Identifier[];
    appointment: Reference;
    start?: instant;
    end?: instant;
    participantType?: CodeableConcept[];
    actor?: Reference;
    participantStatus: code;
    comment?: string;
  }
  interface AuditEvent extends DomainResource {
    type: Coding;
    subtype?: Coding[];
    action?: code;
    period?: Period; // R4Added
    recorded: instant;
    outcome?: code;
    outcomeDesc?: string;
    purposeOfEvent?: CodeableConcept[];
    agent: AuditEventAgent[];
    source: AuditEventSource;
    entity?: AuditEventEntity[];
  }
  interface AuditEventAgent extends Element {
    type?: CodeableConcept; // R4Added
    role?: CodeableConcept[];
    // R4Deleted reference?: Reference;
    who?: Reference; // R4Added
    // R4Deleted userId?: Identifier;
    altId?: string;
    name?: string;
    requestor: boolean;
    location?: Reference;
    policy?: uri[];
    media?: Coding;
    network?: AuditEventAgentNetwork;
    purposeOfUse?: CodeableConcept[];
  }
  interface AuditEventAgentNetwork extends Element {
    address?: string;
    type?: code;
  }
  interface AuditEventSource extends Element {
    observer: Reference; // R4Added
    site?: string;
    // R4Deleted identifier: Identifier;
    type?: Coding[];
  }
  interface AuditEventEntity extends Element {
    // R4Deleted identifier?: Identifier;
    // R4Deleted reference?: Reference;
    what?: Reference; // R4Added
    type?: Coding;
    role?: Coding;
    lifecycle?: Coding;
    securityLabel?: Coding[];
    name?: string;
    description?: string;
    query?: base64Binary;
    detail?: AuditEventEntityDetail[];
  }
  interface AuditEventEntityDetail extends Element {
    type: string;
    // R4Deleted value: base64Binary;
    valueString?: string; // R4Added
    valueBase64Binary?: base64Binary; // R4Added
  }
  interface Basic extends DomainResource {
    identifier?: Identifier[];
    code: CodeableConcept;
    subject?: Reference;
    created?: date;
    author?: Reference;
  }
  interface Binary extends ResourceBase {
    contentType: code;
    securityContext?: Reference;
    // R4Deleted content: base64Binary;
    data?: base64Binary; // R4Added
  }
  /* R4Deleted interface BodySite extends DomainResource {
      patient: Reference;
      identifier?: Identifier[];
      code?: CodeableConcept;
      modifier?: CodeableConcept[];
      description?: string;
      image?: Attachment[];
    }*/
  interface Bundle extends ResourceBase {
    identifier?: Identifier;
    type: code;
    timestamp?: instant; // R4Added
    total?: unsignedInt;
    link?: BundleLink[];
    entry?: BundleEntry[];
    signature?: Signature;
  }
  interface BundleLink extends Element {
    relation: string;
    url: uri;
  }
  interface BundleEntry extends Element {
    link?: BundleLink[];
    fullUrl?: uri;
    resource?: Resource;
    search?: BundleEntrySearch;
    request?: BundleEntryRequest;
    response?: BundleEntryResponse;
  }
  interface BundleEntrySearch extends Element {
    mode?: code;
    score?: decimal;
  }
  interface BundleEntryRequest extends Element {
    method: code;
    url: uri;
    ifNoneMatch?: string;
    ifModifiedSince?: instant;
    ifMatch?: string;
    ifNoneExist?: string;
  }
  interface BundleEntryResponse extends Element {
    status: string;
    location?: uri;
    etag?: string;
    lastModified?: instant;
    outcome?: Resource;
  }
  interface CarePlan extends DomainResource {
    identifier?: Identifier[];
    instantiatesCanonical?: canonical[]; // R4Added
    instantiatesUri?: uri[]; // R4Added
    basedOn?: Reference[]; // R4Added
    replaces?: Reference[];
    partOf?: Reference[]; // R4Added
    status: code;
    intent: code;
    category?: CodeableConcept[];
    title?: string;
    description?: string;
    subject: Reference;
    // R4Deleted context?: Reference;
    encounter?: Reference; // R4Added
    period?: Period;
    created?: dateTime; // R4Added
    // R4Deleted modified?: dateTime;
    // R4Deleted author?: Reference[];
    author?: Reference; // R4Added
    contributor?: Reference[]; // R4Added
    careTeam?: Reference[];
    addresses?: Reference[];
    supportingInfo?: Reference[];
    // R4Deleted definition?: Reference;
    // R4Deleted relatedPlan?: CarePlanRelatedPlan[];
    goal?: Reference[];
    activity?: CarePlanActivity[];
    note?: Annotation[];
  }
  /* R4Deleted interface CarePlanRelatedPlan extends Element {
      code?: code;
      plan: Reference;
    }*/
  interface CarePlanActivity extends Element {
    outcomeCodeableConcept?: CodeableConcept[];
    outcomeReference?: Reference[];
    // R4Deleted actionResulting?: Reference[];
    progress?: Annotation[];
    reference?: Reference;
    detail?: CarePlanActivityDetail;
  }
  interface CarePlanActivityDetail extends Element {
    // R4Deleted category?: CodeableConcept;
    kind?: CodeableConcept; // R4Added
    instantiatesCanonical?: canonical[]; // R4Added
    instantiatesUri?: uri[]; // R4Added
    definition?: Reference;
    code?: CodeableConcept;
    reasonCode?: CodeableConcept[];
    reasonReference?: Reference[];
    goal?: Reference[];
    status: code;
    // R4Deleted statusReason?: string;
    statusReason?: CodeableConcept; // R4Added
    // R4Deleted prohibited?: boolean;
    doNotPerform?: boolean; // R4Added
    scheduledTiming?: Timing;
    scheduledPeriod?: Period;
    scheduledString?: string;
    location?: Reference;
    performer?: Reference[];
    productCodeableConcept?: CodeableConcept;
    productReference?: Reference;
    dailyAmount?: SimpleQuantity;
    quantity?: SimpleQuantity;
    description?: string;
  }
  interface CareTeam extends DomainResource {
    identifier?: Identifier[];
    status?: code;
    category?: CodeableConcept[];
    name?: string;
    subject?: Reference;
    encounter?: Reference; // R4Added
    period?: Period;
    participant?: CareTeamParticipant[];
    reasonCode?: CodeableConcept[]; // R4Added
    reasonReference?: Reference[]; // R4Added
    telecom?: ContactPoint[]; // R4Added
    managingOrganization?: Reference[];
    note?: Annotation[]; // R4Added
  }
  interface CareTeamParticipant extends Element {
    role?: CodeableConcept[];
    member?: Reference;
    onBehalfOf?: Reference;
    period?: Period;
  }
  interface Claim extends DomainResource { // TODO R4
    identifier?: Identifier[];
    status?: code;
    type?: CodeableConcept;
    subType?: CodeableConcept[];
    use?: code;
    patient?: Reference;
    billablePeriod?: Period;
    created?: dateTime;
    enterer?: Reference;
    insurer?: Reference;
    provider?: Reference;
    organization?: Reference;
    priority?: CodeableConcept;
    fundsReserve?: CodeableConcept;
    related?: ClaimRelated[];
    prescription?: Reference;
    originalPrescription?: Reference;
    payee?: ClaimPayee;
    referral?: Reference;
    facility?: Reference;
    careTeam?: ClaimCareTeam[];
    information?: ClaimInformation[];
    diagnosis?: ClaimDiagnosis[];
    procedure?: ClaimProcedure[];
    insurance?: ClaimInsurance[];
    accident?: ClaimAccident;
    employmentImpacted?: Period;
    hospitalization?: Period;
    item?: ClaimItem[];
    total?: Money;
  }
  interface ClaimRelated extends Element {
    claim?: Reference;
    relationship?: CodeableConcept;
    reference?: Identifier;
  }
  interface ClaimPayee extends Element {
    type: CodeableConcept;
    resourceType?: Coding;
    party?: Reference;
  }
  interface ClaimCareTeam extends Element {
    sequence: positiveInt;
    provider: Reference;
    responsible?: boolean;
    role?: CodeableConcept;
    qualification?: CodeableConcept;
  }
  interface ClaimInformation extends Element {
    category: CodeableConcept;
    code?: CodeableConcept;
    timingDate?: date;
    timingPeriod?: Period;
    valueString?: string;
    valueQuantity?: Quantity;
    valueAttachment?: Attachment;
    valueReference?: Reference;
    reason?: CodeableConcept;
  }
  interface ClaimDiagnosis extends Element {
    sequence: positiveInt;
    diagnosisCodeableConcept?: CodeableConcept;
    diagnosisReference?: Reference;
    type?: CodeableConcept[];
    packageCode?: CodeableConcept;
  }
  interface ClaimProcedure extends Element {
    sequence: positiveInt;
    date?: dateTime;
    procedureCodeableConcept?: CodeableConcept;
    procedureReference?: Reference;
  }
  interface ClaimInsurance extends Element {
    sequence: positiveInt;
    focal: boolean;
    coverage: Reference;
    businessArrangement?: string;
    preAuthRef?: string[];
    claimResponse?: Reference;
  }
  interface ClaimAccident extends Element {
    date: date;
    type?: CodeableConcept;
    locationAddress?: Address;
    locationReference?: Reference;
  }
  interface ClaimItem extends Element {
    sequence: positiveInt;
    careTeamLinkId?: positiveInt[];
    diagnosisLinkId?: positiveInt[];
    procedureLinkId?: positiveInt[];
    informationLinkId?: positiveInt[];
    revenue?: CodeableConcept;
    category?: CodeableConcept;
    service?: CodeableConcept;
    modifier?: CodeableConcept[];
    programCode?: CodeableConcept[];
    servicedDate?: date;
    servicedPeriod?: Period;
    locationCodeableConcept?: CodeableConcept;
    locationAddress?: Address;
    locationReference?: Reference;
    quantity?: Quantity;
    unitPrice?: Money;
    factor?: decimal;
    net?: Money;
    udi?: Reference[];
    bodySite?: CodeableConcept;
    subSite?: CodeableConcept[];
    detail?: ClaimItemDetail[];
    prosthesis?: ClaimItemProsthesis;
  }
  interface ClaimItemDetail extends Element {
    sequence: positiveInt;
    revenue?: CodeableConcept;
    category?: CodeableConcept;
    service?: CodeableConcept;
    modifier?: CodeableConcept[];
    programCode?: CodeableConcept[];
    quantity?: Quantity;
    unitPrice?: Money;
    factor?: decimal;
    net?: Money;
    udi?: Reference[];
    subDetail?: ClaimItemDetailSubDetail[];
  }
  interface ClaimItemDetailSubDetail extends Element {
    sequence: positiveInt;
    revenue?: CodeableConcept;
    category?: CodeableConcept;
    service?: CodeableConcept;
    modifier?: CodeableConcept[];
    programCode?: CodeableConcept[];
    quantity?: Quantity;
    unitPrice?: Money;
    factor?: decimal;
    net?: Money;
    udi?: Reference[];
  }
  interface ClaimItemProsthesis extends Element {
    initial?: boolean;
    priorDate?: date;
    priorMaterial?: CodeableConcept;
  }
  interface ClaimResponse extends DomainResource { // TODO R4
    identifier?: Identifier[];
    status?: code;
    created?: dateTime;
    insurer?: Reference;
    requestProvider?: Reference;
    requestOrganization?: Reference;
    request?: Reference;
    outcome?: CodeableConcept;
    disposition?: string;
    payeeType?: CodeableConcept;
    item?: ClaimResponseItem[];
    addItem?: ClaimResponseAddItem[];
    error?: ClaimResponseError[];
    totalCost?: Money;
    unallocDeductable?: Money;
    totalBenefit?: Money;
    payment?: ClaimResponsePayment;
    reserved?: Coding;
    form?: CodeableConcept;
    note?: ClaimResponseNote[];
    communicationRequest?: Reference[];
    insurance?: ClaimResponseInsurance[];
  }
  interface ClaimResponseItem extends Element {
    sequenceLinkId: positiveInt;
    noteNumber?: positiveInt[];
    adjudication?: ClaimResponseItemAdjudication[];
    detail?: ClaimResponseItemDetail[];
  }
  interface ClaimResponseItemAdjudication extends Element {
    category: CodeableConcept;
    reason?: CodeableConcept;
    amount?: Money;
    value?: decimal;
  }
  interface ClaimResponseItemDetail extends Element {
    sequenceLinkId: positiveInt;
    noteNumber?: positiveInt[];
    adjudication?: ClaimResponseItemAdjudication[];
    subDetail?: ClaimResponseItemDetailSubDetail[];
  }
  interface ClaimResponseItemDetailSubDetail extends Element {
    sequenceLinkId: positiveInt;
    noteNumber?: positiveInt[];
    adjudication?: ClaimResponseItemAdjudication[];
  }
  interface ClaimResponseAddItem extends Element {
    sequenceLinkId?: positiveInt[];
    revenue?: CodeableConcept;
    category?: CodeableConcept;
    service?: CodeableConcept;
    modifier?: CodeableConcept[];
    fee?: Money;
    noteNumber?: positiveInt[];
    adjudication?: ClaimResponseItemAdjudication[];
    detail?: ClaimResponseAddItemDetail[];
  }
  interface ClaimResponseAddItemDetail extends Element {
    revenue?: CodeableConcept;
    category?: CodeableConcept;
    service?: CodeableConcept;
    modifier?: CodeableConcept[];
    fee?: Money;
    noteNumber?: positiveInt[];
    adjudication?: ClaimResponseItemAdjudication[];
  }
  interface ClaimResponseError extends Element {
    sequenceLinkId?: positiveInt;
    detailSequenceLinkId?: positiveInt;
    subdetailSequenceLinkId?: positiveInt;
    code: CodeableConcept;
  }
  interface ClaimResponsePayment extends Element {
    type?: CodeableConcept;
    adjustment?: Money;
    adjustmentReason?: CodeableConcept;
    date?: date;
    amount?: Money;
    identifier?: Identifier;
  }
  interface ClaimResponseNote extends Element {
    number?: positiveInt;
    type?: CodeableConcept;
    text?: string;
    language?: CodeableConcept;
  }
  interface ClaimResponseInsurance extends Element {
    sequence: positiveInt;
    focal: boolean;
    coverage: Reference;
    businessArrangement?: string;
    preAuthRef?: string[];
    claimResponse?: Reference;
  }
  interface ClinicalImpression extends DomainResource { // TODO R4
    identifier?: Identifier[];
    status: code;
    code?: CodeableConcept;
    description?: string;
    subject: Reference;
    assessor?: Reference;
    date?: dateTime;
    effectiveDateTime?: dateTime;
    effectivePeriod?: Period;
    context?: Reference;
    previous?: Reference;
    problem?: Reference[];
    investigation?: ClinicalImpressionInvestigation[];
    protocol?: uri[];
    summary?: string;
    finding?: ClinicalImpressionFinding[];
    prognosisCodeableConcept?: CodeableConcept[];
    prognosisReference?: Reference[];
    action?: Reference[];
    note?: Annotation[];
  }
  interface ClinicalImpressionInvestigation extends Element {
    code: CodeableConcept;
    item?: Reference[];
  }
  interface ClinicalImpressionFinding extends Element {
    itemCodeableConcept?: CodeableConcept;
    itemReference?: Reference;
    basis?: string;
  }
  interface CodeSystem extends DomainResource { // TODO R4
    url?: uri;
    identifier?: Identifier;
    version?: string;
    name?: string;
    title?: string;
    status: code;
    experimental?: boolean;
    publisher?: string;
    contact?: ContactDetail[];
    date?: dateTime;
    description?: markdown;
    useContext?: UsageContext[];
    jurisdiction?: CodeableConcept[];
    purpose?: markdown;
    copyright?: markdown;
    caseSensitive?: boolean;
    valueSet?: uri;
    hierarchyMeaning?: code;
    compositional?: boolean;
    versionNeeded?: boolean;
    content: code;
    count?: unsignedInt;
    filter?: CodeSystemFilter[];
    property?: CodeSystemProperty[];
    concept?: CodeSystemConcept[];
  }
  interface CodeSystemFilter extends Element {
    code: code;
    description?: string;
    operator: code[];
    value: string;
  }
  interface CodeSystemProperty extends Element {
    code: code;
    uri?: uri;
    description?: string;
    type: code;
  }
  interface CodeSystemConcept extends Element {
    code: code;
    display?: string;
    definition?: string;
    designation?: CodeSystemConceptDesignation[];
    property?: CodeSystemConceptProperty[];
    concept?: CodeSystemConcept[];
  }
  interface CodeSystemConceptDesignation extends Element {
    language?: code;
    use?: Coding;
    value: string;
  }
  interface CodeSystemConceptProperty extends Element {
    code: code;
    valueCode?: code;
    valueCoding?: Coding;
    valueString?: string;
    valueInteger?: integer;
    valueBoolean?: boolean;
    valueDateTime?: dateTime;
  }
  interface Communication extends DomainResource {
    identifier?: Identifier[];
    instantiatesCanonical?: canonical[]; // R4Added
    instantiatesUri?: uri[]; // R4Added
    basedOn?: Reference[];
    partOf?: Reference[]; // R4Added
    inResponseTo?: Reference[]; // R4Added
    // R4Deleted parent?: Reference[];
    status?: code;
    category?: CodeableConcept[];
    priority?: code; // R4Added
    medium?: CodeableConcept[];
    subject?: Reference;
    // R4Deleted topic?: Reference[];
    topic?: CodeableConcept; // R4Added
    about?: Reference[]; // R4Added
    // R4Deleted context?: Reference;
    encounter?: Reference; // R4Added
    sent?: dateTime;
    received?: dateTime;
    recipient?: Reference[];
    sender?: Reference;
    reasonCode?: CodeableConcept[];
    payload?: CommunicationPayload[];
    note?: Annotation[];
  }
  interface CommunicationPayload extends Element {
    contentString?: string;
    contentAttachment?: Attachment;
    contentReference?: Reference;
  }
  interface CommunicationRequest extends DomainResource {
    identifier?: Identifier[];
    basedOn?: Reference[]; // R4Added
    replaces?: Reference[]; // R4Added
    groupIdentifier?: Identifier; // R4Added
    status?: code;
    statusReason?: CodeableConcept; // R4Added
    category?: CodeableConcept[];
    // R4Deleted priority?: CodeableConcept;
    priority?: code; // R4Added
    doNotPerform?: boolean; // R4Added
    medium?: CodeableConcept[];
    subject?: Reference;
    about?: Reference[]; // R4Added
    // R4Deleted context?: Reference;
    encounter?: Reference; // R4Added
    payload?: CommunicationRequestPayload[];
    occurrenceDateTime?: dateTime;
    occurrencePeriod?: Period;
    authoredOn?: dateTime;
    requester?: Reference;
    recipient?: Reference[];
    sender?: Reference;
    // R4 Deleted topic?: Reference[];

    reasonCode?: CodeableConcept[];
    reasonReference?: Reference[];
    // R4Deleted requestedOn?: dateTime;
    note?: Annotation[];
  }
  interface CommunicationRequestPayload extends Element {
    contentString?: string;
    contentAttachment?: Attachment;
    contentReference?: Reference;
  }
  interface CompartmentDefinition extends DomainResource { // TODO R4
    url: uri;
    name: string;
    title?: string;
    status: code;
    experimental?: boolean;
    publisher?: string;
    contact?: ContactDetail[];
    date?: dateTime;
    description?: markdown;
    purpose?: markdown;
    useContext?: UsageContext[];
    jurisdiction?: CodeableConcept[];
    code: code;
    search: boolean;
    resource?: CompartmentDefinitionResource[];
  }
  interface CompartmentDefinitionResource extends Element {
    code: code;
    param?: string[];
    documentation?: string;
  }
  interface Composition extends DomainResource {
    identifier?: Identifier;
    status: code;
    type: CodeableConcept;
    // R4Deleted class?: CodeableConcept;
    category?: CodeableConcept; // R4Added
    subject: Reference;
    encounter?: Reference;
    date: dateTime;
    author: Reference[];
    title: string;
    confidentiality?: code;
    attester?: CompositionAttester[];
    custodian?: Reference;
    relatesTo?: CompositionRelatesTo[]; // R4Added
    event?: CompositionEvent[];
    section?: CompositionSection[];
  }
  interface CompositionAttester extends Element {
    // R4Deleted mode: code[];
    mode: code; // R4Added
    time?: dateTime;
    party?: Reference;
  }
  interface CompositionRelatesTo extends Element { // R4Added
    code: code;
    targetIdentifier?: Identifier;
    targetReference?: Reference;
  }
  interface CompositionEvent extends Element {
    code?: CodeableConcept[];
    period?: Period;
    detail?: Reference[];
  }
  interface CompositionSection extends Element {
    title?: string;
    code?: CodeableConcept;
    author?: Reference[]; // R4Added
    focus?: Reference; // R4Added
    text?: Narrative;
    mode?: code;
    orderedBy?: CodeableConcept;
    entry?: Reference[];
    emptyReason?: CodeableConcept;
    section?: CompositionSection[];
  }
  interface ConceptMap extends DomainResource { // TODO R4
    url?: uri;
    identifier?: Identifier;
    version?: string;
    name?: string;
    title?: string;
    status: code;
    experimental?: boolean;
    publisher?: string;
    contact?: ContactDetail[];
    date?: dateTime;
    description?: markdown;
    useContext?: UsageContext[];
    jurisdiction?: CodeableConcept[];
    purpose?: markdown;
    copyright?: markdown;
    sourceUri?: uri;
    sourceReference?: Reference;
    targetUri?: uri;
    targetReference?: Reference;
    group?: ConceptMapGroup[];
  }
  interface ConceptMapGroup extends Element {
    source: uri;
    sourceVersion?: string;
    target?: uri;
    targetVersion?: string;
    element: ConceptMapGroupElement[];
  }
  interface ConceptMapGroupElement extends Element {
    code?: code;
    target?: ConceptMapGroupElementTarget[];
  }
  interface ConceptMapGroupElementTarget extends Element {
    code?: code;
    equivalence?: code;
    comments?: string;
    dependsOn?: ConceptMapGroupElementTargetDependsOn[];
    product?: ConceptMapGroupElementTargetDependsOn[];
  }
  interface ConceptMapGroupElementTargetDependsOn extends Element {
    property: uri;
    system?: uri;
    code: string;
  }
  interface Condition extends DomainResource {
    identifier?: Identifier[];
    // R4Deleted clinicalStatus?: code;
    clinicalStatus?: CodeableConcept; // R4Added
    // R4Deleted verificationStatus?: code;
    verificationStatus?: CodeableConcept; // R4Added
    category?: CodeableConcept[];
    severity?: CodeableConcept;
    code: CodeableConcept;
    bodySite?: CodeableConcept[];
    subject: Reference;
    // R4Deleted context?: Reference;
    encounter?: Reference; // R4Added
    onsetDateTime?: dateTime;
    onsetAge?: Age;
    onsetPeriod?: Period;
    onsetRange?: Range;
    onsetString?: string;
    abatementDateTime?: dateTime;
    abatementAge?: Age;
    // R4Deleted abatementBoolean?: boolean;
    abatementPeriod?: Period;
    abatementRange?: Range;
    abatementString?: string;
    // R4Deleted assertedDate?: date;
    recordedDate?: dateTime; // R4Added
    recorder?: Reference; // R4Added
    asserter?: Reference;
    // R4Deleted stage?: ConditionStage;
    stage?: ConditionStage[]; // R4Added
    evidence?: ConditionEvidence[];
    note?: Annotation[];
  }
  interface ConditionStage extends Element {
    summary?: CodeableConcept;
    assessment?: Reference[];
    type?: CodeableConcept; // R4Added
  }
  interface ConditionEvidence extends Element {
    // R4Deleted code?: CodeableConcept;
    code?: CodeableConcept[]; // R4Added
    detail?: Reference[];
  }
  interface CapabilityStatement extends DomainResource { // TODO R4
    url?: uri;
    version?: string;
    name?: string;
    title?: string;
    status: code;
    experimental?: boolean;
    date: dateTime;
    publisher?: string;
    contact?: ContactDetail[];
    description?: markdown;
    useContext?: UsageContext[];
    jurisdiction?: CodeableConcept[];
    purpose?: markdown;
    copyright?: markdown;
    kind: code;
    instantiates?: uri[];
    software?: CapabilityStatementSoftware;
    implementation?: CapabilityStatementImplementation;
    fhirVersion: id;
    acceptUnknown: code;
    format: code[];
    patchFormat?: code[];
    implementationGuide?: uri[];
    profile?: Reference[];
    rest?: CapabilityStatementRest[];
    messaging?: CapabilityStatementMessaging[];
    document?: CapabilityStatementDocument[];
  }
  interface CapabilityStatementSoftware extends Element {
    name: string;
    version?: string;
    releaseDate?: dateTime;
  }
  interface CapabilityStatementImplementation extends Element {
    description: string;
    url?: uri;
  }
  interface CapabilityStatementRest extends Element {
    mode: code;
    documentation?: string;
    security?: CapabilityStatementRestSecurity;
    resource?: CapabilityStatementRestResource[];
    interaction?: CapabilityStatementRestInteraction[];
    searchParam?: CapabilityStatementRestResourceSearchParam[];
    operation?: CapabilityStatementRestOperation[];
    compartment?: uri[];
  }
  interface CapabilityStatementRestSecurity extends Element {
    cors?: boolean;
    service?: CodeableConcept[];
    description?: string;
    certificate?: CapabilityStatementRestSecurityCertificate[];
  }
  interface CapabilityStatementRestSecurityCertificate extends Element {
    type?: code;
    blob?: base64Binary;
  }
  interface CapabilityStatementRestResource extends Element {
    type: code;
    profile?: Reference;
    documentation?: markdown;
    interaction: CapabilityStatementRestResourceInteraction[];
    versioning?: code;
    readHistory?: boolean;
    updateCreate?: boolean;
    conditionalCreate?: boolean;
    conditionalRead?: code;
    conditionalUpdate?: boolean;
    conditionalDelete?: code;
    referencePolicy?: code[];
    searchInclude?: string[];
    searchRevInclude?: string[];
    searchParam?: CapabilityStatementRestResourceSearchParam[];
  }
  interface CapabilityStatementRestResourceInteraction extends Element {
    code: code;
    documentation?: string;
  }
  interface CapabilityStatementRestResourceSearchParam extends Element {
    name: string;
    definition?: uri;
    type: code;
    documentation?: string;
  }
  interface CapabilityStatementRestInteraction extends Element {
    code: code;
    documentation?: string;
  }
  interface CapabilityStatementRestOperation extends Element {
    name: string;
    definition: Reference;
  }
  interface CapabilityStatementMessaging extends Element {
    endpoint?: CapabilityStatementMessagingEndpoint[];
    reliableCache?: unsignedInt;
    documentation?: string;
    event: CapabilityStatementMessagingEvent[];
  }
  interface CapabilityStatementMessagingEndpoint extends Element {
    protocol: Coding;
    address: uri;
  }
  interface CapabilityStatementMessagingEvent extends Element {
    code: Coding;
    category?: code;
    mode: code;
    focus: code;
    request: Reference;
    response: Reference;
    documentation?: string;
  }
  interface CapabilityStatementDocument extends Element {
    mode: code;
    documentation?: string;
    profile: Reference;
  }
  interface Consent extends DomainResource { // TODO R4
    identifier?: Identifier;
    status: code;
    category?: CodeableConcept[];
    dateTime?: dateTime;
    period?: Period;
    patient: Reference;
    consentor?: Reference[];
    organization?: Reference;
    sourceAttachment?: Attachment;
    sourceIdentifier?: Identifier;
    sourceReference?: Reference;
    policy: uri;
    recipient?: Reference[];
    purpose?: Coding[];
    except?: ConsentExcept[];
  }
  interface ConsentExcept extends Element {
    type: code;
    period?: Period;
    actor?: ConsentExceptActor[];
    action?: CodeableConcept[];
    securityLabel?: Coding[];
    purpose?: Coding[];
    class?: Coding[];
    code?: Coding[];
    data?: ConsentExceptData[];
  }
  interface ConsentExceptActor extends Element {
    role: CodeableConcept;
    reference: Reference;
  }
  interface ConsentExceptData extends Element {
    meaning: code;
    reference: Reference;
  }
  interface Contract extends DomainResource { // TODO R4
    identifier?: Identifier;
    status?: code;
    issued?: dateTime;
    applies?: Period;
    subject?: Reference[];
    topic?: Reference[];
    authority?: Reference[];
    domain?: Reference[];
    type?: CodeableConcept;
    subType?: CodeableConcept[];
    action?: CodeableConcept[];
    actionReason?: CodeableConcept[];
    agent?: ContractAgent[];
    signer?: ContractSigner[];
    valuedItem?: ContractValuedItem[];
    term?: ContractTerm[];
    bindingAttachment?: Attachment;
    bindingReference?: Reference;
    friendly?: ContractFriendly[];
    legal?: ContractLegal[];
    rule?: ContractRule[];
  }
  interface ContractAgent extends Element {
    actor: Reference;
    role?: CodeableConcept[];
  }
  interface ContractSigner extends Element {
    type: Coding;
    party: Reference;
    signature: Signature[];
  }
  interface ContractValuedItem extends Element {
    entityCodeableConcept?: CodeableConcept;
    entityReference?: Reference;
    identifier?: Identifier;
    effectiveTime?: dateTime;
    quantity?: Quantity;
    unitPrice?: Money;
    factor?: decimal;
    points?: decimal;
    net?: Money;
  }
  interface ContractTerm extends Element {
    identifier?: Identifier;
    issued?: dateTime;
    applies?: Period;
    type?: CodeableConcept;
    subType?: CodeableConcept;
    topic?: Reference[];
    action?: CodeableConcept[];
    actionReason?: CodeableConcept[];
    agent?: ContractTermAgent[];
    text?: string;
    valuedItem?: ContractTermValuedItem[];
    group?: ContractTerm[];
  }
  interface ContractTermAgent extends Element {
    actor: Reference;
    role?: CodeableConcept[];
  }
  interface ContractTermValuedItem extends Element {
    entityCodeableConcept?: CodeableConcept;
    entityReference?: Reference;
    identifier?: Identifier;
    effectiveTime?: dateTime;
    quantity?: Quantity;
    unitPrice?: Money;
    factor?: decimal;
    points?: decimal;
    net?: Money;
  }
  interface ContractFriendly extends Element {
    contentAttachment?: Attachment;
    contentReference?: Reference;
  }
  interface ContractLegal extends Element {
    contentAttachment?: Attachment;
    contentReference?: Reference;
  }
  interface ContractRule extends Element {
    contentAttachment?: Attachment;
    contentReference?: Reference;
  }
  interface Coverage extends DomainResource { // TODO R4
    identifier?: Identifier[];
    status?: code;
    type?: CodeableConcept;
    policyHolder?: Reference;
    subscriber?: Reference;
    subscriberId?: string;
    beneficiary?: Reference;
    relationship?: CodeableConcept;
    period?: Period;
    payor?: Reference[];
    group?: CoverageGroup;
    dependent?: string;
    sequence?: string;
    order?: positiveInt;
    network?: string;
    contract?: Reference[];
  }
  interface CoverageGroup extends Element {
    group?: string;
    groupDisplay?: string;
    subGroup?: string;
    subGroupDisplay?: string;
    plan?: string;
    planDisplay?: string;
    subPlan?: string;
    subPlanDisplay?: string;
    class?: string;
    classDisplay?: string;
    subClass?: string;
    subClassDisplay?: string;
  }
  interface DataElement extends DomainResource { // TODO R4
    url?: uri;
    identifier?: Identifier[];
    version?: string;
    status: code;
    experimental?: boolean;
    publisher?: string;
    date?: dateTime;
    name?: string;
    title?: string;
    contact?: ContactDetail[];
    useContext?: UsageContext[];
    jurisdiction?: CodeableConcept[];
    copyright?: markdown;
    stringency?: code;
    mapping?: DataElementMapping[];
    element: ElementDefinition[];
  }
  interface DataElementMapping extends Element {
    identity: id;
    uri?: uri;
    name?: string;
    comment?: string;
  }
  interface DetectedIssue extends DomainResource { // TODO R4
    patient?: Reference;
    category?: CodeableConcept;
    severity?: code;
    implicated?: Reference[];
    detail?: string;
    date?: dateTime;
    author?: Reference;
    identifier?: Identifier;
    reference?: uri;
    mitigation?: DetectedIssueMitigation[];
  }
  interface DetectedIssueMitigation extends Element {
    action: CodeableConcept;
    date?: dateTime;
    author?: Reference;
  }
  interface Device extends DomainResource {
    identifier?: Identifier[];
    definition?: Reference; // R4Added
    // R4Deleted udi?: DeviceUdi;
    udiCarrier?: DeviceUdiCarrier; // R4Added
    status?: code;
    statusReason?: CodeableConcept[]; // R4Added
    distinctIdentifier?: string; // R4Added
    manufacturer?: string;
    manufactureDate?: dateTime;
    expirationDate?: dateTime;
    lotNumber?: string;
    serialNumber?: string; // R4Added
    deviceName?: DeviceDeviceName[]; // R4Added
    // R4Deleted model?: string;
    modelNumber?: string; // R4Added
    partNumber?: string; // R4Added
    type?: CodeableConcept;
    specialization?: DeviceSpecialization[]; // R4Added
    // R4Deleted version?: string;
    version?: DeviceVersion[]; // R4Added
    property?: DeviceProperty[]; // R4Added
    patient?: Reference;
    owner?: Reference;
    contact?: ContactPoint[];
    location?: Reference;
    url?: uri;
    note?: Annotation[];
    safety: CodeableConcept[];
    parennt?: Reference; // R4Added
  }
  interface DeviceUdiCarrier extends Element {
    deviceIdentifier: string;
    name: string;
    jurisdiction: uri;
    carrierHRF:	string;
    carrierAIDC: base64Binary;
    issuer: uri;
    entryType: code;
  }
  interface DeviceDeviceName extends Element { // R4Added
    name: string;
    type: code;
  }
  interface DeviceSpecialization extends Element { // R4Added
    systemType: CodeableConcept;
    version?: string;
  }
  interface DeviceVersion extends Element { // R4Added
    type?: CodeableConcept;
    component?: Identifier;
    value: string;
  }
  interface DeviceProperty extends Element { // R4Added
    type: CodeableConcept;
    valueQuantity?: Quantity[];
    valueCode?: CodeableConcept[];
  }
  interface DeviceComponent extends DomainResource { // TODO R4
    type: CodeableConcept;
    identifier: Identifier;
    lastSystemChange: instant;
    source?: Reference;
    parent?: Reference;
    operationalStatus?: CodeableConcept[];
    parameterGroup?: CodeableConcept;
    measurementPrinciple?: code;
    productionSpecification?: DeviceComponentProductionSpecification[];
    languageCode?: CodeableConcept;
  }
  interface DeviceComponentProductionSpecification extends Element {
    specType?: CodeableConcept;
    componentId?: Identifier;
    productionSpec?: string;
  }
  interface DeviceMetric extends DomainResource { // TODO R4
    type: CodeableConcept;
    identifier: Identifier;
    unit?: CodeableConcept;
    source?: Reference;
    parent?: Reference;
    operationalStatus?: code;
    color?: code;
    category: code;
    measurementPeriod?: Timing;
    calibration?: DeviceMetricCalibration[];
  }
  interface DeviceMetricCalibration extends Element {
    type?: code;
    state?: code;
    time?: instant;
  }
  interface DeviceRequest extends DomainResource {
    identifier?: Identifier[];
    instantiatesCanonical?: canonical[]; // R4Added
    instantiatesUri?: uri[]; // R4Added
    basedOn?: Reference[];
    priorRequest?: Reference[];
    groupIdentifier?: Identifier;
    // R4Deleted definition?: Reference[];
    status?: code;
    // R4Deleted intent: CodeableConcept;
    intent: code; // R4Added
    // R4Deleted priority?: string;
    priority?: code; // R4Added
    codeReference?: Reference;
    codeCodeableConcept?: CodeableConcept;
    parameter?: DeviceRequestParameter[]; // R4Added
    subject: Reference;
    // R4Deleted context?: Reference;
    encounter?: Reference; // R4Added
    occurrenceDateTime?: dateTime;
    occurrencePeriod?: Period;
    occurrenceTiming?: Timing;
    authoredOn?: dateTime;
    // R4Deleted requester?: DeviceRequestRequester;
    requester?: Reference; // R4Added
    performerType?: CodeableConcept;
    performer?: Reference;
    reasonCode?: CodeableConcept[];
    reasonReference?: Reference[];
    insurance?: Reference[]; // R4Added
    supportingInfo?: Reference[];
    note?: Annotation[];
    relevantHistory?: Reference[];
  }
  interface DeviceRequestParameter extends Element { // R4Added
    code?: CodeableConcept;
    valueCodeableConcept?: CodeableConcept;
    valueQuantity?: Quantity;
    valueRange?: Range;
    valueBoolean?: boolean;
  }
  /* R4Deleted interface DeviceRequestRequester extends Element {
      agent?: Reference;
      onBehalfOf?: Reference;
    }*/
  interface DeviceUseStatement extends DomainResource { // TODO R4
    bodySite?: CodeableConcept;
    whenUsed?: Period;
    device: Reference;
    identifier?: Identifier[];
    indication?: CodeableConcept[];
    notes?: string[];
    recordedOn?: dateTime;
    subject: Reference;
    timingTiming?: Timing;
    timingPeriod?: Period;
    timingDateTime?: dateTime;
  }
  interface DiagnosticRequest extends DomainResource { // TODO R4
    identifier?: Identifier[];
    definition?: Reference[];
    basedOn?: Reference[];
    replaces?: Reference[];
    requisition?: Identifier;
    status: code;
    intent: code;
    priority?: code;
    code: CodeableConcept;
    subject: Reference;
    context?: Reference;
    occurrenceDateTime?: dateTime;
    occurrencePeriod?: Period;
    occurrenceTiming?: Timing;
    authoredOn?: dateTime;
    requester?: Reference;
    performerType?: CodeableConcept;
    performer?: Reference;
    reasonCode?: CodeableConcept[];
    reasonReference?: Reference[];
    supportingInformation?: Reference[];
    note?: Annotation[];
    relevantHistory?: Reference[];
  }
  interface DiagnosticReport extends DomainResource { // TODO R4
    identifier?: Identifier[];
    status: code;
    category?: CodeableConcept;
    code: CodeableConcept;
    subject?: Reference;
    encounter?: Reference;
    effectiveDateTime?: dateTime;
    effectivePeriod?: Period;
    issued?: instant;
    performer?: Reference[];
    request?: Reference[];
    specimen?: Reference[];
    result?: Reference[];
    imagingStudy?: Reference[];
    image?: DiagnosticReportImage[];
    conclusion?: string;
    codedDiagnosis?: CodeableConcept[];
    presentedForm?: Attachment[];
  }
  interface DiagnosticReportImage extends Element {
    comment?: string;
    link: Reference;
  }
  interface DocumentManifest extends DomainResource { // TODO R4
    masterIdentifier?: Identifier;
    identifier?: Identifier[];
    subject?: Reference;
    recipient?: Reference[];
    type?: CodeableConcept;
    author?: Reference[];
    created?: dateTime;
    source?: uri;
    status: code;
    description?: string;
    content: DocumentManifestContent[];
    related?: DocumentManifestRelated[];
  }
  interface DocumentManifestContent extends Element {
    pAttachment?: Attachment;
    pReference?: Reference;
  }
  interface DocumentManifestRelated extends Element {
    identifier?: Identifier;
    ref?: Reference;
  }
  interface DocumentReference extends DomainResource { // TODO R4
    masterIdentifier?: Identifier;
    identifier?: Identifier[];
    subject?: Reference;
    type: CodeableConcept;
    class?: CodeableConcept;
    author?: Reference[];
    custodian?: Reference;
    authenticator?: Reference;
    created?: dateTime;
    indexed: instant;
    status: code;
    docStatus?: CodeableConcept;
    relatesTo?: DocumentReferenceRelatesTo[];
    description?: string;
    securityLabel?: CodeableConcept[];
    content: DocumentReferenceContent[];
    context?: DocumentReferenceContext;
  }
  interface DocumentReferenceRelatesTo extends Element {
    code: code;
    target: Reference;
  }
  interface DocumentReferenceContent extends Element {
    attachment: Attachment;
    format?: Coding[];
  }
  interface DocumentReferenceContext extends Element {
    encounter?: Reference;
    event?: CodeableConcept[];
    period?: Period;
    facilityType?: CodeableConcept;
    practiceSetting?: CodeableConcept;
    sourcePatientInfo?: Reference;
    related?: DocumentReferenceContextRelated[];
  }
  interface DocumentReferenceContextRelated extends Element {
    identifier?: Identifier;
    ref?: Reference;
  }
  interface EligibilityRequest extends DomainResource { // TODO R4
    identifier?: Identifier[];
    status?: code;
    priority?: CodeableConcept;
    patient?: Reference;
    servicedDate?: date;
    servicedPeriod?: Period;
    created?: dateTime;
    enterer?: Reference;
    provider?: Reference;
    organization?: Reference;
    insurer?: Reference;
    facility?: Reference;
    coverage?: Reference;
    businessArrangement?: string;
    benefitCategory?: CodeableConcept;
    benefitSubCategory?: CodeableConcept;
  }
  interface EligibilityResponse extends DomainResource { // TODO R4
    identifier?: Identifier[];
    status?: code;
    created?: dateTime;
    requestProvider?: Reference;
    requestOrganization?: Reference;
    request?: Reference;
    outcome?: CodeableConcept;
    disposition?: string;
    insurer?: Reference;
    inforce?: boolean;
    insurance?: EligibilityResponseInsurance[];
    form?: CodeableConcept;
    error?: EligibilityResponseError[];
  }
  interface EligibilityResponseInsurance extends Element {
    coverage?: Reference;
    contract?: Reference;
    benefitBalance?: EligibilityResponseInsuranceBenefitBalance[];
  }
  interface EligibilityResponseInsuranceBenefitBalance extends Element {
    category: CodeableConcept;
    subCategory?: CodeableConcept;
    excluded?: boolean;
    name?: string;
    description?: string;
    network?: CodeableConcept;
    unit?: CodeableConcept;
    term?: CodeableConcept;
    financial?: EligibilityResponseInsuranceBenefitBalanceFinancial[];
  }
  interface EligibilityResponseInsuranceBenefitBalanceFinancial extends Element {
    type: CodeableConcept;
    benefitUnsignedInt?: unsignedInt;
    benefitString?: string;
    benefitMoney?: Money;
    benefitUsedUnsignedInt?: unsignedInt;
    benefitUsedMoney?: Money;
  }
  interface EligibilityResponseError extends Element {
    code: CodeableConcept;
  }
  interface Encounter extends DomainResource {
    identifier?: Identifier[];
    status: code;
    statusHistory?: EncounterStatusHistory[];
    // R4Deleted class?: Coding;
    class: Coding; // R4Added
    classHistory?: EncounterClassHistory[];
    type?: CodeableConcept[];
    serviceType?: CodeableConcept; // R4Added
    priority?: CodeableConcept;
    subject?: Reference;
    episodeOfCare?: Reference[];
    // R4Deleted incomingReferral?: Reference[];
    basedOn?: Reference[]; // R4Added
    participant?: EncounterParticipant[];
    // R4Deleted appointment?: Reference;
    appointment?: Reference[]; // R4Added
    period?: Period;
    length?: Duration;
    // R4Deleted reason?: CodeableConcept[];
    reasonCode?: CodeableConcept[]; // R4Added
    reasonReference?: CodeableConcept[]; // R4Added
    diagnosis?: EncounterDiagnosis[];
    account?: Reference[];
    hospitalization?: EncounterHospitalization;
    location?: EncounterLocation[];
    serviceProvider?: Reference;
    partOf?: Reference;
  }
  interface EncounterStatusHistory extends Element {
    status: code;
    period: Period;
  }
  interface EncounterClassHistory extends Element {
    class: Coding;
    period: Period;
  }
  interface EncounterParticipant extends Element {
    type?: CodeableConcept[];
    period?: Period;
    individual?: Reference;
  }
  interface EncounterDiagnosis extends Element {
    condition: Reference;
    // R4Deleted role?: CodeableConcept;
    use?: CodeableConcept; // R4Added
    rank: positiveInt;
  }
  interface EncounterHospitalization extends Element {
    preAdmissionIdentifier?: Identifier;
    // R4Deleted origin?: Reference;
    admitSource?: CodeableConcept;
    reAdmission?: CodeableConcept;
    dietPreference?: CodeableConcept[];
    specialCourtesy?: CodeableConcept[];
    specialArrangement?: CodeableConcept[];
    destination?: Reference;
    dischargeDisposition?: CodeableConcept;
  }
  interface EncounterLocation extends Element {
    location: Reference;
    status?: code;
    physicalType?: CodeableConcept; // R4Added
    period?: Period;
  }
  interface Endpoint extends DomainResource { // TODO R4
    identifier?: Identifier[];
    status: code;
    name?: string;
    managingOrganization?: Reference;
    contact?: ContactPoint[];
    period?: Period;
    connectionType: Coding;
    payloadType: CodeableConcept[];
    payloadMimeType?: code[];
    address: uri;
    header?: string[];
    publicKey?: string;
  }
  interface EnrollmentRequest extends DomainResource { // TODO R4
    identifier?: Identifier[];
    status?: code;
    created?: dateTime;
    insurer?: Reference;
    provider?: Reference;
    organization?: Reference;
    subject?: Reference;
    coverage?: Reference;
  }
  interface EnrollmentResponse extends DomainResource { // TODO R4
    identifier?: Identifier[];
    status?: code;
    request?: Reference;
    outcome?: CodeableConcept;
    disposition?: string;
    created?: dateTime;
    organization?: Reference;
    requestProvider?: Reference;
    requestOrganization?: Reference;
  }
  interface EpisodeOfCare extends DomainResource {
    identifier?: Identifier[];
    status: code;
    statusHistory?: EpisodeOfCareStatusHistory[];
    type?: CodeableConcept[];
    // R4Deleted condition?: Reference[];
    diagnosis?: EpisodeOfCareDiagnosis[]; // R4Added
    patient: Reference;
    managingOrganization?: Reference;
    period?: Period;
    referralRequest?: Reference[];
    careManager?: Reference;
    team?: Reference[];
    account?: Reference[];
  }
  interface EpisodeOfCareStatusHistory extends Element {
    status: code;
    period: Period;
  }
  interface EpisodeOfCareDiagnosis extends Element { // R4Added
    condition: Reference;
    role?: CodeableConcept;
    rank?: positiveInt;
  }
  interface ExpansionProfile extends DomainResource { // TODO R4
    url?: uri;
    identifier?: Identifier;
    version?: string;
    name?: string;
    status: code;
    experimental?: boolean;
    publisher?: string;
    contact?: ContactDetail[];
    date?: dateTime;
    description?: markdown;
    useContext?: UsageContext[];
    jurisdiction?: CodeableConcept[];
    fixedVersion?: ExpansionProfileFixedVersion[];
    excludedSystem?: ExpansionProfileExcludedSystem;
    includeDesignations?: boolean;
    designation?: ExpansionProfileDesignation;
    includeDefinition?: boolean;
    activeOnly?: boolean;
    excludeNested?: boolean;
    excludeNotForUI?: boolean;
    excludePostCoordinated?: boolean;
    displayLanguage?: code;
    limitedExpansion?: boolean;
  }
  interface ExpansionProfileFixedVersion extends Element {
    system: uri;
    version: string;
    mode: code;
  }
  interface ExpansionProfileExcludedSystem extends Element {
    system: uri;
    version?: string;
  }
  interface ExpansionProfileDesignation extends Element {
    include?: ExpansionProfileDesignationInclude;
    exclude?: ExpansionProfileDesignationExclude;
  }
  interface ExpansionProfileDesignationInclude extends Element {
    designation?: ExpansionProfileDesignationIncludeDesignation[];
  }
  interface ExpansionProfileDesignationIncludeDesignation extends Element {
    language?: code;
    use?: Coding;
  }
  interface ExpansionProfileDesignationExclude extends Element {
    designation?: ExpansionProfileDesignationExcludeDesignation[];
  }
  interface ExpansionProfileDesignationExcludeDesignation extends Element {
    language?: code;
    use?: Coding;
  }
  interface ExplanationOfBenefit extends DomainResource { // TODO R4
    identifier?: Identifier[];
    status?: code;
    type?: CodeableConcept;
    subType?: CodeableConcept[];
    patient?: Reference;
    billablePeriod?: Period;
    created?: dateTime;
    enterer?: Reference;
    insurer?: Reference;
    provider?: Reference;
    organization?: Reference;
    referral?: Reference;
    facility?: Reference;
    claim?: Reference;
    claimResponse?: Reference;
    outcome?: CodeableConcept;
    disposition?: string;
    related?: ExplanationOfBenefitRelated[];
    prescription?: Reference;
    originalPrescription?: Reference;
    payee?: ExplanationOfBenefitPayee;
    information?: ExplanationOfBenefitInformation[];
    careTeam?: ExplanationOfBenefitCareTeam[];
    diagnosis?: ExplanationOfBenefitDiagnosis[];
    procedure?: ExplanationOfBenefitProcedure[];
    precedence?: positiveInt;
    insurance?: ExplanationOfBenefitInsurance;
    accident?: ExplanationOfBenefitAccident;
    employmentImpacted?: Period;
    hospitalization?: Period;
    item?: ExplanationOfBenefitItem[];
    addItem?: ExplanationOfBenefitAddItem[];
    totalCost?: Money;
    unallocDeductable?: Money;
    totalBenefit?: Money;
    payment?: ExplanationOfBenefitPayment;
    form?: CodeableConcept;
    note?: ExplanationOfBenefitNote[];
    benefitBalance?: ExplanationOfBenefitBenefitBalance[];
  }
  interface ExplanationOfBenefitRelated extends Element {
    claim?: Reference;
    relationship?: CodeableConcept;
    reference?: Identifier;
  }
  interface ExplanationOfBenefitPayee extends Element {
    type?: CodeableConcept;
    resourceType?: CodeableConcept;
    partyIdentifier?: Identifier;
    partyReference?: Reference;
  }
  interface ExplanationOfBenefitInformation extends Element {
    category: CodeableConcept;
    code?: CodeableConcept;
    timingDate?: date;
    timingPeriod?: Period;
    valueString?: string;
    valueQuantity?: Quantity;
    valueAttachment?: Attachment;
    valueReference?: Reference;
    reason?: Coding;
  }
  interface ExplanationOfBenefitCareTeam extends Element {
    sequence: positiveInt;
    provider: Reference;
    responsible?: boolean;
    role?: CodeableConcept;
    qualification?: CodeableConcept;
  }
  interface ExplanationOfBenefitDiagnosis extends Element {
    sequence: positiveInt;
    diagnosisCodeableConcept?: CodeableConcept;
    diagnosisReference?: Reference;
    type?: CodeableConcept[];
    packageCode?: CodeableConcept;
  }
  interface ExplanationOfBenefitProcedure extends Element {
    sequence: positiveInt;
    date?: dateTime;
    procedureCodeableConcept?: CodeableConcept;
    procedureReference?: Reference;
  }
  interface ExplanationOfBenefitInsurance extends Element {
    coverage?: Reference;
    preAuthRef?: string[];
  }
  interface ExplanationOfBenefitAccident extends Element {
    date?: date;
    type?: CodeableConcept;
    locationAddress?: Address;
    locationReference?: Reference;
  }
  interface ExplanationOfBenefitItem extends Element {
    sequence: positiveInt;
    careTeamLinkId?: positiveInt[];
    diagnosisLinkId?: positiveInt[];
    procedureLinkId?: positiveInt[];
    informationLinkId?: positiveInt[];
    revenue?: CodeableConcept;
    category?: CodeableConcept;
    service?: CodeableConcept;
    modifier?: CodeableConcept[];
    programCode?: CodeableConcept[];
    servicedDate?: date;
    servicedPeriod?: Period;
    locationCodeableConcept?: CodeableConcept;
    locationAddress?: Address;
    locationReference?: Reference;
    quantity?: Quantity;
    unitPrice?: Money;
    factor?: decimal;
    net?: Money;
    udi?: Reference[];
    bodySite?: CodeableConcept;
    subSite?: CodeableConcept[];
    noteNumber?: positiveInt[];
    adjudication?: ExplanationOfBenefitItemAdjudication[];
    detail?: ExplanationOfBenefitItemDetail[];
    prosthesis?: ExplanationOfBenefitItemProsthesis;
  }
  interface ExplanationOfBenefitItemAdjudication extends Element {
    category: CodeableConcept;
    reason?: CodeableConcept;
    amount?: Money;
    value?: decimal;
  }
  interface ExplanationOfBenefitItemDetail extends Element {
    sequence: positiveInt;
    type: CodeableConcept;
    revenue?: CodeableConcept;
    category?: CodeableConcept;
    service?: CodeableConcept;
    modifier?: CodeableConcept[];
    programCode?: CodeableConcept[];
    quantity?: Quantity;
    unitPrice?: Money;
    factor?: decimal;
    net?: Money;
    udi?: Reference[];
    noteNumber?: positiveInt[];
    adjudication?: ExplanationOfBenefitItemAdjudication[];
    subDetail?: ExplanationOfBenefitItemDetailSubDetail[];
  }
  interface ExplanationOfBenefitItemDetailSubDetail extends Element {
    sequence: positiveInt;
    type: CodeableConcept;
    revenue?: CodeableConcept;
    category?: CodeableConcept;
    service?: CodeableConcept;
    modifier?: CodeableConcept[];
    programCode?: CodeableConcept[];
    quantity?: Quantity;
    unitPrice?: Money;
    factor?: decimal;
    net?: Money;
    udi?: Reference[];
    noteNumber?: positiveInt[];
    adjudication?: ExplanationOfBenefitItemAdjudication[];
  }
  interface ExplanationOfBenefitItemProsthesis extends Element {
    initial?: boolean;
    priorDate?: date;
    priorMaterial?: CodeableConcept;
  }
  interface ExplanationOfBenefitAddItem extends Element {
    sequenceLinkId?: positiveInt[];
    revenue?: CodeableConcept;
    category?: CodeableConcept;
    service?: CodeableConcept;
    modifier?: CodeableConcept[];
    fee?: Money;
    noteNumber?: positiveInt[];
    adjudication?: ExplanationOfBenefitItemAdjudication[];
    detail?: ExplanationOfBenefitAddItemDetail[];
  }
  interface ExplanationOfBenefitAddItemDetail extends Element {
    revenue?: CodeableConcept;
    category?: CodeableConcept;
    service?: CodeableConcept;
    modifier?: CodeableConcept[];
    fee?: Money;
    noteNumber?: positiveInt[];
    adjudication?: ExplanationOfBenefitItemAdjudication[];
  }
  interface ExplanationOfBenefitPayment extends Element {
    type?: CodeableConcept;
    adjustment?: Money;
    adjustmentReason?: CodeableConcept;
    date?: date;
    amount?: Money;
    identifier?: Identifier;
  }
  interface ExplanationOfBenefitNote extends Element {
    number?: positiveInt;
    type?: CodeableConcept;
    text?: string;
    language?: CodeableConcept;
  }
  interface ExplanationOfBenefitBenefitBalance extends Element {
    category: CodeableConcept;
    subCategory?: CodeableConcept;
    excluded?: boolean;
    name?: string;
    description?: string;
    network?: CodeableConcept;
    unit?: CodeableConcept;
    term?: CodeableConcept;
    financial?: ExplanationOfBenefitBenefitBalanceFinancial[];
  }
  interface ExplanationOfBenefitBenefitBalanceFinancial extends Element {
    type: CodeableConcept;
    benefitUnsignedInt?: unsignedInt;
    benefitString?: string;
    benefitMoney?: Money;
    benefitUsedUnsignedInt?: unsignedInt;
    benefitUsedMoney?: Money;
  }
  interface FamilyMemberHistory extends DomainResource {
    identifier?: Identifier[];
    instantiatesCanonical?: canonical[]; // R4Added
    instantiatesUri?: uri[]; // R4Added
    status: code;
    dataAbsentReason?: CodeableConcept; // R4Added
    patient: Reference;
    date?: dateTime;
    // R4Deleted notDone?: boolean;
    // R4Deleted notDoneReason?: CodeableConcept;
    name?: string;
    relationship: CodeableConcept;
    // R4Deleted gender?: code;
    sex?: CodeableConcept; // R4Added
    bornPeriod?: Period;
    bornDate?: date;
    bornString?: string;
    ageAge?: Age;
    ageRange?: Range;
    ageString?: string;
    estimatedAge?: boolean;
    deceasedBoolean?: boolean;
    deceasedAge?: Age;
    deceasedRange?: Range;
    deceasedDate?: date;
    deceasedString?: string;
    reasonCode?: CodeableConcept[];
    reasonReference?: Reference[];
    note?: Annotation[];
    condition?: FamilyMemberHistoryCondition[];
  }
  interface FamilyMemberHistoryCondition extends Element {
    code: CodeableConcept;
    outcome?: CodeableConcept;
    contributedToDeath?: boolean; // R4Added
    onsetAge?: Age;
    onsetRange?: Range;
    onsetPeriod?: Period;
    onsetString?: string;
    note?: Annotation[];
  }
  interface Flag extends DomainResource { // TODO R4
    identifier?: Identifier[];
    category?: CodeableConcept;
    status: code;
    period?: Period;
    subject: Reference;
    encounter?: Reference;
    author?: Reference;
    code: CodeableConcept;
  }
  interface Goal extends DomainResource {
    identifier?: Identifier[];
    // R4Deleted status: code;
    lifecycleStatus: code; // R4Added
    achievementStatus?: CodeableConcept; // R4Added
    category?: CodeableConcept[];
    priority?: CodeableConcept;
    description: CodeableConcept;
    // R4Deleted subject?: Reference;
    subject: Reference; // R4Added
    startDate?: date;
    startCodeableConcept?: CodeableConcept;
    // R4Deleted target: GoalTarget;
    target: GoalTarget[]; // R4Added
    statusDate?: date;
    statusReason?: string;
    expressedBy?: Reference;
    addresses?: Reference[];
    note?: Annotation[];
    outcomeCode?: CodeableConcept[];
    outcomeReference?: Reference[];
  }
  interface GoalTarget extends Element {
    measure?: CodeableConcept;
    detailQuantity?: Quantity;
    detailRange?: Range;
    detailCodeableConcept?: CodeableConcept;
    detailString?: string; // R4Added
    detailBoolean?: boolean; // R4Added
    detailInteger?: integer; // R4Added
    detailRatio?: Ratio; // R4Added
    dueDate?: date;
    dueDuration?: Duration;
  }
  /* R4Deleted interface GoalOutcome extends Element {
      resultCodeableConcept?: CodeableConcept;
      resultReference?: Reference;
    }*/
  interface Group extends DomainResource { // TODO R4
    identifier?: Identifier[];
    type: code;
    actual: boolean;
    active?: boolean;
    code?: CodeableConcept;
    name?: string;
    quantity?: unsignedInt;
    characteristic?: GroupCharacteristic[];
    member?: GroupMember[];
  }
  interface GroupCharacteristic extends Element {
    code: CodeableConcept;
    valueCodeableConcept?: CodeableConcept;
    valueBoolean?: boolean;
    valueQuantity?: Quantity;
    valueRange?: Range;
    exclude: boolean;
    period?: Period;
  }
  interface GroupMember extends Element {
    entity: Reference;
    period?: Period;
    inactive?: boolean;
  }
  interface GuidanceResponse extends DomainResource { // TODO R4
    requestId?: id;
    identifier?: Identifier;
    module: Reference;
    status: code;
    subject?: Reference;
    context?: Reference;
    occurrenceDateTime?: dateTime;
    performer?: Reference;
    reasonCodeableConcept?: CodeableConcept;
    reasonReference?: Reference;
    note?: Annotation[];
    evaluationMessage?: Reference[];
    outputParameters?: Reference;
    result?: Reference;
    dataRequirement?: DataRequirement[];
  }
  interface HealthcareService extends DomainResource { // TODO R4
    identifier?: Identifier[];
    active?: boolean;
    providedBy?: Reference;
    serviceCategory?: CodeableConcept;
    serviceType?: CodeableConcept[];
    specialty?: CodeableConcept[];
    location?: Reference[];
    serviceName?: string;
    comment?: string;
    extraDetails?: string;
    photo?: Attachment;
    telecom?: ContactPoint[];
    coverageArea?: Reference[];
    serviceProvisionCode?: CodeableConcept[];
    eligibility?: CodeableConcept;
    eligibilityNote?: string;
    programName?: string[];
    characteristic?: CodeableConcept[];
    referralMethod?: CodeableConcept[];
    publicKey?: string;
    appointmentRequired?: boolean;
    availableTime?: HealthcareServiceAvailableTime[];
    notAvailable?: HealthcareServiceNotAvailable[];
    availabilityExceptions?: string;
    endpoint?: Reference[];
  }
  interface HealthcareServiceAvailableTime extends Element {
    daysOfWeek?: code[];
    allDay?: boolean;
    availableStartTime?: time;
    availableEndTime?: time;
  }
  interface HealthcareServiceNotAvailable extends Element {
    description: string;
    during?: Period;
  }
  interface ImagingManifest extends DomainResource { // TODO R4
    uid?: oid;
    patient: Reference;
    authoringTime?: dateTime;
    author?: Reference;
    title: CodeableConcept;
    description?: string;
    study: ImagingManifestStudy[];
  }
  interface ImagingManifestStudy extends Element {
    uid: oid;
    imagingStudy?: Reference;
    baseLocation?: ImagingManifestStudyBaseLocation[];
    series: ImagingManifestStudySeries[];
  }
  interface ImagingManifestStudyBaseLocation extends Element {
    type: Coding;
    url: uri;
  }
  interface ImagingManifestStudySeries extends Element {
    uid: oid;
    baseLocation?: ImagingManifestStudySeriesBaseLocation[];
    instance: ImagingManifestStudySeriesInstance[];
  }
  interface ImagingManifestStudySeriesBaseLocation extends Element {
    type: Coding;
    url: uri;
  }
  interface ImagingManifestStudySeriesInstance extends Element {
    sopClass: oid;
    uid: oid;
  }
  interface Immunization extends DomainResource {
    identifier?: Identifier[];
    status: code;
    statusReason?: CodeableConcept; // R4Added
    vaccineCode: CodeableConcept;
    patient: Reference;
    encounter?: Reference;
    // R4Deleted date?: dateTime;
    occurrenceDateTime?: dateTime; // R4Added
    occurrenceString?: string; // R4Added
    recorded?: dateTime; // R4Added
    // R4Deleted notGiven: boolean;
    primarySource: boolean;
    reportOrigin?: CodeableConcept;
    location?: Reference;
    manufacturer?: Reference;
    lotNumber?: string;
    expirationDate?: date;
    site?: CodeableConcept;
    route?: CodeableConcept;
    doseQuantity?: SimpleQuantity;
    // R4Deleted performer?: Reference;
    performer?: ImmunizationPerfomer[]; // R4Added
    // R4Deleted requester?: Reference;
    note?: Annotation[];
    reasonCode?: CodeableConcept[]; // R4Added
    reasonReference?: Reference[]; // R4Added
    isSubpotent?: boolean; // R4Added
    subpotentReason?: CodeableConcept[]; // R4Added
    education?: ImmunizationEducation[]; // R4Added
    programEligibility?: CodeableConcept[]; // R4Added
    fundingSource?: CodeableConcept; // R4Added
    reaction?: ImmunizationReaction[];

    // R4Deleted explanation?: ImmunizationExplanation;
    // R4Deleted vaccinationProtocol?: ImmunizationVaccinationProtocol[];
    protocolApplied?: ImmunizationProtocolApplied[];
  }
  interface ImmunizationPerfomer extends Element { // R4Added
    function?: CodeableConcept;
    actor: Reference
  }
  interface ImmunizationEducation extends Element { // R4Added
    documentType?: string;
    reference?: uri;
    publicationDate?: dateTime;
    presentationDate?: dateTime;
  }
  /* R4Deleted interface ImmunizationExplanation extends Element {
      reason?: CodeableConcept[];
      reasonNotGiven?: CodeableConcept[];
    }*/
  interface ImmunizationReaction extends Element {
    date?: dateTime;
    detail?: Reference;
    reported?: boolean;
  }
  interface ImmunizationProtocolApplied extends Element { // R4Added name change
    series?: string;
    authority?: Reference;
    targetDisease: CodeableConcept[];
    doseNumberPositiveInt?: positiveInt; // R4Added
    doseNumberString?: string; // R4Added
    // R4Deleted doseSequence?: positiveInt;
    // R4Deleted description?: string;
    // R4Deleted seriesDoses?: positiveInt;
    seriesDosesPositiveInt?: positiveInt; // R4Added
    seriesDosesString?: string; // R4Added
    // R4Deleted doseStatus: CodeableConcept;
    // R4Deleted doseStatusReason?: CodeableConcept;
  }
  interface ImmunizationRecommendation extends DomainResource { // TODO R4
    identifier?: Identifier[];
    patient: Reference;
    recommendation: ImmunizationRecommendationRecommendation[];
  }
  interface ImmunizationRecommendationRecommendation extends Element {
    date: dateTime;
    vaccineCode: CodeableConcept;
    doseNumber?: positiveInt;
    forecastStatus: CodeableConcept;
    dateCriterion?: ImmunizationRecommendationRecommendationDateCriterion[];
    protocol?: ImmunizationRecommendationRecommendationProtocol;
    supportingImmunization?: Reference[];
    supportingPatientInformation?: Reference[];
  }
  interface ImmunizationRecommendationRecommendationDateCriterion extends Element {
    code: CodeableConcept;
    value: dateTime;
  }
  interface ImmunizationRecommendationRecommendationProtocol extends Element {
    doseSequence?: positiveInt;
    description?: string;
    authority?: Reference;
    series?: string;
  }
  interface ImplementationGuide extends DomainResource { // TODO R4
    url: uri;
    version?: string;
    name: string;
    status: code;
    experimental?: boolean;
    publisher?: string;
    contact?: ContactDetail[];
    date?: dateTime;
    description?: markdown;
    useContext?: UsageContext[];
    jurisdiction?: CodeableConcept[];
    copyright?: markdown;
    fhirVersion?: id;
    dependency?: ImplementationGuideDependency[];
    package?: ImplementationGuidePackage[];
    global?: ImplementationGuideGlobal[];
    binary?: uri[];
    page?: ImplementationGuidePage;
  }
  interface ImplementationGuideDependency extends Element {
    type: code;
    uri: uri;
  }
  interface ImplementationGuidePackage extends Element {
    name: string;
    description?: string;
    resource: ImplementationGuidePackageResource[];
  }
  interface ImplementationGuidePackageResource extends Element {
    example: boolean;
    name?: string;
    description?: string;
    acronym?: string;
    sourceUri?: uri;
    sourceReference?: Reference;
    exampleFor?: Reference;
  }
  interface ImplementationGuideGlobal extends Element {
    type: code;
    profile: Reference;
  }
  interface ImplementationGuidePage extends Element {
    source: uri;
    title: string;
    kind: code;
    type?: code[];
    package?: string[];
    format?: code;
    page?: ImplementationGuidePage[];
  }
  interface Library extends DomainResource { // TODO R4
    url?: uri;
    identifier?: Identifier[];
    version?: string;
    name?: string;
    title?: string;
    type: CodeableConcept;
    status: code;
    experimental?: boolean;
    date?: dateTime;
    description?: markdown;
    purpose?: markdown;
    usage?: string;
    approvalDate?: date;
    lastReviewDate?: date;
    effectivePeriod?: Period;
    useContext?: UsageContext[];
    jurisdiction?: CodeableConcept[];
    topic?: CodeableConcept[];
    contributor?: Contributor[];
    publisher?: string;
    contact?: ContactDetail[];
    copyright?: markdown;
    relatedArtifact?: RelatedArtifact[];
    parameter?: ParameterDefinition[];
    dataRequirement?: DataRequirement[];
    content?: Attachment[];
  }
  interface Linkage extends DomainResource { // TODO R4
    author?: Reference;
    item: LinkageItem[];
  }
  interface LinkageItem extends Element {
    type: code;
    resource: Reference;
  }
  interface List extends DomainResource { // TODO R4
    identifier?: Identifier[];
    status: code;
    mode: code;
    title?: string;
    code?: CodeableConcept;
    subject?: Reference;
    encounter?: Reference;
    date?: dateTime;
    source?: Reference;
    orderedBy?: CodeableConcept;
    note?: Annotation[];
    entry?: ListEntry[];
    emptyReason?: CodeableConcept;
  }
  interface ListEntry extends Element {
    flag?: CodeableConcept;
    deleted?: boolean;
    date?: dateTime;
    item: Reference;
  }
  interface Location extends DomainResource {
    identifier?: Identifier[];
    status?: code;
    operationalStatus?: Coding;
    name?: string;
    alias?: string[];
    description?: string;
    mode?: code;
    // R4Deleted type?: CodeableConcept;
    type?: CodeableConcept[]; // R4Added
    telecom?: ContactPoint[];
    address?: Address;
    physicalType?: CodeableConcept;
    position?: LocationPosition;
    managingOrganization?: Reference;
    partOf?: Reference;
    hoursOfOperation?: LocationHoursOfOperation[]; // R4Added
    availabilityExceptions?: string; // R4Added
    endpoint?: Reference[];
  }
  interface LocationPosition extends Element {
    longitude: decimal;
    latitude: decimal;
    altitude?: decimal;
  }
  interface LocationHoursOfOperation extends Element { // R4Added
    daysOfWeek?: code[];
    allDay?: boolean;
    openingTime?: time;
    closingTime?: time;
  }
  interface Measure extends DomainResource { // TODO R4
    url?: uri;
    identifier?: Identifier[];
    version?: string;
    name?: string;
    title?: string;
    status: code;
    experimental?: boolean;
    date?: dateTime;
    description?: markdown;
    purpose?: markdown;
    usage?: string;
    approvalDate?: date;
    lastReviewDate?: date;
    effectivePeriod?: Period;
    useContext?: UsageContext[];
    jurisdiction?: CodeableConcept[];
    topic?: CodeableConcept[];
    contributor?: Contributor[];
    publisher?: string;
    contact?: ContactDetail[];
    copyright?: markdown;
    relatedArtifact?: RelatedArtifact[];
    library?: Reference[];
    disclaimer?: markdown;
    scoring?: code;
    compositeScoring?: code;
    type?: code[];
    riskAdjustment?: string;
    rateAggregation?: string;
    rationale?: markdown;
    clinicalRecommendationStatement?: markdown;
    improvementNotation?: string;
    definition?: markdown;
    guidance?: markdown;
    set?: string;
    group?: MeasureGroup[];
    supplementalData?: MeasureSupplementalData[];
  }
  interface MeasureGroup extends Element {
    identifier: Identifier;
    name?: string;
    description?: string;
    population?: MeasureGroupPopulation[];
    stratifier?: MeasureGroupStratifier[];
  }
  interface MeasureGroupPopulation extends Element {
    type: code;
    identifier: Identifier;
    name?: string;
    description?: string;
    criteria: string;
  }
  interface MeasureGroupStratifier extends Element {
    identifier: Identifier;
    criteria?: string;
    path?: string;
  }
  interface MeasureSupplementalData extends Element {
    identifier: Identifier;
    usage?: code[];
    criteria?: string;
    path?: string;
  }
  interface MeasureReport extends DomainResource { // TODO R4
    measure: Reference;
    type: code;
    patient?: Reference;
    period: Period;
    status: code;
    date?: dateTime;
    reportingOrganization?: Reference;
    group?: MeasureReportGroup[];
    evaluatedResources?: Reference;
  }
  interface MeasureReportGroup extends Element {
    identifier: Identifier;
    population?: MeasureReportGroupPopulation[];
    measureScore?: decimal;
    stratifier?: MeasureReportGroupStratifier[];
    supplementalData?: MeasureReportGroupSupplementalData[];
  }
  interface MeasureReportGroupPopulation extends Element {
    type: code;
    count?: integer;
    patients?: Reference;
  }
  interface MeasureReportGroupStratifier extends Element {
    identifier: Identifier;
    group?: MeasureReportGroupStratifierGroup[];
  }
  interface MeasureReportGroupStratifierGroup extends Element {
    value: string;
    population?: MeasureReportGroupStratifierGroupPopulation[];
    measureScore?: decimal;
  }
  interface MeasureReportGroupStratifierGroupPopulation extends Element {
    type: code;
    count?: integer;
    patients?: Reference;
  }
  interface MeasureReportGroupSupplementalData extends Element {
    identifier: Identifier;
    group?: MeasureReportGroupSupplementalDataGroup[];
  }
  interface MeasureReportGroupSupplementalDataGroup extends Element {
    value: string;
    count?: integer;
    patients?: Reference;
  }
  interface Media extends DomainResource {
    identifier?: Identifier[];
    basedOn?: Reference[]; // R4Added
    partOf?: Reference[]; // R4Added
    status: code; // R4Added
    // R4Deleted type: code;
    type?: CodeableConcept; // R4Added
    modality?: CodeableConcept; // R4Added
    view?: CodeableConcept;
    subject?: Reference;
    encounter?: Reference; // R4Added
    createdDateTime?: dateTime; // R4Added
    createdPeriod?: Period; // R4Added
    issued?: instant; // R4Added
    operator?: Reference;
    reasonCode?: CodeableConcept[]; // R4Added
    bodySite?: CodeableConcept;
    // R4Deleted subtype?: CodeableConcept;
    deviceName?: string;
    device?: Reference; // R4Added
    height?: positiveInt;
    width?: positiveInt;
    frames?: positiveInt;
    // R4Deleted duration?: unsignedInt;
    duration?: decimal; // R4Added
    content: Attachment;
    note?: Annotation[]; // R4Added
  }
  interface Medication extends DomainResource {
    identifier?: Identifier[]; // R4Added
    code?: CodeableConcept;
    status?: code; // R4Added
    manufacturer?: Reference;
    form?: CodeableConcept; // R4Added
    amount?: Ratio; // R4Added
    ingredient?: MedicationIngredient[]; // R4Added
    batch?: MedicationBatch; // R4Added
    // R4 Deleted isBrand?: boolean;
    // R4Deleted product?: MedicationProduct;
    // R4Deleted package?: MedicationPackage;
  }
  interface MedicationIngredient extends Element { // R4Added
    itemCodeableConcept?: CodeableConcept;
    itemReference?: Reference;
    isActive?: boolean;
    strength?: Ratio;
  }
  interface MedicationBatch extends Element { // R4Added
    lotNumber?: string;
    expirationDate?: dateTime;
  }
  /* R4Deleted interface MedicationProduct extends Element {
      form?: CodeableConcept;
      ingredient?: MedicationProductIngredient[];
      batch?: MedicationProductBatch[];
    }*/
  /* R4Deleted interface MedicationProductIngredient extends Element {
      itemCodeableConcept?: CodeableConcept;
      itemReference?: Reference;
      amount?: Ratio;
    }*/
  /* R4Deleted interface MedicationProductBatch extends Element {
      lotNumber?: string;
      expirationDate?: dateTime;
    }  */
  /* R4Deleted interface MedicationPackage extends Element {
      container?: CodeableConcept;
      content?: MedicationPackageContent[];
    }*/
  /* R4Deleted interface MedicationPackageContent extends Element {
      itemCodeableConcept?: CodeableConcept;
      itemReference?: Reference;
      amount?: Quantity;
    }*/
  interface MedicationAdministration extends DomainResource { // TODO R4
    identifier?: Identifier[];
    status: code;
    medicationCodeableConcept?: CodeableConcept;
    medicationReference?: Reference;
    patient: Reference;
    encounter?: Reference;
    supportingInformation?: Reference[];
    effectiveDateTime?: dateTime;
    effectivePeriod?: Period;
    performer?: Reference;
    reasonReference?: Reference[];
    prescription?: Reference;
    notGiven?: boolean;
    reasonNotGiven?: CodeableConcept[];
    reasonGiven?: CodeableConcept[];
    device?: Reference[];
    note?: Annotation[];
    dosage?: MedicationAdministrationDosage;
    eventHistory?: Reference[];
  }
  interface MedicationAdministrationDosage extends Element {
    text?: string;
    site?: CodeableConcept;
    route?: CodeableConcept;
    method?: CodeableConcept;
    dose?: Quantity;
    rateRatio?: Ratio;
    rateQuantity?: Quantity;
  }
  interface MedicationDispense extends DomainResource { // TODO R4
    identifier?: Identifier;
    status?: code;
    medicationCodeableConcept?: CodeableConcept;
    medicationReference?: Reference;
    patient?: Reference;
    supportingInformation?: Reference[];
    dispenser?: Reference;
    dispensingOrganization?: Reference;
    authorizingPrescription?: Reference[];
    type?: CodeableConcept;
    quantity?: Quantity;
    daysSupply?: Quantity;
    whenPrepared?: dateTime;
    whenHandedOver?: dateTime;
    destination?: Reference;
    receiver?: Reference[];
    note?: Annotation[];
    dosageInstruction?: Dosage[];
    substitution?: MedicationDispenseSubstitution;
    eventHistory?: Reference[];
  }
  interface MedicationDispenseSubstitution extends Element {
    type: CodeableConcept;
    reason?: CodeableConcept[];
    responsibleParty?: Reference[];
  }
  interface MedicationRequest extends DomainResource {
    identifier?: Identifier[];
    // R4Deleted status?: code;
    status: code; // R4Added
    statusReason?: CodeableConcept; // R4Added
    // R4Deleted definition?: Reference[];
    intent: code;
    // R4Deleted category?: CodeableConcept;
    category?: CodeableConcept[]; // R4Added
    priority?: code;
    doNotPerform?: boolean; // R4Added
    reportedBoolean?: boolean; // R4Added
    reportedReference?: Reference; // R4Added
    medicationCodeableConcept?: CodeableConcept;
    medicationReference?: Reference;
    subject: Reference;
    // R4Deleted context?: Reference;
    encounter?: Reference; // R4Added
    supportingInformation?: Reference[];
    authoredOn?: dateTime;
    // R4Deleted requester?: MedicationRequestRequester;
    requester?: Reference; // R4Added
    performer?: Reference; // R4Added
    performerType?: CodeableConcept; // R4Added
    recorder?: Reference; // R4Added
    reasonCode?: CodeableConcept[];
    reasonReference?: Reference[];
    instantiatesCanonical?: canonical[]; // R4Added
    instantiatesUri?: uri[]; // R4Added
    basedOn?: Reference[];
    groupIdentifier?: Identifier; // R4Added
    courseOfTheraptyType?: CodeableConcept; // R4Added
    insurance?: Reference[]; // R4Added
    note?: Annotation[];
    // R4Deleted dosageInstruction?: DosageInstruction[];
    dosageInstruction?: Dosage[]; // R4Added
    dispenseRequest?: MedicationRequestDispenseRequest;
    substitution?: MedicationRequestSubstitution;
    priorPrescription?: Reference;
    detectedIssue?: Reference[]; // R4Added
    eventHistory?: Reference[];
    // R4Deleted requisition?: Identifier;
  }
  /* R4Deleted  interface MedicationRequestRequester extends Element {
      agent?: Reference;
      onBehalfOf?: Reference;
    }*/
  interface MedicationRequestDispenseRequest extends Element {
    initialFill?: MedicationRequestDispenseRequestInitialFill; // R4Added
    dispenseInterval?: Duration; // R4Added
    validityPeriod?: Period;
    // R4Deleted numberOfRepeatsAllowed?: positiveInt;
    numberOfRepeatsAllowed?: unsignedInt; // R4Added
    quantity?: SimpleQuantity;
    expectedSupplyDuration?: Duration;
    performer?: Reference;
  }
  interface MedicationRequestDispenseRequestInitialFill extends Element { // R4Added
    quantity?: SimpleQuantity;
    duration?: Duration;
  }
  interface MedicationRequestSubstitution extends Element {
    // R4Deleted allowed: boolean;
    allowedBoolean?: boolean; // R4Added
    allowedCodeableConcept?: CodeableConcept; // R4Added
    reason?: CodeableConcept;
  }
  interface MedicationStatement extends DomainResource {
    identifier?: Identifier[];
    basedOn?: Reference[];
    partOf?: Reference[];
    status: code;
    statusReason?: CodeableConcept[]; // R4Added
    category?: CodeableConcept;
    medicationCodeableConcept?: CodeableConcept;
    medicationReference?: Reference;
    subject: Reference;
    context?: Reference;
    effectiveDateTime?: dateTime;
    effectivePeriod?: Period;
    dateAsserted?: dateTime;
    informationSource?: Reference;
    derivedFrom?: Reference[];
    reasonCode?: CodeableConcept[];
    reasonReference?: Reference[];
    // R4Deleted taken?: code;
    // R4Deleted reasonNotTaken?: CodeableConcept[];
    note?: Annotation[];
    dosage?: Dosage[];
  }
  interface MessageDefinition extends DomainResource { // TODO R4
    url?: uri;
    version?: string;
    name?: string;
    title?: string;
    status: code;
    experimental?: boolean;
    date: dateTime;
    publisher?: string;
    contact?: ContactDetail[];
    description?: markdown;
    useContext?: UsageContext[];
    jurisdiction?: CodeableConcept[];
    purpose?: markdown;
    copyright?: markdown;
    base?: Reference;
    parent?: Reference[];
    replaces?: Reference[];
    event: Coding;
    category?: code;
    focus?: MessageDefinitionFocus[];
    responseRequired?: boolean;
    allowedResponse?: MessageDefinitionAllowedResponse[];
  }
  interface MessageDefinitionFocus extends Element {
    code: code;
    profile?: Reference;
    min?: unsignedInt;
    max?: string;
  }
  interface MessageDefinitionAllowedResponse extends Element {
    message: Reference;
    situation?: markdown;
  }
  interface MessageHeader extends DomainResource { // TODO R4
    timestamp: instant;
    event: Coding;
    response?: MessageHeaderResponse;
    source: MessageHeaderSource;
    destination?: MessageHeaderDestination[];
    enterer?: Reference;
    author?: Reference;
    receiver?: Reference;
    responsible?: Reference;
    reason?: CodeableConcept;
    data?: Reference[];
  }
  interface MessageHeaderResponse extends Element {
    identifier: id;
    code: code;
    details?: Reference;
  }
  interface MessageHeaderSource extends Element {
    name?: string;
    software?: string;
    version?: string;
    contact?: ContactPoint;
    endpoint: uri;
  }
  interface MessageHeaderDestination extends Element {
    name?: string;
    target?: Reference;
    endpoint: uri;
  }
  interface NamingSystem extends DomainResource { // TODO R4
    name: string;
    status: code;
    kind: code;
    date: dateTime;
    publisher?: string;
    contact?: ContactDetail[];
    responsible?: string;
    type?: CodeableConcept;
    description?: markdown;
    useContext?: UsageContext[];
    jurisdiction?: CodeableConcept[];
    usage?: string;
    uniqueId: NamingSystemUniqueId[];
    replacedBy?: Reference;
  }
  interface NamingSystemUniqueId extends Element {
    type: code;
    value: string;
    preferred?: boolean;
    comment?: string;
    period?: Period;
  }
  interface NutritionRequest extends DomainResource { // TODO R4
    identifier?: Identifier[];
    status?: code;
    patient: Reference;
    encounter?: Reference;
    dateTime: dateTime;
    orderer?: Reference;
    allergyIntolerance?: Reference[];
    foodPreferenceModifier?: CodeableConcept[];
    excludeFoodModifier?: CodeableConcept[];
    oralDiet?: NutritionRequestOralDiet;
    supplement?: NutritionRequestSupplement[];
    enteralFormula?: NutritionRequestEnteralFormula;
  }
  interface NutritionRequestOralDiet extends Element {
    type?: CodeableConcept[];
    schedule?: Timing[];
    nutrient?: NutritionRequestOralDietNutrient[];
    texture?: NutritionRequestOralDietTexture[];
    fluidConsistencyType?: CodeableConcept[];
    instruction?: string;
  }
  interface NutritionRequestOralDietNutrient extends Element {
    modifier?: CodeableConcept;
    amount?: Quantity;
  }
  interface NutritionRequestOralDietTexture extends Element {
    modifier?: CodeableConcept;
    foodType?: CodeableConcept;
  }
  interface NutritionRequestSupplement extends Element {
    type?: CodeableConcept;
    productName?: string;
    schedule?: Timing[];
    quantity?: Quantity;
    instruction?: string;
  }
  interface NutritionRequestEnteralFormula extends Element {
    baseFormulaType?: CodeableConcept;
    baseFormulaProductName?: string;
    additiveType?: CodeableConcept;
    additiveProductName?: string;
    caloricDensity?: Quantity;
    routeofAdministration?: CodeableConcept;
    administration?: NutritionRequestEnteralFormulaAdministration[];
    maxVolumeToDeliver?: Quantity;
    administrationInstruction?: string;
  }
  interface NutritionRequestEnteralFormulaAdministration extends Element {
    schedule?: Timing;
    quantity?: Quantity;
    rateQuantity?: Quantity;
    rateRatio?: Ratio;
  }
  interface Observation extends DomainResource {
    identifier?: Identifier[];
    basedOn?: Reference[]; // R4Added
    partOf?: Reference[]; // R4Added
    status: code;
    category?: CodeableConcept[];
    code: CodeableConcept;
    subject?: Reference;
    focus?: Reference[]; // R4Added
    // R4Deleted context?: Reference;
    encounter?: Reference; // R4Added
    effectiveDateTime?: dateTime;
    effectivePeriod?: Period;
    effectiveTiming?: Timing; // R4Added
    effectiveInstant?: instant; // R4Added
    issued?: instant;
    performer?: Reference[];
    valueQuantity?: Quantity;
    valueCodeableConcept?: CodeableConcept;
    valueString?: string;
    valueBoolean?: boolean;
    valueInteger?: integer; // R4Added
    valueRange?: Range;
    valueRatio?: Ratio;
    valueSampledData?: SampledData;
    // R4Deleted valueAttachment?: Attachment;
    valueTime?: time;
    valueDateTime?: dateTime;
    valuePeriod?: Period;
    dataAbsentReason?: CodeableConcept;
    // R4Deleted interpretation?: CodeableConcept;
    interpretation?: CodeableConcept[]; // R4Added
    // R4Deleted comment?: string;
    note?: Annotation[]; // R4Added
    bodySite?: CodeableConcept;
    method?: CodeableConcept;
    specimen?: Reference;
    device?: Reference;
    referenceRange?: ObservationReferenceRange[];
    hasMember?: Reference[]; // R4Added
    derivedFrom?: Reference[]; // R4Added
    // R4Deleted related?: ObservationRelated[];
    component?: ObservationComponent[];
  }
  interface ObservationReferenceRange extends Element {
    low?: Quantity; // TODO Quantity -> SimpleQuantity
    high?: Quantity; // TODO Quantity -> SimpleQuantity
    type?: CodeableConcept; // R4Added
    // R4Deleted meaning?: CodeableConcept[];
    appliesTo?: CodeableConcept[]; // R4Added
    age?: Range;
    text?: string;
  }
  /* R4Deleted interface ObservationRelated extends Element {
      type?: code;
      target: Reference;
    }*/
  interface ObservationComponent extends Element {
    code: CodeableConcept;
    valueQuantity?: Quantity;
    valueCodeableConcept?: CodeableConcept;
    valueString?: string;
    valueBoolean?: boolean; // R4Added
    valueInteger?: integer; // R4Added
    valueRange?: Range;
    valueRatio?: Ratio;
    valueSampledData?: SampledData;
    // R4Deleted valueAttachment?: Attachment;
    valueTime?: time;
    valueDateTime?: dateTime;
    valuePeriod?: Period;
    dataAbsentReason?: CodeableConcept;
    // R4Deleted interpretation?: CodeableConcept;
    interpretation?: CodeableConcept[]; // R4Added
    referenceRange?: ObservationReferenceRange[];
  }
  interface OperationDefinition extends DomainResource { // TODO R4
    url?: uri;
    version?: string;
    name: string;
    status: code;
    kind: code;
    experimental?: boolean;
    date?: dateTime;
    publisher?: string;
    contact?: ContactDetail[];
    description?: markdown;
    useContext?: UsageContext[];
    jurisdiction?: CodeableConcept[];
    purpose?: markdown;
    idempotent?: boolean;
    code: code;
    comment?: string;
    base?: Reference;
    resource?: code[];
    system: boolean;
    type: boolean;
    instance: boolean;
    parameter?: OperationDefinitionParameter[];
    overload?: OperationDefinitionOverload[];
  }
  interface OperationDefinitionParameter extends Element {
    name: code;
    use: code;
    min: integer;
    max: string;
    documentation?: string;
    type?: code;
    searchType?: code;
    profile?: Reference;
    binding?: OperationDefinitionParameterBinding;
    part?: OperationDefinitionParameter[];
  }
  interface OperationDefinitionParameterBinding extends Element {
    strength: code;
    valueSetUri?: uri;
    valueSetReference?: Reference;
  }
  interface OperationDefinitionOverload extends Element {
    parameterName?: string[];
    comment?: string;
  }
  interface OperationOutcome extends DomainResource {
    issue: OperationOutcomeIssue[];
  }
  interface OperationOutcomeIssue extends Element {
    severity: code;
    code: code;
    details?: CodeableConcept;
    diagnostics?: string;
    location?: string[]; // TODO Deprecated?
    expression?: string[];
  }
  interface Organization extends DomainResource {
    identifier?: Identifier[];
    active?: boolean;
    type?: CodeableConcept[];
    name?: string;
    alias?: string[];
    telecom?: ContactPoint[];
    address?: Address[];
    partOf?: Reference;
    contact?: OrganizationContact[];
    endpoint?: Reference[];
  }
  interface OrganizationContact extends Element {
    purpose?: CodeableConcept;
    name?: HumanName;
    telecom?: ContactPoint[];
    address?: Address;
  }
  interface Parameters extends ResourceBase { // TODO R4
    parameter?: ParametersParameter[];
  }
  interface ParametersParameter extends Element {
    name: string;
    valueBase64Binary?: base64Binary;
    valueBoolean?: boolean;
    valueCode?: code;
    valueDate?: date;
    valueDateTime?: dateTime;
    valueDecimal?: decimal;
    valueId?: id;
    valueInstant?: instant;
    valueInteger?: integer;
    valueMarkdown?: markdown;
    valueOid?: oid;
    valuePositiveInt?: positiveInt;
    valueString?: string;
    valueTime?: time;
    valueUnsignedInt?: unsignedInt;
    valueUri?: uri;
    valueAddress?: Address;
    valueAge?: Age;
    valueAnnotation?: Annotation;
    valueAttachment?: Attachment;
    valueCodeableConcept?: CodeableConcept;
    valueCoding?: Coding;
    valueContactPoint?: ContactPoint;
    valueCount?: Count;
    valueDistance?: Distance;
    valueDuration?: Duration;
    valueHumanName?: HumanName;
    valueIdentifier?: Identifier;
    valueMoney?: Money;
    valuePeriod?: Period;
    valueQuantity?: Quantity;
    valueRange?: Range;
    valueRatio?: Ratio;
    valueReference?: Reference;
    valueSampledData?: SampledData;
    valueSignature?: Signature;
    valueTiming?: Timing;
    valueMeta?: Meta;
    resource?: any;
    part?: ParametersParameter[];
  }
  interface Patient extends DomainResource {
    identifier?: Identifier[];
    active?: boolean;
    name?: HumanName[];
    telecom?: ContactPoint[];
    gender?: code;
    birthDate?: date;
    deceasedBoolean?: boolean;
    deceasedDateTime?: dateTime;
    address?: Address[];
    maritalStatus?: CodeableConcept;
    multipleBirthBoolean?: boolean;
    multipleBirthInteger?: integer;
    photo?: Attachment[];
    contact?: PatientContact[];
    // R4Deleted animal?: PatientAnimal;
    communication?: PatientCommunication[];
    generalPractitioner?: Reference[];
    managingOrganization?: Reference;
    link?: PatientLink[];
  }
  interface PatientContact extends Element {
    relationship?: CodeableConcept[];
    name?: HumanName;
    telecom?: ContactPoint[];
    address?: Address;
    gender?: code;
    organization?: Reference;
    period?: Period;
  }
  /* R4Deleted interface PatientAnimal extends Element {
      species: CodeableConcept;
      breed?: CodeableConcept;
      genderStatus?: CodeableConcept;
    }*/
  interface PatientCommunication extends Element {
    language: CodeableConcept;
    preferred?: boolean;
  }
  interface PatientLink extends Element {
    other: Reference;
    type: code;
  }
  interface PaymentNotice extends DomainResource { // TODO R4
    identifier?: Identifier[];
    status?: code;
    request?: Reference;
    response?: Reference;
    statusDate?: date;
    created?: dateTime;
    target?: Reference;
    provider?: Reference;
    organization?: Reference;
    paymentStatus?: CodeableConcept;
  }
  interface PaymentReconciliation extends DomainResource { // TODO R4
    identifier?: Identifier[];
    status?: code;
    period?: Period;
    created?: dateTime;
    organization?: Reference;
    request?: Reference;
    outcome?: CodeableConcept;
    disposition?: string;
    requestProvider?: Reference;
    requestOrganization?: Reference;
    detail?: PaymentReconciliationDetail[];
    form?: CodeableConcept;
    total?: Money;
    note?: PaymentReconciliationNote[];
  }
  interface PaymentReconciliationDetail extends Element {
    type: CodeableConcept;
    request?: Reference;
    response?: Reference;
    submitter?: Reference;
    payee?: Reference;
    date?: date;
    amount?: Money;
  }
  interface PaymentReconciliationNote extends Element {
    type?: CodeableConcept;
    text?: string;
  }
  interface Person extends DomainResource {
    identifier?: Identifier[];
    name?: HumanName[];
    telecom?: ContactPoint[];
    gender?: code;
    birthDate?: date;
    address?: Address[];
    photo?: Attachment;
    managingOrganization?: Reference;
    active?: boolean;
    link?: PersonLink[];
  }
  interface PersonLink extends Element {
    target: Reference;
    assurance?: code;
  }
  interface PlanDefinition extends DomainResource { // TODO R4
    url?: uri;
    identifier?: Identifier[];
    version?: string;
    name?: string;
    title?: string;
    type?: CodeableConcept;
    status: code;
    experimental?: boolean;
    date?: dateTime;
    description?: markdown;
    purpose?: markdown;
    usage?: string;
    approvalDate?: date;
    lastReviewDate?: date;
    effectivePeriod?: Period;
    useContext?: UsageContext[];
    jurisdiction?: CodeableConcept[];
    topic?: CodeableConcept[];
    contributor?: Contributor[];
    publisher?: string;
    contact?: ContactDetail[];
    copyright?: markdown;
    relatedArtifact?: RelatedArtifact[];
    library?: Reference[];
    actionDefinition?: PlanDefinitionActionDefinition[];
  }
  interface PlanDefinitionActionDefinition extends Element {
    actionIdentifier?: Identifier;
    label?: string;
    title?: string;
    description?: string;
    textEquivalent?: string;
    code?: CodeableConcept[];
    documentation?: RelatedArtifact[];
    triggerDefinition?: TriggerDefinition[];
    condition?: PlanDefinitionActionDefinitionCondition[];
    input?: DataRequirement[];
    output?: DataRequirement[];
    relatedAction?: PlanDefinitionActionDefinitionRelatedAction[];
    timingDateTime?: dateTime;
    timingPeriod?: Period;
    timingDuration?: Duration;
    timingRange?: Range;
    timingTiming?: Timing;
    participantType?: code[];
    type?: Coding;
    groupingBehavior?: code;
    selectionBehavior?: code;
    requiredBehavior?: code;
    precheckBehavior?: code;
    cardinalityBehavior?: code;
    activityDefinition?: Reference;
    transform?: Reference;
    dynamicValue?: PlanDefinitionActionDefinitionDynamicValue[];
    actionDefinition?: PlanDefinitionActionDefinition[];
  }
  interface PlanDefinitionActionDefinitionCondition extends Element {
    kind: code;
    description?: string;
    language?: string;
    expression?: string;
  }
  interface PlanDefinitionActionDefinitionRelatedAction extends Element {
    actionIdentifier: Identifier;
    relationship: code;
    offsetDuration?: Duration;
    offsetRange?: Range;
  }
  interface PlanDefinitionActionDefinitionDynamicValue extends Element {
    description?: string;
    path?: string;
    language?: string;
    expression?: string;
  }
  interface Practitioner extends DomainResource {
    identifier?: Identifier[];
    active?: boolean;
    name?: HumanName[];
    telecom?: ContactPoint[];
    address?: Address[];
    gender?: code;
    birthDate?: date;
    photo?: Attachment[];
    // R4Deleted role?: PractitionerRole[];
    qualification?: PractitionerQualification[];
    communication?: CodeableConcept[];
  }
  /* R4Deleted  interface PractitionerRole extends Element {
      organization?: Reference;
      code?: CodeableConcept;
      specialty?: CodeableConcept[];
      identifier?: Identifier[];
      telecom?: ContactPoint[];
      period?: Period;
      location?: Reference[];
      healthcareService?: Reference[];
      endpoint?: Reference[];
    }*/
  interface PractitionerQualification extends Element {
    identifier?: Identifier[];
    code: CodeableConcept;
    period?: Period;
    issuer?: Reference;
  }
  interface PractitionerRole extends DomainResource { // R4Added
    identifier?: Identifier[];
    active?: boolean;
    period?: Period;
    practitioner?: Reference;
    organization?: Reference;
    code?: CodeableConcept[];
    specialty?: CodeableConcept[];
    location?: Reference[];
    healthcareService?: Reference[];
    telecom?: ContactPoint[];
    availableTime?: PractitionerRoleAvailableTime[];
    notAvailable?: PractitionerRoleNotAvailable[];
    availabilityExceptions?: string;
    endpoint?: Reference[];
  }
  interface PractitionerRoleAvailableTime extends Element { // R4Added
    daysOfWeek?: code[];
    allDay?: boolean;
    availableStartTime?: time;
    availableEndTime?: time;
  }
  interface PractitionerRoleNotAvailable extends Element { // R4Added
    description: string;
    during?: Period;
  }
  interface Procedure extends DomainResource {
    identifier?: Identifier[];
    // instantiatesCanonical?: canonical[]; // R4Added TODO canonical
    instantiatesUri?: uri[]; // R4Added
    basedOn?: Reference[]; // R4Added
    partOf?: Reference[]; // R4Added
    status: code;
    statusReason?: CodeableConcept; // R4Added
    // R4Deleted notDone?: boolean;
    // R4Deleted notDoneReason?: CodeableConcept;
    category?: CodeableConcept;
    code?: CodeableConcept;
    subject: Reference;
    // R4Deleted context?: Reference;
    encounter?: Reference; // R4Added
    performedDateTime?: dateTime;
    performedPeriod?: Period;
    performedString?: string; // R4Added
    performedAge?: Age; // R4Added
    performedRange?: Range; // R4Added
    recorder?: Reference; // R4Added
    asserter?: Reference; // R4Added
    performer?: ProcedurePerformer[];
    location?: Reference;
    reasonCode?: CodeableConcept[];
    reasonReference?: Reference[];
    bodySite?: CodeableConcept[];
    outcome?: CodeableConcept;
    report?: Reference[];
    complication?: CodeableConcept[];
    complicationDetail?: Reference[];
    followUp?: CodeableConcept[];
    note?: Annotation[];
    focalDevice?: ProcedureFocalDevice[];
    usedReference?: Reference[];
    usedCode?: CodeableConcept[];
  }
  interface ProcedurePerformer extends Element {
    function?: CodeableConcept;
    actor: Reference;
    // R4Deleted role?: CodeableConcept;
    onBehalfOf?: Reference;
  }
  interface ProcedureFocalDevice extends Element {
    action?: CodeableConcept;
    manipulated: Reference;
  }
  /* R4Deleted interface ProcedureRequest extends DomainResource {
      identifier?: Identifier[];
      subject: Reference;
      code: CodeableConcept;
      bodySite?: CodeableConcept[];
      reasonCode?: CodeableConcept[];
      reasonReference?: Reference[];
      occurrenceDateTime?: dateTime;
      occurrencePeriod?: Period;
      occurrenceTiming?: Timing;
      encounter?: Reference;
      performer?: Reference;
      status?: code;
      intent?: string;
      supportingInfo?: Reference[];
      notes?: Annotation[];
      asNeededBoolean?: boolean;
      asNeededCodeableConcept?: CodeableConcept;
      orderedOn?: dateTime;
      requester?: ProcedureRequestRequester;
      priority?: code;
    }*/
  /* R4Deleted interface ProcedureRequestRequester extends Element {
      agent?: Reference;
      onBehalfOf?: Reference;
    }*/
  interface ProcessRequest extends DomainResource { // TODO R4
    identifier?: Identifier[];
    status?: code;
    action?: code;
    target?: Reference;
    created?: dateTime;
    provider?: Reference;
    organization?: Reference;
    request?: Reference;
    response?: Reference;
    nullify?: boolean;
    reference?: string;
    item?: ProcessRequestItem[];
    include?: string[];
    exclude?: string[];
    period?: Period;
  }
  interface ProcessRequestItem extends Element {
    sequenceLinkId: integer;
  }
  interface ProcessResponse extends DomainResource {
    identifier?: Identifier[];
    status?: code;
    created?: dateTime;
    organization?: Reference;
    request?: Reference;
    outcome?: CodeableConcept;
    disposition?: string;
    requestProvider?: Reference;
    requestOrganization?: Reference;
    form?: CodeableConcept;
    note?: ProcessResponseNote[];
    error?: CodeableConcept[];
    communicationRequest?: Reference[];
  }
  interface ProcessResponseNote extends Element {
    type?: CodeableConcept;
    text?: string;
  }
  interface Provenance extends DomainResource { // TODO R4
    target: Reference[];
    period?: Period;
    recorded: instant;
    reason?: Coding[];
    activity?: Coding;
    location?: Reference;
    policy?: uri[];
    agent: ProvenanceAgent[];
    entity?: ProvenanceEntity[];
    signature?: Signature[];
  }
  interface ProvenanceAgent extends Element {
    role: Coding;
    whoUri?: uri;
    whoReference?: Reference;
    onBehalfOfUri?: uri;
    onBehalfOfReference?: Reference;
    relatedAgentType?: CodeableConcept;
  }
  interface ProvenanceEntity extends Element {
    role: code;
    reference: Reference;
    agent?: ProvenanceAgent[];
  }
  interface Questionnaire extends DomainResource {
    url?: uri;
    identifier?: Identifier[];
    version?: string;
    name?: string;
    title?: string;
    derivedFrom?: canonical; // R4Added
    status: code;
    experimental?: boolean; // R4Added
    subjectType?: code[];
    date?: dateTime;
    publisher?: string;
    contact?: ContactDetail[]; // R4Added
    // R4Deleted telecom?: ContactPoint[];
    description?: markdown; // R4Added
    // R4Deleted useContext?: CodeableConcept[];
    useContext?: UsageContext[]; // R4Added
    jurisdiction ?: CodeableConcept[]; // R4Added
    purpose?: markdown; // R4Added
    copyright?: markdown; // R4Added
    approvalDate?: date; // R4Added
    lastReviewDate?: date; // R4Added
    effectivePeriod?: Period; // R4Added
    // R4Deleted concept?: Coding[];
    code?: Coding[]; // R4Added
    item?: QuestionnaireItem[];
  }
  interface QuestionnaireItem extends Element {
    linkId: string;
    definition?: uri;
    code?: Coding[];
    prefix?: string;
    text?: string;
    // R4Deleted type?: code;
    type: code; // R4Added
    enableWhen?: QuestionnaireItemEnableWhen[];
    enableBehavior?: code; // R4Added
    required?: boolean;
    repeats?: boolean;
    readOnly?: boolean;
    maxLength?: integer;
    // R4Deleted options?: Reference;
    // answerValueSet?: canonical; // R4Added TODO canonical
    // R4Deleted option?: QuestionnaireItemOption[];
    answerOption?: QuestionnaireItemAnswerOption[]; // R4Added
    // R4Deleted initialBoolean?: boolean;
    // R4Deleted initialDecimal?: decimal;
    // R4Deleted initialInteger?: integer;
    // R4Deleted initialDate?: date;
    // R4Deleted initialDateTime?: dateTime;
    // R4Deleted initialInstant?: instant;
    // R4Deleted initialTime?: time;
    // R4Deleted initialString?: string;
    // R4Deleted initialUri?: uri;
    // R4Deleted initialAttachment?: Attachment;
    // R4Deleted initialCoding?: Coding;
    // R4Deleted initialQuantity?: Quantity;
    // R4Deleted initialReference?: Reference;
    initial?: QuestionnaireItemInitial[]; // R4Added
    item?: QuestionnaireItem[];
  }
  interface QuestionnaireItemEnableWhen extends Element {
    question: string;
    // R4Deleted hasAnswer?: boolean;
    operator: code; // R4Added
    answerBoolean?: boolean;
    answerDecimal?: decimal;
    answerInteger?: integer;
    answerDate?: date;
    answerDateTime?: dateTime;
    // R4Deleted answerInstant?: instant;
    answerTime?: time;
    answerString?: string;
    // R4Deleted answerUri?: uri;
    // R4Deleted answerAttachment?: Attachment;
    answerCoding?: Coding;
    answerQuantity?: Quantity;
    answerReference?: Reference;
  }
  interface QuestionnaireItemAnswerOption extends Element { // R4Added name change
    valueInteger?: integer;
    valueDate?: date;
    valueTime?: time;
    valueString?: string;
    valueCoding?: Coding;
    valueReference?: Reference; // R4Added
    initialSelected?: boolean; // R4Added
  }
  interface QuestionnaireItemInitial extends Element { // R4Added
    valueBoolean?: boolean;
    valueDecimal?: decimal;
    valueInteger?: integer;
    valueDate?: date;
    valueDateTime?: dateTime;
    valueTime?: time;
    valueString?: string;
    valueUri?: uri;
    valueAttachment?: Attachment;
    valueCoding?: Coding;
    valueQuantity?: Quantity;
    valueReference?: Reference;
  }
  interface QuestionnaireResponse extends DomainResource {
    identifier?: Identifier;
    basedOn?: Reference[];
    parent?: Reference[];
    questionnaire?: string;
    status: code;
    subject?: Reference;
    // R4Deleted context?: Reference;
    encounter?: Reference; // R4Added
    authored?: dateTime;
    author?: Reference;
    source?: Reference;
    item?: QuestionnaireResponseItem[];
  }
  interface QuestionnaireResponseItem extends Element {
    linkId: string;
    definition?: uri;
    text?: string;
    // R4Deleted subject?: Reference;
    answer?: QuestionnaireResponseItemAnswer[];
    item?: QuestionnaireResponseItem[];
  }
  interface QuestionnaireResponseItemAnswer extends Element {
    valueBoolean?: boolean;
    valueDecimal?: decimal;
    valueInteger?: integer;
    valueDate?: date;
    valueDateTime?: dateTime;
    // R4Deleted valueInstant?: instant;
    valueTime?: time;
    valueString?: string;
    valueUri?: uri;
    valueAttachment?: Attachment;
    valueCoding?: Coding;
    valueQuantity?: Quantity;
    valueReference?: Reference;
    item?: QuestionnaireResponseItem[];
  }
  /* R4Deleted interface ReferralRequest extends DomainResource {
      identifier?: Identifier[];
      basedOn?: Reference[];
      parent?: Identifier;
      status: code;
      intent: code;
      category: code;
      type?: CodeableConcept;
      priority?: code;
      subject: Reference;
      context?: Reference;
      occurrenceDateTime: dateTime;
      occurrencePeriod?: Period;
      authoredOn?: dateTime;
      requester?: ReferralRequestRequester;
      specialty?: CodeableConcept;
      recipient?: Reference[];
      reasonCode?: CodeableConcept[];
      reasonReference?: Reference[];
      description?: string;
      serviceRequested?: CodeableConcept[];
      supportingInformation?: Reference[];
      note?: Annotation[];
    }*/
  /* R4Deleted interface ReferralRequestRequester extends Element {
      agent?: Reference;
      onBehalfOf?: Reference;
    }*/
  interface RelatedPerson extends DomainResource {
    identifier?: Identifier[];
    active?: boolean;
    patient: Reference;
    // R4Deleted relationship?: CodeableConcept;
    relationship?: CodeableConcept[]; // R4Added
    name?: HumanName[];
    telecom?: ContactPoint[];
    gender?: code;
    birthDate?: date;
    address?: Address[];
    photo?: Attachment[];
    period?: Period;
    communication?: RelatedPersonCommunication; // R4Added
  }
  interface RelatedPersonCommunication extends Element { // R4Added
    language?: CodeableConcept;
    preferred?: boolean;
  }
  interface RequestGroup extends DomainResource {
    identifier?: Identifier[];
    instantiatesCanonical?: canonical[]; // R4Added
    instantiatesUri?: uri; // R4Added
    basedOn?: Reference[];
    replaces?: Reference[];
    groupIdentifier?: Identifier[];
    status: code;
    intent: code;
    priority?: CodeableConcept; // R4Added
    code?: CodeableConcept; // R4Added
    subject?: Reference;
    // R4Deleted context?: Reference;
    encounter?: Reference; // R4Added
    authoredOn?: dateTime;
    author?: Reference;
    reasonCode?: CodeableConcept[]; // R4Added
    // R4Deleted reasonCodeableConcept?: CodeableConcept;
    reasonReference?: Reference[]; // R4Added
    note?: Annotation[];
    action?: RequestGroupAction[];
  }
  interface RequestGroupAction extends Element {
    // R4Deleted actionIdentifier?: Identifier; TODO
    // R4Deleted label?: string;
    prefix?: string // R4Added
    title?: string;
    description?: string;
    textEquivalent?: string;
    priority?: code; // R4Added
    code?: CodeableConcept[];
    documentation?: RelatedArtifact[];
    condition?: RequestGroupActionCondition[];
    relatedAction?: RequestGroupActionRelatedAction[];
    timingDateTime?: dateTime;
    timingAge?: Age // R4Added
    timingPeriod?: Period;
    timingDuration?: Duration;
    timingRange?: Range;
    timingTiming?: Timing;
    participant?: Reference[];
    type?: CodeableConcept; // R4Added type change
    groupingBehavior?: code;
    selectionBehavior?: code;
    requiredBehavior?: code;
    precheckBehavior?: code;
    cardinalityBehavior?: code;
    resource?: Reference;
    action?: RequestGroupAction[];
  }
  interface RequestGroupActionCondition extends Element {
    kind: code;
    // R4Deleted description?: string;
    // R4Deleted language?: string;
    expression?: Expression; // R4Added type change
  }
  interface RequestGroupActionRelatedAction extends Element {
    actionId: Identifier; // R4Added name change
    relationship: code;
    offsetDuration?: Duration;
    offsetRange?: Range;
  }
  interface ResearchStudy extends DomainResource { // TODO R4
    identifier?: Identifier[];
    title?: string;
    protocol?: Reference[];
    partOf?: Reference[];
    status: code;
    category?: CodeableConcept[];
    focus?: CodeableConcept[];
    contact?: ContactDetail[];
    relatedArtifact?: RelatedArtifact[];
    keyword?: CodeableConcept[];
    jurisdiction?: CodeableConcept[];
    description?: markdown;
    enrollment?: Reference[];
    period?: Period;
    sponsor?: Reference;
    principalInvestigator?: Reference;
    site?: Reference[];
    reasonStopped?: CodeableConcept;
    note?: Annotation[];
    arm?: ResearchStudyArm[];
  }
  interface ResearchStudyArm extends Element {
    name: string;
    code?: CodeableConcept;
    description?: string;
  }
  interface ResearchSubject extends DomainResource { // TODO R4
    identifier?: Identifier;
    status: code;
    period?: Period;
    study: Reference;
    individual: Reference;
    assignedArm?: string;
    actualArm?: string;
    consent?: Reference;
  }
  interface RiskAssessment extends DomainResource { // TODO R4
    identifier?: Identifier;
    basedOn?: Reference;
    parent?: Reference;
    status: code;
    code?: CodeableConcept;
    subject?: Reference;
    context?: Reference;
    occurrenceDateTime?: dateTime;
    occurrencePeriod?: Period;
    condition?: Reference;
    performer?: Reference;
    reasonCodeableConcept?: CodeableConcept;
    reasonReference?: Reference;
    method?: CodeableConcept;
    basis?: Reference[];
    prediction?: RiskAssessmentPrediction[];
    mitigation?: string;
    note?: Annotation;
  }
  interface RiskAssessmentPrediction extends Element {
    outcome: CodeableConcept;
    probabilityDecimal?: decimal;
    probabilityRange?: Range;
    probabilityCodeableConcept?: CodeableConcept;
    relativeRisk?: decimal;
    whenPeriod?: Period;
    whenRange?: Range;
    rationale?: string;
  }
  interface Schedule extends DomainResource { // TODO R4
    identifier?: Identifier[];
    active?: boolean;
    serviceCategory?: CodeableConcept;
    serviceType?: CodeableConcept[];
    specialty?: CodeableConcept[];
    actor: Reference;
    planningHorizon?: Period;
    comment?: string;
  }
  interface SearchParameter extends DomainResource { // TODO R4
    url: uri;
    version?: string;
    name: string;
    status: code;
    experimental?: boolean;
    date?: dateTime;
    publisher?: string;
    contact?: ContactDetail[];
    useContext?: UsageContext[];
    jurisdiction?: CodeableConcept[];
    purpose?: markdown;
    code: code;
    base: code[];
    type: code;
    derivedFrom?: uri;
    description: markdown;
    expression?: string;
    xpath?: string;
    xpathUsage?: code;
    target?: code[];
    comparator?: code[];
    modifier?: code[];
    chain?: string[];
    component?: SearchParameterComponent[];
  }
  interface SearchParameterComponent extends Element {
    definition: Reference;
    expression: string;
  }
  interface Sequence extends DomainResource { // TODO R4
    identifier?: Identifier[];
    type?: code;
    coordinateSystem: integer;
    patient?: Reference;
    specimen?: Reference;
    device?: Reference;
    performer?: Reference;
    quantity?: Quantity;
    referenceSeq?: SequenceReferenceSeq;
    variant?: SequenceVariant[];
    observedSeq?: string;
    quality?: SequenceQuality[];
    readCoverage?: integer;
    repository?: SequenceRepository[];
    pointer?: Reference[];
    structureVariant?: SequenceStructureVariant[];
  }
  interface SequenceReferenceSeq extends Element {
    chromosome?: CodeableConcept;
    genomeBuild?: string;
    referenceSeqId?: CodeableConcept;
    referenceSeqPointer?: Reference;
    referenceSeqString?: string;
    strand?: integer;
    windowStart: integer;
    windowEnd: integer;
  }
  interface SequenceVariant extends Element {
    start?: integer;
    end?: integer;
    observedAllele?: string;
    referenceAllele?: string;
    cigar?: string;
    variantPointer?: Reference;
  }
  interface SequenceQuality extends Element {
    type: code;
    standardSequence?: CodeableConcept;
    start?: integer;
    end?: integer;
    score?: Quantity;
    method?: CodeableConcept;
    truthTP?: decimal;
    queryTP?: decimal;
    truthFN?: decimal;
    queryFP?: decimal;
    gtFP?: decimal;
    precision?: decimal;
    recall?: decimal;
    fScore?: decimal;
  }
  interface SequenceRepository extends Element {
    type: code;
    url?: uri;
    name?: string;
    datasetId?: string;
    variantsetId?: string;
    readsetId?: string;
  }
  interface SequenceStructureVariant extends Element {
    precisionOfBoundaries?: string;
    reportedaCGHRatio?: decimal;
    length?: integer;
    outer?: SequenceStructureVariantOuter;
    inner?: SequenceStructureVariantInner;
  }
  interface SequenceStructureVariantOuter extends Element {
    start?: integer;
    end?: integer;
  }
  interface SequenceStructureVariantInner extends Element {
    start?: integer;
    end?: integer;
  }
  interface ServiceDefinition extends DomainResource { // TODO R4
    url?: uri;
    identifier?: Identifier[];
    version?: string;
    name?: string;
    title?: string;
    status: code;
    experimental?: boolean;
    date?: dateTime;
    description?: markdown;
    purpose?: markdown;
    usage?: string;
    approvalDate?: date;
    lastReviewDate?: date;
    effectivePeriod?: Period;
    useContext?: UsageContext[];
    jurisdiction?: CodeableConcept[];
    topic?: CodeableConcept[];
    contributor?: Contributor[];
    publisher?: string;
    contact?: ContactDetail[];
    copyright?: markdown;
    relatedArtifact?: RelatedArtifact[];
    trigger?: TriggerDefinition[];
    dataRequirement?: DataRequirement[];
    operationDefinition?: Reference;
  }
  interface ServiceRequest extends DomainResource {
    identifier?: Identifier[];
    // instantiatesCanonical?: canonical[]; //TODO
    instantiatesUri?: uri[];
    basedOn?: Reference[];
    replaces?: Reference[];
    requisition?: Identifier;
    status: code;
    intent: code;
    category?: CodeableConcept[];
    priority?: code;
    doNotPerform?: boolean;
    code?: CodeableConcept;
    orderDetail?: CodeableConcept[];
    quantityQuantity?: Quantity;
    quantityRatio?: Ratio;
    quantityRange?: Range;
    subject: Reference;
    encounter?: Reference;
    occurrenceDateTime?: dateTime;
    occurrencePeriod?: Period;
    occurrenceTiming?: Timing;
    asNeededBoolean?: boolean;
    asNeededCodeableConcept?: CodeableConcept;
    authoredOn?: dateTime;
    requester?: Reference;
    performerType?: CodeableConcept;
    performer?: Reference[];
    locationCode?: CodeableConcept[];
    locationReference?: Reference[];
    reasonCode?: CodeableConcept[];
    reasonReference?: Reference[];
    insurance?: Reference[];
    supportingInfo?: Reference[];
    specimen?: Reference[];
    bodySite?: CodeableConcept[];
    note?: Annotation[];
    patientInstruction?: string;
    relevantHistory?: Reference[];
  }
  interface Slot extends DomainResource { // TODO R4
    identifier?: Identifier[];
    serviceCategory?: CodeableConcept;
    serviceType?: CodeableConcept[];
    specialty?: CodeableConcept[];
    appointmentType?: CodeableConcept;
    schedule: Reference;
    status: code;
    start: instant;
    end: instant;
    overbooked?: boolean;
    comment?: string;
  }
  interface Specimen extends DomainResource { // TODO R4
    identifier?: Identifier[];
    accessionIdentifier?: Identifier;
    status?: code;
    type?: CodeableConcept;
    subject: Reference;
    receivedTime?: dateTime;
    parent?: Reference[];
    request?: Reference[];
    collection?: SpecimenCollection;
    treatment?: SpecimenTreatment[];
    container?: SpecimenContainer[];
    note?: Annotation[];
  }
  interface SpecimenCollection extends Element {
    collector?: Reference;
    collectedDateTime?: dateTime;
    collectedPeriod?: Period;
    quantity?: Quantity;
    method?: CodeableConcept;
    bodySite?: CodeableConcept;
  }
  interface SpecimenTreatment extends Element {
    description?: string;
    procedure?: CodeableConcept;
    additive?: Reference[];
    timeDateTime?: dateTime;
    timePeriod?: Period;
  }
  interface SpecimenContainer extends Element {
    identifier?: Identifier[];
    description?: string;
    type?: CodeableConcept;
    capacity?: Quantity;
    specimenQuantity?: Quantity;
    additiveCodeableConcept?: CodeableConcept;
    additiveReference?: Reference;
  }
  interface StructureDefinition extends DomainResource { // TODO R4
    url: uri;
    identifier?: Identifier[];
    version?: string;
    name: string;
    title?: string;
    status: code;
    experimental?: boolean;
    publisher?: string;
    contact?: ContactDetail[];
    date?: dateTime;
    description?: markdown;
    useContext?: UsageContext[];
    jurisdiction?: CodeableConcept[];
    purpose?: markdown;
    copyright?: markdown;
    keyword?: Coding[];
    fhirVersion?: id;
    mapping?: StructureDefinitionMapping[];
    kind: code;
    abstract: boolean;
    contextType?: code;
    context?: string[];
    contextInvariant?: string[];
    type: code;
    baseDefinition?: uri;
    derivation?: code;
    snapshot?: StructureDefinitionSnapshot;
    differential?: StructureDefinitionDifferential;
  }
  interface StructureDefinitionMapping extends Element {
    identity: id;
    uri?: uri;
    name?: string;
    comments?: string;
  }
  interface StructureDefinitionSnapshot extends Element {
    element: ElementDefinition[];
  }
  interface StructureDefinitionDifferential extends Element {
    element: ElementDefinition[];
  }
  interface StructureMap extends DomainResource { // TODO R4
    url: uri;
    identifier?: Identifier[];
    version?: string;
    name: string;
    title?: string;
    status: code;
    experimental?: boolean;
    publisher?: string;
    contact?: ContactDetail[];
    date?: dateTime;
    description?: markdown;
    useContext?: UsageContext[];
    jurisdiction?: CodeableConcept[];
    purpose?: markdown;
    copyright?: markdown;
    structure?: StructureMapStructure[];
    import?: uri[];
    group: StructureMapGroup[];
  }
  interface StructureMapStructure extends Element {
    url: uri;
    mode: code;
    documentation?: string;
  }
  interface StructureMapGroup extends Element {
    name: id;
    extends?: id;
    documentation?: string;
    input: StructureMapGroupInput[];
    rule: StructureMapGroupRule[];
  }
  interface StructureMapGroupInput extends Element {
    name: id;
    type?: string;
    mode: code;
    documentation?: string;
  }
  interface StructureMapGroupRule extends Element {
    name: id;
    source: StructureMapGroupRuleSource[];
    target?: StructureMapGroupRuleTarget[];
    rule?: StructureMapGroupRule[];
    dependent?: StructureMapGroupRuleDependent[];
    documentation?: string;
  }
  interface StructureMapGroupRuleSource extends Element {
    required: boolean;
    context: id;
    contextType: code;
    min?: integer;
    max?: string;
    type?: string;
    element?: string;
    listMode?: code;
    variable?: id;
    condition?: string;
    check?: string;
  }
  interface StructureMapGroupRuleTarget extends Element {
    context?: id;
    contextType?: code;
    element?: string;
    variable?: id;
    listMode?: code[];
    listRuleId?: id;
    transform?: code;
    parameter?: StructureMapGroupRuleTargetParameter[];
  }
  interface StructureMapGroupRuleTargetParameter extends Element {
    valueId?: id;
    valueString?: string;
    valueBoolean?: boolean;
    valueInteger?: integer;
    valueDecimal?: decimal;
  }
  interface StructureMapGroupRuleDependent extends Element {
    name: id;
    variable: string[];
  }
  interface Subscription extends DomainResource {
    status: code;
    contact?: ContactPoint[];
    end?: instant;
    reason: string;
    criteria: string;
    error?: string;
    channel: SubscriptionChannel;
  }
  interface SubscriptionChannel extends Element {
    type: code;
    endpoint?: uri;
    payload?: code;
    header?: string[];
  }
  interface Substance extends DomainResource { // TODO R4
    identifier?: Identifier[];
    category?: CodeableConcept[];
    code: CodeableConcept;
    description?: string;
    instance?: SubstanceInstance[];
    ingredient?: SubstanceIngredient[];
  }
  interface SubstanceInstance extends Element {
    identifier?: Identifier;
    expiry?: dateTime;
    quantity?: Quantity;
  }
  interface SubstanceIngredient extends Element {
    quantity?: Ratio;
    substanceCodeableConcept?: CodeableConcept;
    substanceReference?: Reference;
  }
  interface SupplyDelivery extends DomainResource { // TODO R4
    identifier?: Identifier;
    status?: code;
    patient?: Reference;
    type?: CodeableConcept;
    quantity?: Quantity;
    suppliedItemCodeableConcept?: CodeableConcept;
    suppliedItemReference?: Reference;
    supplier?: Reference;
    whenPrepared?: Period;
    time?: dateTime;
    destination?: Reference;
    receiver?: Reference[];
  }
  interface SupplyRequest extends DomainResource { // TODO R4
    patient?: Reference;
    source?: Reference;
    date?: dateTime;
    identifier?: Identifier;
    status?: code;
    kind?: CodeableConcept;
    orderedItemCodeableConcept?: CodeableConcept;
    orderedItemReference?: Reference;
    supplier?: Reference[];
    reasonCodeableConcept?: CodeableConcept;
    reasonReference?: Reference;
    when?: SupplyRequestWhen;
  }
  interface SupplyRequestWhen extends Element {
    code?: CodeableConcept;
    schedule?: Timing;
  }
  interface Task extends DomainResource { // TODO R4
    identifier?: Identifier[];
    definitionUri?: uri;
    definitionReference?: Reference;
    basedOn?: Reference[];
    groupIdentifier?: Identifier;
    partOf?: Reference[];
    status: code;
    statusReason?: CodeableConcept;
    businessStatus?: CodeableConcept;
    intent: code;
    priority?: code;
    code?: CodeableConcept;
    description?: string;
    focus?: Reference;
    for?: Reference;
    context?: Reference;
    executionPeriod?: Period;
    authoredOn?: dateTime;
    lastModified?: dateTime;
    requester?: TaskRequester;
    performerType?: CodeableConcept[];
    owner?: Reference;
    reason?: CodeableConcept;
    note?: Annotation[];
    relevantHistory?: Reference[];
    restriction?: TaskRestriction;
    input?: TaskInput[];
    output?: TaskOutput[];
  }
  interface TaskRequester extends Element {
    agent: Reference;
    onBehalfOf?: Reference;
  }
  interface TaskRestriction extends Element {
    repetitions?: positiveInt;
    period?: Period;
    recipient?: Reference[];
  }
  interface TaskInput extends Element {
    type: CodeableConcept;
    valueBase64Binary?: base64Binary;
    valueBoolean?: boolean;
    valueCode?: code;
    valueDate?: date;
    valueDateTime?: dateTime;
    valueDecimal?: decimal;
    valueId?: id;
    valueInstant?: instant;
    valueInteger?: integer;
    valueMarkdown?: markdown;
    valueOid?: oid;
    valuePositiveInt?: positiveInt;
    valueString?: string;
    valueTime?: time;
    valueUnsignedInt?: unsignedInt;
    valueUri?: uri;
    valueAddress?: Address;
    valueAge?: Age;
    valueAnnotation?: Annotation;
    valueAttachment?: Attachment;
    valueCodeableConcept?: CodeableConcept;
    valueCoding?: Coding;
    valueContactPoint?: ContactPoint;
    valueCount?: Count;
    valueDistance?: Distance;
    valueDuration?: Duration;
    valueHumanName?: HumanName;
    valueIdentifier?: Identifier;
    valueMoney?: Money;
    valuePeriod?: Period;
    valueQuantity?: Quantity;
    valueRange?: Range;
    valueRatio?: Ratio;
    valueReference?: Reference;
    valueSampledData?: SampledData;
    valueSignature?: Signature;
    valueTiming?: Timing;
    valueMeta?: Meta;
  }
  interface TaskOutput extends Element {
    type: CodeableConcept;
    valueBase64Binary?: base64Binary;
    valueBoolean?: boolean;
    valueCode?: code;
    valueDate?: date;
    valueDateTime?: dateTime;
    valueDecimal?: decimal;
    valueId?: id;
    valueInstant?: instant;
    valueInteger?: integer;
    valueMarkdown?: markdown;
    valueOid?: oid;
    valuePositiveInt?: positiveInt;
    valueString?: string;
    valueTime?: time;
    valueUnsignedInt?: unsignedInt;
    valueUri?: uri;
    valueAddress?: Address;
    valueAge?: Age;
    valueAnnotation?: Annotation;
    valueAttachment?: Attachment;
    valueCodeableConcept?: CodeableConcept;
    valueCoding?: Coding;
    valueContactPoint?: ContactPoint;
    valueCount?: Count;
    valueDistance?: Distance;
    valueDuration?: Duration;
    valueHumanName?: HumanName;
    valueIdentifier?: Identifier;
    valueMoney?: Money;
    valuePeriod?: Period;
    valueQuantity?: Quantity;
    valueRange?: Range;
    valueRatio?: Ratio;
    valueReference?: Reference;
    valueSampledData?: SampledData;
    valueSignature?: Signature;
    valueTiming?: Timing;
    valueMeta?: Meta;
  }
  interface TestScript extends DomainResource { // TODO R4
    url: uri;
    identifier?: Identifier;
    version?: string;
    name: string;
    title?: string;
    status: code;
    experimental?: boolean;
    publisher?: string;
    contact?: ContactDetail[];
    date?: dateTime;
    description?: markdown;
    useContext?: UsageContext[];
    jurisdiction?: CodeableConcept[];
    purpose?: markdown;
    copyright?: markdown;
    origin?: TestScriptOrigin[];
    destination?: TestScriptDestination[];
    metadata?: TestScriptMetadata;
    fixture?: TestScriptFixture[];
    profile?: Reference[];
    variable?: TestScriptVariable[];
    rule?: TestScriptRule[];
    ruleset?: TestScriptRuleset[];
    setup?: TestScriptSetup;
    test?: TestScriptTest[];
    teardown?: TestScriptTeardown;
  }
  interface TestScriptOrigin extends Element {
    index: integer;
    profile: Coding;
  }
  interface TestScriptDestination extends Element {
    index: integer;
    profile: Coding;
  }
  interface TestScriptMetadata extends Element {
    link?: TestScriptMetadataLink[];
    capability: TestScriptMetadataCapability[];
  }
  interface TestScriptMetadataLink extends Element {
    url: uri;
    description?: string;
  }
  interface TestScriptMetadataCapability extends Element {
    required?: boolean;
    validated?: boolean;
    description?: string;
    origin?: integer[];
    destination?: integer;
    link?: uri[];
    capabilities: Reference;
  }
  interface TestScriptFixture extends Element {
    autocreate?: boolean;
    autodelete?: boolean;
    resource?: Reference;
  }
  interface TestScriptVariable extends Element {
    name: string;
    defaultValue?: string;
    description?: string;
    expression?: string;
    headerField?: string;
    hint?: string;
    path?: string;
    sourceId?: id;
  }
  interface TestScriptRule extends Element {
    resource: Reference;
    param?: TestScriptRuleParam[];
  }
  interface TestScriptRuleParam extends Element {
    name: string;
    value?: string;
  }
  interface TestScriptRuleset extends Element {
    resource: Reference;
    rule: TestScriptRulesetRule[];
  }
  interface TestScriptRulesetRule extends Element {
    ruleId: id;
    param?: TestScriptRulesetRuleParam[];
  }
  interface TestScriptRulesetRuleParam extends Element {
    name: string;
    value?: string;
  }
  interface TestScriptSetup extends Element {
    action: TestScriptSetupAction[];
  }
  interface TestScriptSetupAction extends Element {
    operation?: TestScriptSetupActionOperation;
    assert?: TestScriptSetupActionAssert;
  }
  interface TestScriptSetupActionOperation extends Element {
    type?: Coding;
    resource?: code;
    label?: string;
    description?: string;
    accept?: code;
    contentType?: code;
    destination?: integer;
    encodeRequestUrl?: boolean;
    origin?: integer;
    params?: string;
    requestHeader?: TestScriptSetupActionOperationRequestHeader[];
    requestId?: id;
    responseId?: id;
    sourceId?: id;
    targetId?: id;
    url?: string;
  }
  interface TestScriptSetupActionOperationRequestHeader extends Element {
    field: string;
    value: string;
  }
  interface TestScriptSetupActionAssert extends Element {
    label?: string;
    description?: string;
    direction?: code;
    compareToSourceId?: string;
    compareToSourceExpression?: string;
    compareToSourcePath?: string;
    contentType?: code;
    expression?: string;
    headerField?: string;
    minimumId?: string;
    navigationLinks?: boolean;
    operator?: code;
    path?: string;
    requestURL?: string;
    resource?: code;
    response?: code;
    responseCode?: string;
    rule?: TestScriptSetupActionAssertRule;
    ruleset?: TestScriptSetupActionAssertRuleset;
    sourceId?: id;
    validateProfileId?: id;
    value?: string;
    warningOnly?: boolean;
  }
  interface TestScriptSetupActionAssertRule extends Element {
    ruleId: id;
    param?: TestScriptSetupActionAssertRuleParam[];
  }
  interface TestScriptSetupActionAssertRuleParam extends Element {
    name: string;
    value: string;
  }
  interface TestScriptSetupActionAssertRuleset extends Element {
    rulesetId: id;
    rule?: TestScriptSetupActionAssertRulesetRule[];
  }
  interface TestScriptSetupActionAssertRulesetRule extends Element {
    ruleId: id;
    param?: TestScriptSetupActionAssertRulesetRuleParam[];
  }
  interface TestScriptSetupActionAssertRulesetRuleParam extends Element {
    name: string;
    value: string;
  }
  interface TestScriptTest extends Element {
    name?: string;
    description?: string;
    action: TestScriptTestAction[];
  }
  interface TestScriptTestAction extends Element {
    operation?: TestScriptSetupActionOperation;
    assert?: TestScriptSetupActionAssert;
  }
  interface TestScriptTeardown extends Element {
    action: TestScriptTeardownAction[];
  }
  interface TestScriptTeardownAction extends Element {
    operation: TestScriptSetupActionOperation;
  }
  interface TestReport extends DomainResource { // TODO R4
    identifier?: Identifier;
    name?: string;
    status: code;
    score?: decimal;
    tester?: string;
    testScript: Reference;
    issued?: dateTime;
    participant?: TestReportParticipant[];
    setup?: TestReportSetup;
    test?: TestReportTest[];
    teardown?: TestReportTeardown;
  }
  interface TestReportParticipant extends Element {
    type: code;
    uri: uri;
    display?: string;
  }
  interface TestReportSetup extends Element {
    action: TestReportSetupAction[];
  }
  interface TestReportSetupAction extends Element {
    operation?: TestReportSetupActionOperation;
    assert?: TestReportSetupActionAssert;
  }
  interface TestReportSetupActionOperation extends Element {
    result: code;
    message?: markdown;
    detail?: uri;
  }
  interface TestReportSetupActionAssert extends Element {
    result: code;
    message?: markdown;
    detail?: string;
  }
  interface TestReportTest extends Element {
    name?: string;
    description?: string;
    action: TestReportTestAction[];
  }
  interface TestReportTestAction extends Element {
    operation?: TestReportSetupActionOperation;
    assert?: TestReportSetupActionAssert;
  }
  interface TestReportTeardown extends Element {
    action: TestReportTeardownAction[];
  }
  interface TestReportTeardownAction extends Element {
    operation: TestReportSetupActionOperation;
  }
  interface ValueSet extends DomainResource {
    url?: uri;
    identifier?: Identifier[];
    version?: string;
    name?: string;
    title?: string;
    status: code;
    experimental?: boolean;
    date?: dateTime;
    publisher?: string;
    contact?: ContactDetail[];
    description?: markdown;
    useContext?: UsageContext[];
    jurisdiction?: CodeableConcept[];
    immutable?: boolean;
    purpose?: markdown;
    copyright?: markdown;
    // R4Deleted extensible?: boolean;
    compose?: ValueSetCompose;
    expansion?: ValueSetExpansion;
  }
  interface ValueSetCompose extends Element {
    lockedDate?: date;
    inactive?: boolean;
    include: ValueSetComposeInclude[];
    exclude?: ValueSetComposeInclude[];
  }
  interface ValueSetComposeInclude extends Element {
    system?: uri;
    version?: string;
    concept?: ValueSetComposeIncludeConcept[];
    filter?: ValueSetComposeIncludeFilter[];
    // R4Deleted valueSet?: uri[];
    valueSet?: canonical[]; // R4Added
  }
  interface ValueSetComposeIncludeConcept extends Element {
    code: code;
    display?: string;
    designation?: ValueSetComposeIncludeConceptDesignation[];
  }
  interface ValueSetComposeIncludeConceptDesignation extends Element {
    language?: code;
    use?: Coding;
    value: string;
  }
  interface ValueSetComposeIncludeFilter extends Element {
    property: code;
    op: code;
    value: code;
  }
  interface ValueSetExpansion extends Element {
    // R4Deleted identifier: uri;
    identifier?: uri; // R4Added
    timestamp: dateTime;
    total?: integer;
    offset?: integer;
    parameter?: ValueSetExpansionParameter[];
    contains?: ValueSetExpansionContains[];
  }
  interface ValueSetExpansionParameter extends Element {
    name: string;
    valueString?: string;
    valueBoolean?: boolean;
    valueInteger?: integer;
    valueDecimal?: decimal;
    valueUri?: uri;
    valueCode?: code;
    valueDateTime?: dateTime; // R4Added
  }
  interface ValueSetExpansionContains extends Element {
    system?: uri;
    abstract?: boolean;
    inactive?: boolean;
    version?: string;
    code?: code;
    display?: string;
    designation?: ValueSetComposeIncludeConceptDesignation[];
    contains?: ValueSetExpansionContains[];
  }
  interface VisionPrescription extends DomainResource { // TODO R4
    identifier?: Identifier[];
    status?: code;
    patient?: Reference;
    encounter?: Reference;
    dateWritten?: dateTime;
    prescriber?: Reference;
    reasonCodeableConcept?: CodeableConcept;
    reasonReference?: Reference;
    dispense?: VisionPrescriptionDispense[];
  }
  interface VisionPrescriptionDispense extends Element {
    product?: CodeableConcept;
    eye?: CodeableConcept;
    sphere?: decimal;
    cylinder?: decimal;
    axis?: integer;
    prism?: decimal;
    base?: CodeableConcept;
    add?: decimal;
    power?: decimal;
    backCurve?: decimal;
    diameter?: decimal;
    duration?: Quantity;
    color?: string;
    brand?: string;
    note?: string;
  }
  interface Address extends Element {
    use?: code;
    type?: code;
    text?: string;
    line?: string[];
    city?: string;
    district?: string;
    state?: string;
    postalCode?: string;
    country?: string;
    period?: Period;
  }
  interface TriggerDefinition extends Element {
    type: code;
    // R4Deleted eventName?: string;
    name?: string; // R4Added
    // R4Deleted eventTimingTiming?: Timing;
    timingTiming?: Timing; // R4Added
    // R4Deleted eventTimingReference?: Reference;
    timingReference?: Reference; // R4Added
    // R4Deleted eventTimingDate?: date;
    timingDate?: date; // R4Added
    // R4Deleted eventTimingDateTime?: dateTime;
    timingDateTime?: dateTime; // R4Added
    // R4Deleted eventData?: DataRequirement;
    data?: DataRequirement; // R4Added
  }
  interface Contributor extends Element {
    type: code;
    name: string;
    contact?: ContactDetail[];
  }
  interface Attachment extends Element {
    contentType?: code;
    language?: code;
    data?: base64Binary;
    // R4Deleted url?: uri;
    url?: url; // R4Added
    size?: unsignedInt;
    hash?: base64Binary;
    title?: string;
    creation?: dateTime;
  }
  interface Duration extends Quantity {
  }
  interface Count extends Quantity {
  }
  interface SimpleQuantity extends Quantity { // TODO?
  }
  interface DataRequirement extends Element {
    type: code;
    // R4Deleted profile?: uri[];
    profile?: canonical[]; // R4Added
    subjectCodeableConcept?: CodeableConcept; // R4Added
    subjectReference?: Reference; // R4Added
    mustSupport?: string[];
    codeFilter?: DataRequirementCodeFilter[];
    dateFilter?: DataRequirementDateFilter[];
    limit?: positiveInt; // R4Added
    sort?: DataRequirementSort[]; // R4Added
  }
  interface DataRequirementCodeFilter extends Element {
    // R4Deleted path: string;
    path?: string; // R4Added
    searchParam?: string; // R4Added
    valueSet?: canonical; // R4Added
    // R4Deleted valueSetString?: string;
    // R4Deleted valueSetReference?: Reference;
    code?: Coding[]; // R4Added
    // R4Deleted valueCode?: code[];
    // R4Deleted valueCoding?: Coding[];
    // R4Deleted valueCodeableConcept?: CodeableConcept[];
  }
  interface DataRequirementDateFilter extends Element {
    // R4Deleted path: string;
    path?: string; // R4Added
    searchParam?: string; // R4Added
    valueDateTime?: dateTime;
    valuePeriod?: Period;
    valueDuration?: Duration;
  }
  interface DataRequirementSort extends Element { // R4Added
    path: string;
    direciotn: code;
  }
  interface Range extends Element {
    low?: Quantity; // TODO Quantity -> SimpleQuantity
    high?: Quantity; // TODO Quantity -> SimpleQuantity
  }
  interface RelatedArtifact extends Element {
    type: code;
    label?: string; // R4Added
    display?: string;
    citation?: string;
    // R4Deleted url?: uri;
    url?: url; // R4Added
    document?: Attachment;
    // R4Deleted resource?: Reference;
    resource?: canonical; // R4Added
  }
  interface Annotation extends Element {
    authorReference?: Reference;
    authorString?: string;
    time?: dateTime;
    // R4Deleted text: string;
    text: markdown; // R4Added
  }
  interface ContactDetail extends Element {
    name?: string;
    telecom?: ContactPoint[];
  }
  interface ContactPoint extends Element {
    system?: code;
    value?: string;
    use?: code;
    rank?: positiveInt;
    period?: Period;
  }
  interface HumanName extends Element {
    use?: code;
    text?: string;
    family?: string;
    given?: string[];
    prefix?: string[];
    suffix?: string[];
    period?: Period;
  }
  interface Money extends Quantity {
  }
  interface Signature extends Element {
    type: Coding[];
    when: instant;
    // R4Deleted whoUri?: uri;
    // R4Deleted whoReference?: Reference;
    who: Reference; // R4Added
    // R4Deleted onBehalfOfUri?: uri;
    // R4Deleted onBehalfOfReference?: Reference;
    onBehalfOf?: Reference; // R4Added
    targetFormat?: code; // R4Added
    sigFormat?: code; // R4Added
    // R4Deleted contentType?: code;
    // R4Deleted blob?: base64Binary;
    data?: base64Binary; // R4Added
  }
  interface SampledData extends Element {
    origin: Quantity;
    period: decimal;
    factor?: decimal;
    lowerLimit?: decimal;
    upperLimit?: decimal;
    dimensions: positiveInt;
    // R4Deleted data: string;
    data?: string; // R4Added
  }
  interface Ratio extends Element {
    numerator?: Quantity;
    denominator?: Quantity;
  }
  interface Timing extends Element {
    event?: dateTime[];
    repeat?: TimingRepeat;
    code?: CodeableConcept;
    when?: string[];
  }
  interface TimingRepeat extends Element {
    boundsDuration?: Duration;
    boundsRange?: Range;
    boundsPeriod?: Period;
    // R4Deleted count?: integer;
    count?: positiveInt; // R4Added
    // R4Deleted countMax?: integer;
    countMax?: positiveInt; // R4Added
    duration?: decimal;
    durationMax?: decimal;
    durationUnit?: code;
    // R4Deleted frequency?: integer;
    frequency?: positiveInt; // R4Added
    // R4Deleted frequencyMax?: integer;
    frequencyMax?: positiveInt; // R4Added
    period?: decimal;
    periodMax?: decimal;
    periodUnit?: code;
    dayOfWeek?: code[];
    timeOfDay?: time[];
    when?: code[];
    offset?: unsignedInt;
  }
  interface ElementDefinition extends Element {
    path: string;
    representation?: code[];
    sliceName?: string;
    sliceIsConstraining?: boolean; // R4Added
    label?: string;
    code?: Coding[];
    slicing?: ElementDefinitionSlicing;
    short?: string;
    definition?: markdown;
    comments?: markdown;
    requirements?: markdown;
    alias?: string[];
    min?: integer;
    max?: string;
    base?: ElementDefinitionBase;
    contentReference?: uri;
    type?: ElementDefinitionType[];
    defaultValueBase64Binary?: base64Binary;
    defaultValueBoolean?: boolean;
    defaultValueCanonical?: canonical; // R4Added
    defaultValueCode?: code;
    defaultValueDate?: date;
    defaultValueDateTime?: dateTime;
    defaultValueDecimal?: decimal;
    defaultValueId?: id;
    defaultValueInstant?: instant;
    defaultValueInteger?: integer;
    defaultValueMarkdown?: markdown;
    defaultValueOid?: oid;
    defaultValuePositiveInt?: positiveInt;
    defaultValueString?: string;
    defaultValueTime?: time;
    defaultValueUnsignedInt?: unsignedInt;
    defaultValueUri?: uri;
    defaultValueUrl?: url; // R4Added
    defaultValueUuid?: uuid; // R4Added
    defaultValueAddress?: Address;
    defaultValueAge?: Age;
    defaultValueAnnotation?: Annotation;
    defaultValueAttachment?: Attachment;
    defaultValueCodeableConcept?: CodeableConcept;
    defaultValueCoding?: Coding;
    defaultValueContactPoint?: ContactPoint;
    defaultValueCount?: Count;
    defaultValueDistance?: Distance;
    defaultValueDuration?: Duration;
    defaultValueHumanName?: HumanName;
    defaultValueIdentifier?: Identifier;
    defaultValueMoney?: Money;
    defaultValuePeriod?: Period;
    defaultValueQuantity?: Quantity;
    defaultValueRange?: Range;
    defaultValueRatio?: Ratio;
    defaultValueReference?: Reference;
    defaultValueSampledData?: SampledData;
    defaultValueSignature?: Signature;
    defaultValueTiming?: Timing;
    defaultValueContactDetail?: ContactDetail; // R4Added
    defaultValueContributor?: Contributor; // R4Added
    defaultValueDataRequirement?: DataRequirement; // R4Added
    defaultValueExpression?: Expression; // R4Added
    defaultValueParameterDefinition?: ParameterDefinition; // R4Added
    defaultValueRelatedArtifact?: RelatedArtifact;  // R4Added
    defaultValueTriggerDefinition?: TriggerDefinition; // R4Added
    defaultValueUsageContext?: UsageContext; // R4Added
    defaultValueDosage?: Dosage; // R4Added
    defaultValueMeta?: Meta;
    meaningWhenMissing?: markdown;
    fixedBase64Binary?: base64Binary;
    fixedBoolean?: boolean;
    fixedCanonical?: canonical; // R4Added
    fixedCode?: code;
    fixedDate?: date;
    fixedDateTime?: dateTime;
    fixedDecimal?: decimal;
    fixedId?: id;
    fixedInstant?: instant;
    fixedInteger?: integer;
    fixedMarkdown?: markdown;
    fixedOid?: oid;
    fixedPositiveInt?: positiveInt;
    fixedString?: string;
    fixedTime?: time;
    fixedUnsignedInt?: unsignedInt;
    fixedUri?: uri;
    fixedUrl?: url; // R4Added
    fixedUuid?: uuid; // R4Added
    fixedAddress?: Address;
    fixedAge?: Age;
    fixedAnnotation?: Annotation;
    fixedAttachment?: Attachment;
    fixedCodeableConcept?: CodeableConcept;
    fixedCoding?: Coding;
    fixedContactPoint?: ContactPoint;
    fixedCount?: Count;
    fixedDistance?: Distance;
    fixedDuration?: Duration;
    fixedHumanName?: HumanName;
    fixedIdentifier?: Identifier;
    fixedMoney?: Money;
    fixedPeriod?: Period;
    fixedQuantity?: Quantity;
    fixedRange?: Range;
    fixedRatio?: Ratio;
    fixedReference?: Reference;
    fixedSampledData?: SampledData;
    fixedSignature?: Signature;
    fixedTiming?: Timing;
    fixedContactDetail?: ContactDetail; // R4Added
    fixedContributor?: Contributor; // R4Added
    fixedDataRequirement?: DataRequirement; // R4Added
    fixedExpression?: Expression; // R4Added
    fixedParameterDefinition?: ParameterDefinition; // R4Added
    fixedRelatedArtifact?: RelatedArtifact;  // R4Added
    fixedTriggerDefinition?: TriggerDefinition; // R4Added
    fixedUsageContext?: UsageContext; // R4Added
    fixedDosage?: Dosage; // R4Added
    fixedMeta?: Meta;
    patternBase64Binary?: base64Binary;
    patternBoolean?: boolean;
    patternCanonical?: canonical; // R4Added
    patternCode?: code;
    patternDate?: date;
    patternDateTime?: dateTime;
    patternDecimal?: decimal;
    patternId?: id;
    patternInstant?: instant;
    patternInteger?: integer;
    patternMarkdown?: markdown;
    patternOid?: oid;
    patternPositiveInt?: positiveInt;
    patternString?: string;
    patternTime?: time;
    patternUnsignedInt?: unsignedInt;
    patternUri?: uri;
    patternUrl?: url; // R4Added
    patternUuid?: uuid; // R4Added
    patternAddress?: Address;
    patternAge?: Age;
    patternAnnotation?: Annotation;
    patternAttachment?: Attachment;
    patternCodeableConcept?: CodeableConcept;
    patternCoding?: Coding;
    patternContactPoint?: ContactPoint;
    patternCount?: Count;
    patternDistance?: Distance;
    patternDuration?: Duration;
    patternHumanName?: HumanName;
    patternIdentifier?: Identifier;
    patternMoney?: Money;
    patternPeriod?: Period;
    patternQuantity?: Quantity;
    patternRange?: Range;
    patternRatio?: Ratio;
    patternReference?: Reference;
    patternSampledData?: SampledData;
    patternSignature?: Signature;
    patternTiming?: Timing;
    patternContactDetail?: ContactDetail; // R4Added
    patternContributor?: Contributor; // R4Added
    patternDataRequirement?: DataRequirement; // R4Added
    patternExpression?: Expression; // R4Added
    patternParameterDefinition?: ParameterDefinition; // R4Added
    patternRelatedArtifact?: RelatedArtifact;  // R4Added
    patternTriggerDefinition?: TriggerDefinition; // R4Added
    patternUsageContext?: UsageContext; // R4Added
    patternDosage?: Dosage; // R4Added
    patternMeta?: Meta;
    example?: ElementDefinitionExample[];
    minValueDate?: date;
    minValueDateTime?: dateTime;
    minValueInstant?: instant;
    minValueTime?: time;
    minValueDecimal?: decimal;
    minValueInteger?: integer;
    minValuePositiveInt?: positiveInt;
    minValueUnsignedInt?: unsignedInt;
    minValueQuantity?: Quantity;
    maxValueDate?: date;
    maxValueDateTime?: dateTime;
    maxValueInstant?: instant;
    maxValueTime?: time;
    maxValueDecimal?: decimal;
    maxValueInteger?: integer;
    maxValuePositiveInt?: positiveInt;
    maxValueUnsignedInt?: unsignedInt;
    maxValueQuantity?: Quantity;
    maxLength?: integer;
    condition?: id[];
    constraint?: ElementDefinitionConstraint[];
    mustSupport?: boolean;
    isModifier?: boolean;
    isModifierReason?: boolean; // R4Added
    isSummary?: boolean;
    binding?: ElementDefinitionBinding;
    mapping?: ElementDefinitionMapping[];
  }
  interface ElementDefinitionSlicing extends Element {
    // R4Deleted discriminator?: string[];
    discriminator?: ElementDefinitionSlicingDiscriminator[]; // R4Added
    description?: string;
    ordered?: boolean;
    rules: code;
  }
  interface ElementDefinitionSlicingDiscriminator extends Element {
    type: code;
    path: string;
  }
  interface ElementDefinitionBase extends Element {
    path: string;
    min: integer;
    max: string;
  }
  interface ElementDefinitionType extends Element {
    code: uri;
    // R4Deleted profile?: uri;
    profile?: canonical[]; // R4Added
    // R4Deleted targetProfile?: uri;
    targetProfile?: canonical[]; // R4Added
    aggregation?: code[];
    versioning?: code;
  }
  interface ElementDefinitionExample extends Element {
    label: string;
    valueBase64Binary?: base64Binary;
    valueBoolean?: boolean;
    valueCanonical?: canonical; // R4Added
    valueCode?: code;
    valueDate?: date;
    valueDateTime?: dateTime;
    valueDecimal?: decimal;
    valueId?: id;
    valueInstant?: instant;
    valueInteger?: integer;
    valueMarkdown?: markdown;
    valueOid?: oid;
    valuePositiveInt?: positiveInt;
    valueString?: string;
    valueTime?: time;
    valueUnsignedInt?: unsignedInt;
    valueUri?: uri;
    valueUrl?: url; // R4Added
    valueUuid?: uuid; // R4Added
    valueAddress?: Address;
    valueAge?: Age;
    valueAnnotation?: Annotation;
    valueAttachment?: Attachment;
    valueCodeableConcept?: CodeableConcept;
    valueCoding?: Coding;
    valueContactPoint?: ContactPoint;
    valueCount?: Count;
    valueDistance?: Distance;
    valueDuration?: Duration;
    valueHumanName?: HumanName;
    valueIdentifier?: Identifier;
    valueMoney?: Money;
    valuePeriod?: Period;
    valueQuantity?: Quantity;
    valueRange?: Range;
    valueRatio?: Ratio;
    valueReference?: Reference;
    valueSampledData?: SampledData;
    valueSignature?: Signature;
    valueTiming?: Timing;
    valueContactDetail?: ContactDetail; // R4Added
    valueContributor?: Contributor; // R4Added
    valueDataRequirement?: DataRequirement; // R4Added
    valueExpression?: Expression; // R4Added
    valueParameterDefinition?: ParameterDefinition; // R4Added
    valueRelatedArtifact?: RelatedArtifact;  // R4Added
    valueTriggerDefinition?: TriggerDefinition; // R4Added
    valueUsageContext?: UsageContext; // R4Added
    valueDosage?: Dosage; // R4Added
    valueMeta?: Meta;
  }
  interface ElementDefinitionConstraint extends Element {
    key: id;
    requirements?: string;
    severity: code;
    human: string;
    // R4Deleted expression: string;
    expression?: string; // R4Added
    xpath?: string;
    // R4Deleted source?: uri;
    source?: canonical; // R4Added
  }
  interface ElementDefinitionBinding extends Element {
    strength: code;
    description?: string;
    // R4Deleted valueSetUri?: uri;
    // R4Deleted valueSetReference?: Reference;
    valueSet?: canonical; // R4Added
  }
  interface ElementDefinitionMapping extends Element {
    identity: id;
    language?: code;
    map: string;
    comment?: string; // R4Added
  }
  interface Expression extends Element { // R4Added
    description?: string;
    name?: id;
    language: code;
    expression?: string;
    reference?: uri;
  }
  interface Age extends Quantity {
  }
  interface Distance extends Quantity {
  }
  interface Dosage extends Element { // R4Added
    sequence?: integer;
    text?: string;
    additionalInstruction?: CodeableConcept[];
    patientInstruction?: string;
    timing?: Timing;
    asNeededBoolean?: boolean;
    asNeededCodeableConcept?: CodeableConcept;
    site?: CodeableConcept;
    route?: CodeableConcept;
    method?: CodeableConcept;
    doseAndRate?: DosageDoseAndRate[];
    maxDosePerPeriod?: Ratio;
    maxDosePerAdministration?: SimpleQuantity;
    maxDosePerLifetime?: SimpleQuantity;
  }
  interface DosageDoseAndRate extends Element { // R4Added
    type?: CodeableConcept;
    doseRange?: Range;
    doseQuantity?: Quantity;
    rateRatio?: Ratio;
    rateRange?: Range;
    rateQuantity?: Quantity;
  }
  /* R4Deleted interface DosageInstruction extends Element {
      sequence?: integer;
      text?: string;
      additionalInstructions?: CodeableConcept[];
      timing?: Timing;
      asNeededBoolean?: boolean;
      asNeededCodeableConcept?: CodeableConcept;
      site?: CodeableConcept;
      route?: CodeableConcept;
      method?: CodeableConcept;
      doseRange?: Range;
      doseQuantity?: Quantity;
      maxDosePerPeriod?: Ratio;
      maxDosePerAdministration?: Quantity;
      maxDosePerLifetime?: Quantity;
      rateRatio?: Ratio;
      rateRange?: Range;
      rateQuantity?: Quantity;
    }*/
  interface ParameterDefinition extends Element {
    name?: code;
    use: code;
    min?: integer;
    max?: string;
    documentation?: string;
    type: code;
    // R4Deleted profile?: Reference;
    profile?: canonical // R4Added;
  }
  type Resource = (DomainResource|Account|ActivityDefinition|AllergyIntolerance|Appointment|AppointmentResponse|
    AuditEvent|Basic|Binary|Bundle|CarePlan|CareTeam|Claim|ClaimResponse|ClinicalImpression|CodeSystem|
    Communication|CommunicationRequest|CompartmentDefinition|Composition|ConceptMap|Condition|CapabilityStatement|
    Consent|Contract|Coverage|DataElement|DetectedIssue|Device|DeviceComponent|DeviceMetric|DeviceRequest|
    DeviceUseStatement|DiagnosticRequest|DiagnosticReport|DocumentManifest|DocumentReference|EligibilityRequest|
    EligibilityResponse|Encounter|Endpoint|EnrollmentRequest|EnrollmentResponse|EpisodeOfCare|ExpansionProfile|
    ExplanationOfBenefit|FamilyMemberHistory|Flag|Goal|Group|GuidanceResponse|HealthcareService|ImagingManifest|
    Immunization|ImmunizationRecommendation|ImplementationGuide|Library|Linkage|List|Location|Measure|MeasureReport|
    Media|Medication|MedicationAdministration|MedicationDispense|MedicationRequest|MedicationStatement|
    MessageDefinition|MessageHeader|NamingSystem|NutritionRequest|Observation|OperationDefinition|OperationOutcome|
    Organization|Parameters|Patient|PaymentNotice|PaymentReconciliation|Person|PlanDefinition|Practitioner|
    PractitionerRole|Procedure|ProcessRequest|ProcessResponse|Provenance|Questionnaire|QuestionnaireResponse|RelatedPerson
    |RequestGroup|ResearchStudy|ResearchSubject|RiskAssessment|Schedule|SearchParameter| Sequence|ServiceDefinition|Slot|Specimen|
    StructureDefinition|StructureMap|Subscription| Substance|SupplyDelivery| SupplyRequest|Task|TestScript|TestReport|ValueSet|
    VisionPrescription);

}
